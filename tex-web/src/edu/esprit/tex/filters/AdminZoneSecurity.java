package edu.esprit.tex.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.esprit.tex.beansModule.UserBean;
import edu.esprit.tex.domain.Administrator;
import edu.esprit.tex.domain.Customer;

/**
 * Servlet Filter implementation class AdminZoneSecurity
 */
@WebFilter("/pages/admin/*")
public class AdminZoneSecurity implements Filter {

    /**
     * Default constructor. 
     */
    public AdminZoneSecurity() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		UserBean session = (UserBean) req.getSession().getAttribute("userBean");

		boolean letGo = false;

		if ((session != null) && (session.isLoggedIn())
				&& (session.getUser() instanceof Administrator)) {
			letGo = true;
		}

		if (letGo) {
			chain.doFilter(request, response);
		} else {
			resp.sendRedirect(req.getContextPath()
					+ "/pages/staticPages/login.jsf");
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
