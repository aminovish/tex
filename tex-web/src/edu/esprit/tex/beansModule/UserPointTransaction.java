package edu.esprit.tex.beansModule;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import edu.esprit.tex.domain.User;
import edu.esprit.tex.services.adminstration.user.UsersLocal;
@ManagedBean(name="poinTran")
@ViewScoped
public class UserPointTransaction implements Serializable {
	
	@ManagedProperty(value="#{userBean.user}")
	private User user;
	
	@EJB
	private UsersLocal usersLocal;
	
	private int numbertoken;
	private Long pointToToken;
	
	
	public UserPointTransaction() {
		// TODO Auto-generated constructor stub
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	public Long getPointToToken() {
		return pointToToken;
	}


	public void setPointToToken(Long pointToToken) {
		this.pointToToken = pointToToken;
		setNumbertoken((int)(pointToToken/10));
		
	}
	
	public int getNumbertoken() {
		return numbertoken;
	}


	public void setNumbertoken(int numbertoken) {
		this.numbertoken = numbertoken;
	}
	
	public String dochange(){
		String navigateTo=null;
		Long point = user.getPoint()-pointToToken;
		int token = user.getAccount()+numbertoken;
		user.setAccount(token);
		user.setPoint(point);
		usersLocal.update(user);
		pointToToken=null;
		numbertoken=0;
		return navigateTo;
	}
	
	
	
	
	

}
