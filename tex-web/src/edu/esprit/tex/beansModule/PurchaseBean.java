package edu.esprit.tex.beansModule;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

@ManagedBean
@ViewScoped
public class PurchaseBean implements Serializable{
	
	public PurchaseBean() {
		// TODO Auto-generated constructor stub
	}
	public void validateMonnth(FacesContext context,
			UIComponent component, Object value) throws ValidatorException {
		String val =  (String) value;
		
		if (val == null || val.trim().isEmpty()) {
			return;
		}
		try {
			int month = Integer.parseInt(val);
			if (month<0 || month>12) {
				throw new ValidatorException(new FacesMessage(
						"Invalid Month!"));
			}
			
		} catch (Exception e) {
			throw new ValidatorException(new FacesMessage(
					"Invalid Month!"));
		}
		
	}
	public void validateDAy(FacesContext context,
			UIComponent component, Object value) throws ValidatorException {
		String val =  (String) value;
		if (val == null || val.trim().isEmpty()) {
			return;
		}
		try {
			int day = Integer.parseInt(val);
			if (day<0 || day>31) {
				throw new ValidatorException(new FacesMessage(
						"Invalid Day!"));
			}
		} catch (Exception e) {
			throw new ValidatorException(new FacesMessage(
					"Invalid Day!"));
		}
		
		
	}

}
