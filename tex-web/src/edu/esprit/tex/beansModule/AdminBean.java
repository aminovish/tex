package edu.esprit.tex.beansModule;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import edu.esprit.tex.domain.Administrator;
import edu.esprit.tex.domain.Business;
import edu.esprit.tex.domain.User;
import edu.esprit.tex.services.adminstration.user.UsersLocal;

@ViewScoped
@ManagedBean (name="AdminBean")
public class AdminBean {
	
	
	@ManagedProperty(value="#{userBean.user}")
	User user;
	
	
	Business businesstoactivate;
	
	Business businessactive;

	@EJB
	private UsersLocal userlocal ; 
	
	List<User> users ;
	List<User> users1;
	List<Business> businessestoactivate = new ArrayList<Business>();
	List<Business> businessesactivate = new ArrayList<Business>();
	
	public AdminBean() {
		
	}
	
	@PostConstruct
	public void init(){
		users=userlocal.findUsers();
		for(User u:users){
			if(u instanceof Business){
				if(!u.getState()){
					businessestoactivate.add((Business)u);
				}
			}
		}
		businessesactivate=userlocal.findUsersByUser((Administrator)user);
		
		
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public Business getBusinesstoactivate() {
		return businesstoactivate;
	}

	public void setBusinesstoactivate(Business businesstoactivate) {
		this.businesstoactivate = businesstoactivate;
	}

	public Business getBusinessactive() {
		return businessactive;
	}

	public void setBusinessactive(Business businessactive) {
		this.businessactive = businessactive;
	}

	public List<Business> getBusinessestoactivate() {
		return businessestoactivate;
	}

	public void setBusinessestoactivate(List<Business> businessestoactivate) {
		this.businessestoactivate = businessestoactivate;
	}

	public List<Business> getBusinessesactivate() {
		return businessesactivate;
	}

	public void setBusinessesactivate(List<Business> businessesactivate) {
		this.businessesactivate = businessesactivate;
	}
	
	

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String doActivate(){
		String navigateTo=null;
		businesstoactivate.setState(true);
		businesstoactivate.setAdmin((Administrator)user);
		userlocal.update(businesstoactivate);
		navigateTo="/pages/admin/accueil?faces-redirect=true";
		init();
		return navigateTo;
	}
	
	
	

}
