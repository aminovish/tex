package edu.esprit.tex.beansModule;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import edu.esprit.tex.domain.Business;
import edu.esprit.tex.domain.Service;
import edu.esprit.tex.services.business.services.BusinessServicesLocal;


@ManagedBean (name="serviceBean")
@ViewScoped
public class ServiceBean implements Serializable{


	@EJB
	private BusinessServicesLocal businessServicesLocal;
	private List<Service> services;
	private Service service;
	private UserBean userBean;
	public ServiceBean() {
		
	}
	@PostConstruct
	 public void init(){
		 service=businessServicesLocal.findById(userBean.getUser().getId());
	 }
	public List<Service> getServices() {
		return services;
	}
	public void setServices(List<Service> services) {
		this.services = services;
	}

	
	
}
