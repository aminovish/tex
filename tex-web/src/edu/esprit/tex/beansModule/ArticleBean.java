package edu.esprit.tex.beansModule;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import edu.esprit.tex.domain.Article;
import edu.esprit.tex.services.adminstration.administrator.article.AdminstratorArticleLocal;

@ManagedBean(name = "articleBean")
@SessionScoped
public class ArticleBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Article article = new Article();
	private List<Article> articles;

	@EJB
	private AdminstratorArticleLocal articleLocal;

	public ArticleBean() {

	}

	@PostConstruct
	public void init() {

		articles = articleLocal.FindAllarticle();

	}

	public Article getArticle() {

		return article;

	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}

	public String detail() {
		String navigateTo = "/pages/includes/article?faces-redirect=true";
		System.out.println(article.getId());
		return navigateTo;
	}

	public String getdes(String des) {
		String description;
		if (des.length() > 50) {
			description = des.substring(0, 50);
		} else {
			description = des;
		}
		return description;
	}

}
