package edu.esprit.tex.beansModule;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import edu.esprit.tex.domain.Business;
import edu.esprit.tex.domain.Customer;
import edu.esprit.tex.domain.Service;
import edu.esprit.tex.domain.User;
import edu.esprit.tex.services.adminstration.user.UsersLocal;

@ManagedBean (name="managerUserBean")
@SessionScoped
public class ManagerUserBean implements Serializable{


	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ManagedProperty(value="#{userBean.user}")
	private User user;

	@EJB
	private UsersLocal usersLocal;

	private Service service ;
	private List<Service> services;
	private boolean formDisplayed;
	private boolean video;
	private String url;
	

	



	public ManagerUserBean() {
		
	}

	
	@PostConstruct
	public void initModel(){
		service = new Service();
		services= usersLocal.findUserService(user);
		formDisplayed = false;
		video=false;

	}
	
	public String myService(){
		String navigateTo=null;

		if(user!=null){
			
			services= usersLocal.findUserService(user);
			navigateTo="pages/businessPages/myServices?faces-redirect=true";
		}else{
			navigateTo="pages/error?faces-redirect=true";
		}
		
		return navigateTo;
		
		
		}
	public void addOrApdateService(){
		Date date = new Date();
		service.setDate(date);
		if(user instanceof Business){
 
			service.setBusiness((Business) user);
		}
	
	else if(user instanceof Customer){
		
		service.setCustomer((Customer) user);
		}
		
         usersLocal.updateService(service);	
         services= usersLocal.findUserService(user);
         formDisplayed = false;
		
		}
	
	
	public void doNew() {
		service = new Service();
		formDisplayed = true;
	}
	
	public void doDelete() {
		usersLocal.deleteService(service);
		 services= usersLocal.findUserService(user);
		formDisplayed = false;
	}
	
	public void doCancel() {
		formDisplayed = false;
	}
	
	public void onRowSelect() {
		formDisplayed = true;
		if(service.getYoutube()!=null){
			video=true;
			url="https://youtube.googleapis.com/v/"+service.getYoutube();
			}else{
				video=false;
			}
	}
	
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}


	public List<Service> getServices() {
		return services;
	}


	public void setServices(List<Service> services) {
		this.services = services;
	}


	public Service getService() {
		return service;
	}


	public void setService(Service service) {
		this.service = service;
	}


	public boolean isFormDisplayed() {
		return formDisplayed;
	}


	public void setFormDisplayed(boolean formDisplayed) {
		this.formDisplayed = formDisplayed;
	}


	public boolean isVideo() {
		return video;
	}


	public void setVideo(boolean video) {
		this.video = video;
	}


	public String getUrl() {
		return url;
	}


	public void setUrl(String url) {
		this.url = url;
	}
	
	

	
	

	
	
}
