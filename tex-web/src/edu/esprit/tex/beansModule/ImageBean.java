package edu.esprit.tex.beansModule;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;

import org.apache.commons.io.IOUtils;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import edu.esprit.tex.domain.ArticlePicture;
import edu.esprit.tex.domain.Picture;
import edu.esprit.tex.domain.User;
import edu.esprit.tex.services.administrator.picture.PictureUserLocal;
import edu.esprit.tex.services.adminstration.administrator.article.picture.ArticlePictureLocal;

@ManagedBean(name = "ImgBean")
@RequestScoped
public class ImageBean {

	@ManagedProperty("#{param.imageid}")
	private int id;

	@ManagedProperty("#{param.userid}")
	private int iduser;

	@EJB
	private ArticlePictureLocal articlePictureLocal;

	@EJB
	private PictureUserLocal pictureUserLocal;
	
	

	public ImageBean() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public StreamedContent getimage() {
		DefaultStreamedContent image = null;
		if (FacesContext.getCurrentInstance().getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
			// So, we're rendering the view. Return a stub StreamedContent so
			// that it will generate right URL.

			image = new DefaultStreamedContent();
		} else {
			ArticlePicture articlePicture = articlePictureLocal
					.getArticlePictureById(id);
			// So, browser is requesting the image. Return a real
			// StreamedContent with the image bytes.
			if (articlePicture == null) {
				InputStream is = FacesContext.getCurrentInstance()
						.getExternalContext()
						.getResourceAsStream("/resources/img/Article_icon.jpg");

				try {
					image = new DefaultStreamedContent(
							new ByteArrayInputStream(IOUtils.toByteArray(is)));
				} catch (IOException e) {
					e.printStackTrace();
				}

			} else {
				image = new DefaultStreamedContent(new ByteArrayInputStream(
						articlePicture.getPicturee()));
			}
		}
		return image;
	}

	public StreamedContent getimageuser() {
		DefaultStreamedContent image = null;
		if (FacesContext.getCurrentInstance().getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
			// So, we're rendering the view. Return a stub StreamedContent so
			// that it will generate right URL.
			image = new DefaultStreamedContent();
		} else {

			Picture picture = pictureUserLocal.getPictureUserById(iduser);
			if (picture == null) {
				InputStream is = FacesContext.getCurrentInstance()
						.getExternalContext()
						.getResourceAsStream("/resources/img/defaultuser.jpg");

				try {
					image = new DefaultStreamedContent(
							new ByteArrayInputStream(IOUtils.toByteArray(is)));
				} catch (IOException e) {
					e.printStackTrace();
				}

			} else {
				// So, browser is requesting the image. Return a real
				// StreamedContent with the image bytes.
				image = new DefaultStreamedContent(new ByteArrayInputStream(
						picture.getPicturee()));
			}
		}
		return image;
	}

	public int getIduser() {
		return iduser;
	}

	public void setIduser(int iduser) {
		this.iduser = iduser;
	}


	
	

}
