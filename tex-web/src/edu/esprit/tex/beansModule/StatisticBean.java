package edu.esprit.tex.beansModule;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;

import com.google.api.services.youtube.YouTube.Activities.List;

import edu.esprit.tex.domain.BusinessTransaction;
import edu.esprit.tex.services.business.transactions.BusinessTransactionsLocal;

@ViewScoped
@ManagedBean(name = "statBean")
public class StatisticBean implements Serializable {

	private CartesianChartModel categoryModel;

	@EJB
	BusinessTransactionsLocal businessTransactionsLocal;
	
	java.util.List<BusinessTransaction> buList ;
	
	public StatisticBean() {

	}
	
	@PostConstruct
	public void init(){
		buList=businessTransactionsLocal.findAllTransaction();
		categoryModel = new CartesianChartModel();
		createCategoryModel();
	}
	
	private void createCategoryModel() {
		categoryModel = new CartesianChartModel();
		ChartSeries series = new ChartSeries();
		
			for (BusinessTransaction bt : buList) {

				try {
					if(bt.getValidate()==3){
						series.set(bt.getService().getName(),
								bt.getValidate());
						
					}
					
				} catch (Exception e) {
					
				}
			}

		categoryModel.addSeries(series);

	}
	

	public CartesianChartModel getCategoryModel() {
		return categoryModel;
	}

	public void setCategoryModel(CartesianChartModel categoryModel) {
		this.categoryModel = categoryModel;
	}

	public java.util.List<BusinessTransaction> getBuList() {
		return buList;
	}

	public void setBuList(java.util.List<BusinessTransaction> buList) {
		this.buList = buList;
	}
	

}
