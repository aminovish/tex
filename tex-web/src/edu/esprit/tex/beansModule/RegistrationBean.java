package edu.esprit.tex.beansModule;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import javax.faces.validator.ValidatorException;

import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import edu.esprit.tex.domain.Business;
import edu.esprit.tex.domain.Customer;
import edu.esprit.tex.domain.Parameter;
import edu.esprit.tex.domain.Picture;
import edu.esprit.tex.services.adminstration.administrator.parameter.AdminstratorParameterLocal;
import edu.esprit.tex.services.adminstration.user.UsersLocal;

@ManagedBean(name = "RegistrBean")
@SessionScoped
public class RegistrationBean implements Serializable {

	
	@EJB
	private UsersLocal usersLocal;
	@EJB
	private AdminstratorParameterLocal adminstratorParameterLocal;
	
	
	private Parameter pram = new Parameter();
	private StreamedContent image =null;
	private Customer customer = new Customer();
	private Business business = new Business();
	private Picture picture = new Picture();
	

	public RegistrationBean() {
		// TODO Auto-generated constructor stub
	}

	@PostConstruct
	public void init() {
		pram= adminstratorParameterLocal.FindParameter();
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Business getBusiness() {
		return business;
	}

	public void setBusiness(Business business) {
		this.business = business;
	}

	public String doCreateCustomer() {
		String navigateTo = null;
		customer.setPicture(picture);
		picture.setUser(customer);
		customer.setAccount(pram.getTokenNumber());
		return navigateTo;
	}

	public StreamedContent getImage() {	
		
			return image;		
	}

	public void setImage(StreamedContent image) {
		this.image = image;
	}

	public String doCreateBusiness() {
		String navigateTo = null;
		business.setPicture(picture);
		picture.setUser(business);
		usersLocal.create(business);
		return navigateTo;

	}

	public void handleFileUpload(FileUploadEvent event) {
		UploadedFile file = event.getFile();
		byte[] fileBytes = file.getContents();
		double size = (double) file.getSize();
		picture.setPicturee(fileBytes);
		picture.setSizePicture(size);
		System.out.println(picture.getSizePicture() + "");
		System.out.println("done");
		setImage(new DefaultStreamedContent(new ByteArrayInputStream(
				picture.getPicturee())));

	}

	public Parameter getPram() {
		return pram;
	}

	public void setPram(Parameter pram) {
		this.pram = pram;
	}
	
	public void validateName(FacesContext context,
			UIComponent component, Object value) throws ValidatorException {
        String name = (String) value;
        if (!name.matches("[a-zA-Z]+")) {
            throw new ValidatorException(new FacesMessage("Name can only contain letters!"));
        }
        
        if(name.length() > 255){
            throw new ValidatorException(new FacesMessage("Name must be within 255 characters!"));
        }
	}
	public void validateEmail(FacesContext context,
			UIComponent component, Object value) throws ValidatorException {
		String mail = (String) value;
		if (mail == null || mail.trim().isEmpty()) {
			return;
		}
		boolean loginInUse = usersLocal.findexitmail(mail);
		if (loginInUse) {
			throw new ValidatorException(new FacesMessage(
					"Email already in use!"));
		}
	}
	public void ValidatePhone(FacesContext context,
			UIComponent component, Object value) throws ValidatorException {
		String phone = (String) value;
		try {
			int ph = Integer.parseInt(phone);
		} catch (Exception e) {
			throw new ValidatorException(new FacesMessage(
					"Phone Must be numbers !"));
		}
		
	}

	public StreamedContent getimageprofile() {
		DefaultStreamedContent img = null;
		if (FacesContext.getCurrentInstance().getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
			// So, we're rendering the view. Return a stub StreamedContent so
			// that it will generate right URL.

			img = new DefaultStreamedContent();
		} else {
			
			// So, browser is requesting the image. Return a real
			// StreamedContent with the image bytes.
			if (image == null) {
				InputStream is = FacesContext.getCurrentInstance()
						.getExternalContext()
						.getResourceAsStream("/resources/img/defaultuser.jpg");

				try {
					img = new DefaultStreamedContent(
							new ByteArrayInputStream(IOUtils.toByteArray(is)));
				} catch (IOException e) {
					e.printStackTrace();
				}

			}else{
				img=(DefaultStreamedContent) image;
			}
		
		
		}
		return img;
	}

	
	

	
		
	

}
