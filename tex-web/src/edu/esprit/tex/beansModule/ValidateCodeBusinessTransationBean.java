package edu.esprit.tex.beansModule;

import java.io.Serializable;
import java.util.List;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import edu.esprit.tex.domain.BusinessTransaction;
import edu.esprit.tex.domain.User;
import edu.esprit.tex.services.adminstration.user.UsersLocal;
import edu.esprit.tex.services.business.transactions.BusinessTransactionsLocal;

@ManagedBean(name="validateCode")
@ViewScoped
public class ValidateCodeBusinessTransationBean implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -7339200373095979885L;

	@EJB
	private UsersLocal usersLocal;
	
	@EJB
	private BusinessTransactionsLocal businessTransactionsLocal;
	
	@ManagedProperty(value="#{userBean.user}")
	private User user;
	

	private List<BusinessTransaction> businessTransactions;

	private BusinessTransaction businessTransaction;
	
	private boolean formDisplayed1;
	private boolean video1;
	private String url1;
	private String code;
	

	
		
		public ValidateCodeBusinessTransationBean() {
		
	}


		@PostConstruct
		public void init(){
			businessTransactions = usersLocal.findForCodeBusinessTransactions(user);

		}
		

		
		
		public void doCancel() {
			formDisplayed1 = false;
		}
		
		public void onRowSelect() {
			formDisplayed1 = true;

		}
		

		

		
		public void validate() {

			int b,v,r=0;
			if((businessTransaction.getsCode().equals(code))){
			formDisplayed1 = false;
			b=businessTransaction.getBusiness().getAccount();
			v=businessTransaction.getValue();
			b=b-v;
			businessTransaction.getBusiness().setAccount(b);
			r=businessTransaction.getService().getBusiness().getAccount();
			System.out.println(r);
			r=r+v;
			System.out.println(r);
			businessTransaction.getService().getBusiness().setAccount(r);
			
			businessTransaction.setValidate(0);
			businessTransactionsLocal.updateTransaction(businessTransaction);
			businessTransactions = usersLocal.findAllBusinessTransactions(user);}
			else{
				System.out.println("error");
			}
			
		}
		
		
		
		
		


		public List<BusinessTransaction> getBusinessTransactions() {
			return businessTransactions;
		}

		public void setBusinessTransactions(
				List<BusinessTransaction> businessTransactions) {
			this.businessTransactions = businessTransactions;
		}

		
		public BusinessTransaction getBusinessTransaction() {
			return businessTransaction;
		}

		public void setBusinessTransaction(BusinessTransaction businessTransaction) {
			this.businessTransaction = businessTransaction;
		}

		public boolean isFormDisplayed1() {
			return formDisplayed1;
		}

		public void setFormDisplayed1(boolean formDisplayed1) {
			this.formDisplayed1 = formDisplayed1;
		}

		public boolean isVideo1() {
			return video1;
		}

		public void setVideo1(boolean video1) {
			this.video1 = video1;
		}

		public String getUrl1() {
			return url1;
		}

		public void setUrl1(String url1) {
			this.url1 = url1;
		}


		public User getUser() {
			return user;
		}


		public void setUser(User user) {
			this.user = user;
		}


		public String getCode() {
			return code;
		}


		public void setCode(String code) {
			this.code = code;
		}
	
	
}
