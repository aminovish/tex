package edu.esprit.tex.beansModule;

import java.io.Serializable;
import java.util.List;


import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import edu.esprit.tex.domain.Conversion;
import edu.esprit.tex.domain.User;
import edu.esprit.tex.services.adminstration.user.UsersLocal;
import edu.esprit.tex.services.business.conversion.BusinessConversionLocal;


@ManagedBean(name="managerTokenTransaction")
@ViewScoped
public class ManagerTokenTransactionBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@ManagedProperty(value="#{managerUserBean.user}")
	private User user;
	
	@ManagedProperty(value="#{ManagerTokenTransactionBean.conversion}")
	private Conversion conversion;
	private List<Conversion> conversions;
	
	
	
	int tokenNumber,account;
	float value;

	
	
	@EJB
	private UsersLocal usersLocal;
	
	@EJB
	private BusinessConversionLocal businessConversionLocal;
	
	
	public ManagerTokenTransactionBean() {
		
	}

	@PostConstruct
	public void initModel(){
		conversion = new Conversion ();
		tokenNumber=conversion.getTokenNumber();
		account=user.getAccount();
		value=conversion.getValue();
		setConversions(businessConversionLocal.findAllTConversion());

	}
	public String addTransactToken(){	
		
		
		
		String navigateTo=null;
		businessConversionLocal.addTransactToken(conversion);
		
		conversion.setTokenNumber(tokenNumber);
		conversion.getValue();
		return navigateTo;
	}

	public Conversion getConversion() {
		return conversion;
	}

	public void setConversion(Conversion conversion) {
		this.conversion = conversion;
	}

	public List<Conversion> getConversions() {
		return conversions;
	}

	public void setConversions(List<Conversion> conversions) {
		this.conversions = conversions;
	}

	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}

	public String calculAccount(){
		
		String navgaTo=null;
		
		account= account - tokenNumber;
		
		user.setAccount(account);
		
		navgaTo="/pages/businessPages/account?faces-redirect=true";
		return navgaTo;
	}

	public int getTokenNumber() {
		return tokenNumber;
	}

	public void setTokenNumber(int tokenNumber) {
		this.tokenNumber = tokenNumber;
	}

	public int getAccount() {
		return account;
	}

	public void setAccount(int account) {
		this.account = account;
	}

	public float getValue() {
		return value;
	}

	public void setValue(float value) {
		this.value = value;
	}

	
	

}
