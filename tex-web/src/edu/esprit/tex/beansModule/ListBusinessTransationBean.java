package edu.esprit.tex.beansModule;

import java.io.Serializable;
import java.util.List;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import edu.esprit.tex.domain.BusinessTransaction;
import edu.esprit.tex.domain.User;
import edu.esprit.tex.services.adminstration.user.UsersLocal;
import edu.esprit.tex.services.business.transactions.BusinessTransactionsLocal;

@ManagedBean(name="BusTran")
@ViewScoped
public class ListBusinessTransationBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	



	@EJB
	private UsersLocal usersLocal;
	
	@EJB
	private BusinessTransactionsLocal businessTransactionsLocal;
	
	@ManagedProperty(value="#{userBean.user}")
	private User user;
	

	private List<BusinessTransaction> businessTransactions;
	private List<BusinessTransaction> bTShow;
	private BusinessTransaction businessTransaction;
	private BusinessTransaction businessTransaction1;
	
	private boolean formDisplayed1;
	private boolean video1;
	private String url1;
	
	private boolean formDisplayed2;
	private boolean video2;
	private String url2;
	

	
		
		public ListBusinessTransationBean() {
		
	}


		@PostConstruct
		public void init(){
			bTShow = usersLocal.showBusinessTransactions(user);
			businessTransactions = usersLocal.findAllBusinessTransactions(user);

		}
		

		
		
		public void doCancel() {
			formDisplayed1 = false;
		}
		
		public void onRowSelect() {
			formDisplayed1 = true;
			if(businessTransaction.getService().getYoutube()!=null){
				video2=true;
				url2="https://youtube.googleapis.com/v/"+businessTransaction.getService().getYoutube();
				}else{
					video2=false;
				}

		}
		
		public void doCancels() {
			formDisplayed2 = false;
		}
		
		public void onRowSelects() {
			formDisplayed2 = true;
			if(businessTransaction1.getService().getYoutube()!=null){
				video2=true;
				url2="https://youtube.googleapis.com/v/"+businessTransaction1.getService().getYoutube();
				}else{
					video2=false;
				}
		}
		
		public void acceptJob() {

			formDisplayed1 = false;
			Random r = new Random(); 
			int valeur = 1 + r.nextInt(100000000); 
			System.out.println (valeur);  
			businessTransaction.setShowReceiver(true);
			businessTransaction.setShowSeeder(false);
			businessTransaction.setValidate(1);
			businessTransaction.setsCode(Integer.toString(valeur));
			businessTransactionsLocal.updateTransaction(businessTransaction);
			businessTransactions = usersLocal.findAllBusinessTransactions(user);
			bTShow = usersLocal.showBusinessTransactions(user);
			
		}
		
		
		
		
		


		public List<BusinessTransaction> getBusinessTransactions() {
			return businessTransactions;
		}

		public void setBusinessTransactions(
				List<BusinessTransaction> businessTransactions) {
			this.businessTransactions = businessTransactions;
		}

		public List<BusinessTransaction> getbTShow() {
			return bTShow;
		}

		public void setbTShow(List<BusinessTransaction> bTShow) {
			this.bTShow = bTShow;
		}

		public BusinessTransaction getBusinessTransaction() {
			return businessTransaction;
		}

		public void setBusinessTransaction(BusinessTransaction businessTransaction) {
			this.businessTransaction = businessTransaction;
		}

		public boolean isFormDisplayed1() {
			return formDisplayed1;
		}

		public void setFormDisplayed1(boolean formDisplayed1) {
			this.formDisplayed1 = formDisplayed1;
		}

		public boolean isVideo1() {
			return video1;
		}

		public void setVideo1(boolean video1) {
			this.video1 = video1;
		}

		public String getUrl1() {
			return url1;
		}

		public void setUrl1(String url1) {
			this.url1 = url1;
		}

		public boolean isFormDisplayed2() {
			return formDisplayed2;
		}

		public void setFormDisplayed2(boolean formDisplayed2) {
			this.formDisplayed2 = formDisplayed2;
		}

		public boolean isVideo2() {
			return video2;
		}

		public void setVideo2(boolean video2) {
			this.video2 = video2;
		}

		public String getUrl2() {
			return url2;
		}

		public void setUrl2(String url2) {
			this.url2 = url2;
		}

		public BusinessTransaction getBusinessTransaction1() {
			return businessTransaction1;
		}

		public void setBusinessTransaction1(BusinessTransaction businessTransaction1) {
			this.businessTransaction1 = businessTransaction1;
		}

		public User getUser() {
			return user;
		}


		public void setUser(User user) {
			this.user = user;
		}
	
	
}
