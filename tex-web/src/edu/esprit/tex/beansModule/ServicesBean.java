package edu.esprit.tex.beansModule;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import edu.esprit.tex.domain.Service;
import edu.esprit.tex.services.business.services.BusinessServicesLocal;

@ManagedBean
@ViewScoped
public class ServicesBean {
	
	@EJB
	BusinessServicesLocal bLocal ;
	private Service service = new Service();
	List<Service> services;
	public ServicesBean() {
		// TODO Auto-generated constructor stub
	}

	public void init(){
		services=bLocal.findServicesByBusiness(null);
	}
	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}
	

}
