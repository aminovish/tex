package edu.esprit.tex.beansModule;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import edu.esprit.tex.domain.Parameter;
import edu.esprit.tex.services.adminstration.administrator.parameter.AdminstratorParameterLocal;


@ManagedBean (name="contractBean")
@ViewScoped
public class ContractBean implements Serializable{


		
		private static final long serialVersionUID = 1L;
		@EJB
		private AdminstratorParameterLocal adminstratorParameterLocal;
		private Parameter parameter;
		public ContractBean() {
			
		}
		@PostConstruct
		 public void init(){
			 parameter =adminstratorParameterLocal.FindParameter();
		 }
		public Parameter getParameter() {
			return parameter;
		}
		public void setParameter(Parameter parameter) {
			this.parameter = parameter;
		}
		
		

}
