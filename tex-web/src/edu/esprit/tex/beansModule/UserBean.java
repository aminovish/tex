package edu.esprit.tex.beansModule;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.UploadedFile;

import edu.esprit.tex.domain.Administrator;
import edu.esprit.tex.domain.Business;
import edu.esprit.tex.domain.BusinessTransaction;
import edu.esprit.tex.domain.Customer;
import edu.esprit.tex.domain.Picture;
import edu.esprit.tex.domain.Service;
import edu.esprit.tex.domain.User;
import edu.esprit.tex.services.adminstration.user.UsersLocal;

@ManagedBean(name = "userBean")
@SessionScoped
public class UserBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5223591022473997953L;
	private User user;
	private Business business;
	private Customer customer;
	@EJB
	private UsersLocal usersLocal;
	private Number nbNotif;
	private Number nbNotifJob;

	private boolean loggedIn;
	private List<Service> services;
	private Picture picture = new Picture();



	public UserBean() {

	}

	@PostConstruct
	public void initModel() {

		
		user = new User();
		setLoggedIn(false);

	}

	public String login() {
		String navigateTo = null;
		User found = usersLocal.authenticate(user.getEmail(),
				user.getPassword());
		System.out.println(found.getName());
		if (found != null) {
			user = found;
			loggedIn = true;
			if (user instanceof Business) {
				if(user.getState()){
				services = usersLocal.findUserService((Business) user);
				business = (Business) user;
				setNbNotif(usersLocal.countNotification(user));
				setNbNotifJob(usersLocal.countAcceptedNotification(user));
				navigateTo = "/pages/businessPages/home?faces-redirect=true";
				}
				else{
					
					loggedIn = false;
					FacesContext.getCurrentInstance().addMessage(
							"login_form:login_submit",
							new FacesMessage("Bad credentials!"));
				}

			}

			else if (user instanceof Customer) {
				services = usersLocal.findAllService();
				customer = (Customer) user;
				navigateTo = "/pages/customerPages/home?faces-redirect=true";

			}else if(user instanceof Administrator){
				navigateTo = "/pages/admin/accueil?faces-redirect=true";
			}			
			else {
				loggedIn = false;
				FacesContext.getCurrentInstance().addMessage(
						"login_form:login_submit",
						new FacesMessage("Bad credentials!"));
			}
		}

		return navigateTo;
	}

	public String doLogout() {
		String navigateTo = null;
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
				.clear();
		navigateTo = "/index?faces-redirect=true";
		user = new User();
		return navigateTo;
	}

	public String update() {
		String navigateTo = null;
		if (user instanceof Business) {
			user = business;
			user.setPicture(picture);
		} else {
			user = customer;
			user.setPicture(picture);
		}
		usersLocal.update(user);
		navigateTo = "/pages/businessPages/profile?faces-redirect=true";
		return navigateTo;
	}


	public String myJob() {
		String navigateTo = null;
		nbNotif = 0;

		navigateTo = "/pages/businessPages/myJob?faces-redirect=true";
		return navigateTo;
	}
	

	public String acceptedJob() {
		String navigateTo = null;
		nbNotifJob = 0;

		navigateTo = "/pages/businessPages/acceptedJob?faces-redirect=true";
		return navigateTo;
	}
	
	
	

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public boolean isLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}

	public List<Service> getServices() {
		return services;
	}

	public void setServices(List<Service> services) {
		this.services = services;
	}

	public Business getBusiness() {
		return business;
	}

	public void setBusiness(Business business) {
		this.business = business;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Number getNbNotif() {
		return nbNotif;
	}

	public void setNbNotif(Number nbNotif) {
		this.nbNotif = nbNotif;
	}

	public Number getNbNotifJob() {
		return nbNotifJob;
	}

	public void setNbNotifJob(Number nbNotifJob) {
		this.nbNotifJob = nbNotifJob;
	}

	
	public void handleFileUpload(FileUploadEvent event) {
		UploadedFile file = event.getFile();
		byte[] fileBytes = file.getContents();
		double size = (double) file.getSize();
		picture.setPicturee(fileBytes);
		picture.setSizePicture(size);
		System.out.println(picture.getSizePicture() + "");
		System.out.println("done");

	}

	public Picture getPicture() {
		return picture;
	}

	public void setPicture(Picture picture) {
		this.picture = picture;
	}
	

	
}
