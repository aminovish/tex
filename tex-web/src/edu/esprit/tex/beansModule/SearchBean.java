package edu.esprit.tex.beansModule;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import edu.esprit.tex.domain.Business;
import edu.esprit.tex.domain.Service;
import edu.esprit.tex.domain.User;
import edu.esprit.tex.services.adminstration.user.UsersLocal;
import edu.esprit.tex.services.business.transactions.BusinessTransactionsLocal;

@ManagedBean (name="searchBean")
@SessionScoped
public class SearchBean implements Serializable{


	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ManagedProperty(value="#{userBean.user}")
	private User user;
	
	@EJB
	private UsersLocal usersLocal;
	@EJB
	private BusinessTransactionsLocal businessTransactions;
	private List<User> users;
	private List<Service> services;
	private Service service;
	private String servicename;
	private int servicevalue;
	private String city;
	private User user2;
	private int rate;
	private boolean formDisplayed;
	private boolean video;
	private String url;

	public SearchBean() {
	
	}
	
	@PostConstruct
	public void initModel(){
		servicename="";
		servicevalue=0;
		formDisplayed = false;
		city="";
		users=null;
	}

	public String doSearch(){
		String navigate=null;
		users=null;
        users=usersLocal.searchuser(servicename,servicevalue,city ,user);
		servicename="";
		servicevalue=0;
		city="";
      navigate="/pages/search?faces-redirect=true";
	return navigate;

     }
	
	public String selectedUser(){
		String navigate=null;
		rate=user2.getRating();
         services=usersLocal.findUserService(user2);
		
      navigate="/pages/selectedUser?faces-redirect=true";
	return navigate;

     }
	
	public String addTransaction(){
		String navigate=null;
        businessTransactions.createTransaction((Business) user, service);
		
	    return navigate;

     }
	
	public String doRating(){
		String navigate=null;
    

    usersLocal.update(user2);
    navigate="/pages/selectedUser?faces-redirect=true";
		
	    return navigate;

     }
	
	public void onRowSelect() {
		formDisplayed = true;
		if(service.getYoutube()!=null){
			video=true;
			url="https://youtube.googleapis.com/v/"+service.getYoutube();
			}else{
				video=false;
			}
	}
	
	

	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	public String getServicename() {
		return servicename;
	}


	public void setServicename(String servicename) {
		this.servicename = servicename;
	}


	public int getServicevalue() {
		return servicevalue;
	}


	public void setServicevalue(int servicevalue) {
		this.servicevalue = servicevalue;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public List<User> getUsers() {
		return users;
	}


	public void setUsers(List<User> users) {
		this.users = users;
	}



	public User getUser2() {
		return user2;
	}


	public void setUser2(User user2) {
		this.user2 = user2;
	}


	public List<Service> getServices() {
		return services;
	}


	public void setServices(List<Service> services) {
		this.services = services;
	}


	public Service getService() {
		return service;
	}


	public void setService(Service service) {
		this.service = service;
	}


	public int getRate() {
		return rate;
	}


	public void setRate(int rate) {
		this.rate = rate;
	}


	public boolean isFormDisplayed() {
		return formDisplayed;
	}


	public void setFormDisplayed(boolean formDisplayed) {
		this.formDisplayed = formDisplayed;
	}

	public boolean isVideo() {
		return video;
	}

	public void setVideo(boolean video) {
		this.video = video;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	
	

}
