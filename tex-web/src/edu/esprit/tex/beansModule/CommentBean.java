package edu.esprit.tex.beansModule;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import edu.esprit.tex.domain.Article;
import edu.esprit.tex.domain.ArticleComment;
import edu.esprit.tex.services.adminstration.administrator.comment.ArticleComenntsLocal;

@ManagedBean(name = "CommBean")
@ViewScoped
public class CommentBean implements Serializable {

	ArticleComment articleComment;
	List<ArticleComment> comments;
	@EJB
	ArticleComenntsLocal articleComenntsLocal;
	
	@ManagedProperty(value="#{articleBean.article}")
	private Article article ;

	

	public CommentBean() {
		articleComment = new ArticleComment();
	}

	@PostConstruct
	public void listcomment() {
		comments = articleComenntsLocal.findCommentByIdArtcile(article);
	}

	public ArticleComment getArticleComment() {
		return articleComment;
	}

	public void setArticleComment(ArticleComment articleComment) {
		this.articleComment = articleComment;
	}

	public List<ArticleComment> getComments() {
		return comments;
	}

	public void setComments(List<ArticleComment> comments) {
		this.comments = comments;
	}

	public String doCreate() {
		String navigateTo = null;
		ArticleComment coment = new ArticleComment(
		articleComment.getDescription(), articleComment.getUser(),
		articleComment.getArticle());
		articleComenntsLocal.CreateComment(coment);
		comments = articleComenntsLocal.findCommentByIdArtcile(article);
		return navigateTo;

	}
	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

}
