package edu.esprit.tex.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Conversion
 *
 */
@Entity

public class Conversion implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int id;
	private int tokenNumber;
	private float value;
	private float percentage;
	private Date date;
	
	private List<Business> businessConversion;

	public Conversion() {
		super();
	}   
	@Id    
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public int getTokenNumber() {
		return this.tokenNumber;
	}

	public void setTokenNumber(int tokenNumber) {
		this.tokenNumber = tokenNumber;
	}   
	public float getValue() {
		return this.value;
	}

	public void setValue(float value) {
		this.value = value;
	}   
	public float getPercentage() {
		return this.percentage;
	}

	public void setPercentage(float percentage) {
		this.percentage = percentage;
	}   
	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	@ManyToOne(targetEntity=Conversion.class)
	public List<Business> getBusinessConversion() {
		return businessConversion;
	}
	public void setBusinessConversion(List<Business> businessConversion) {
		this.businessConversion = businessConversion;
	}
	
   
}
