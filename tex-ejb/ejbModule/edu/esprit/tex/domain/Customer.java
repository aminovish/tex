package edu.esprit.tex.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import edu.esprit.tex.domain.User;

/**
 * Entity implementation class for Entity: Customer
 *
 */
@Entity

public class Customer extends User implements Serializable {

	
	private static final long serialVersionUID = 1L;
	private List<Post> postes;
	private List<Service> services;
	private List<PostComment> postComments;
	private List<CustomerTransaction> customerTransactions;

	

	public Customer() {
	
	}


	
    public Customer(String name, String email, String password, String adress,
			String phone, String city, int account, boolean state,Long point) {
    	super(name, email, password, adress, phone, city, account, state,point);
	
	}



	@OneToMany(mappedBy="customers")
	public List<Post> getPostes() {
		return postes;
	}

	public void setPostes(List<Post> postes) {
		this.postes = postes;
	}

	@OneToMany(mappedBy="customer")
	public List<Service> getServices() {
		return services;
	}

	public void setServices(List<Service> services) {
		this.services = services;
	}

	@OneToMany(mappedBy="customer")
	public List<PostComment> getPostComments() {
		return postComments;
	}

	public void setPostComments(List<PostComment> postComments) {
		this.postComments = postComments;
	}

	@OneToMany(mappedBy="customer")
	public List<CustomerTransaction> getCustomerTransactions() {
		return customerTransactions;
	}

	public void setCustomerTransactions(List<CustomerTransaction> customerTransactions) {
		this.customerTransactions = customerTransactions;
	}
	




	
   
}
