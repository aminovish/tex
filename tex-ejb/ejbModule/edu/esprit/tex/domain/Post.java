package edu.esprit.tex.domain;

import java.io.Serializable;
import java.lang.String;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Post
 *
 */
@Entity


public class Post implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int id;
	private String title;
	private String description;
	private int value;
	private Date date;
	private Customer customers;
	private List<PostComment> postComments;

	public Post(String title, String description, int value) {
	
		this.title = title;
		this.description = description;
		this.value = value;
		this.setDate(new Date());
	}
	public Post() {
		
	}   
	@Id    
	@GeneratedValue (strategy= GenerationType.IDENTITY)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}   
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}   
	public int getValue() {
		return this.value;
	}

	public void setValue(int value) {
		this.value = value;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	@ManyToOne(cascade = CascadeType.MERGE)
	public Customer getCustomers() {
		return customers;
	}
	public void setCustomers(Customer customers) {
		this.customers = customers;
	}
	
	@OneToMany(mappedBy="post")
	public List<PostComment> getPostComments() {
		return postComments;
	}
	public void setPostComments(List<PostComment> postComments) {
		this.postComments = postComments;
	}
   
}
