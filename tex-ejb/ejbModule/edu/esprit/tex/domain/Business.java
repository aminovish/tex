package edu.esprit.tex.domain;

import edu.esprit.tex.domain.User;
import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Business
 *
 */
@Entity

public class Business extends User implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private List<Service> services;
	private List<Conversion> conversion;
	private List<BusinessTransaction>businessTransactions;
	private Administrator admin;
	
	
	private String taxCode;
	private String description;

	public Business() {
	
	}
	

	public Business(String name, String email, String password, String adress,
			String phone, String city, int account, boolean state,	String taxCode,	String description,Administrator admin,Long point) {
		super(name, email, password, adress, phone, city, account, state,point);
		this.setTaxCode(taxCode);
		this.setDescription(description);
		this.admin=admin;
	}
	public Business(String name, String email, String password, String adress,
			String phone, String city, int account, boolean state,	String taxCode,	String description,Administrator admin,Long point,Picture picture) {
		super(name, email, password, adress, phone, city, account, state,point,picture);
		this.setTaxCode(taxCode);
		this.setDescription(description);
		this.admin=admin;
	}


	@OneToMany(mappedBy="business")
	public List<Service> getServices() {
		return services;
	}

	public void setServices(List<Service> services) {
		services = services;
	}

	@OneToMany( mappedBy="businessConversion")
	public List<Conversion> getConversion() {
		return conversion;
	}

	public void setConversion(List<Conversion> conversion) {
		this.conversion = conversion;
	}


	public String getTaxCode() {
		return taxCode;
	}


	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	@OneToMany( mappedBy="business")
	public List<BusinessTransaction> getBusinessTransactions() {
		return businessTransactions;
	}


	public void setBusinessTransactions(List<BusinessTransaction> businessTransactions) {
		this.businessTransactions = businessTransactions;
	}


	@ManyToOne(cascade = CascadeType.MERGE)
	public Administrator getAdmin() {
		return admin;
	}


	public void setAdmin(Administrator admin) {
		this.admin = admin;
	}
	
   
}
