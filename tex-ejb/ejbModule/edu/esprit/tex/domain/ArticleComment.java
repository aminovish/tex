package edu.esprit.tex.domain;

import java.io.Serializable;
import java.lang.String;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: ArticleComment
 *
 */
@Entity

public class ArticleComment implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private ArticleCommentFK articleCommentFK;
	private User user;
	private Article article;
	
	
	private String description;
	

	public ArticleComment( String description ,User user , Article article) {

		this.getArticleCommentFK().setIdArticle(article.getId());
		this.getArticleCommentFK().setIdUser(user.getId());
		this.description = description;
		this.user=user ;
		this.article=article;
		this.getArticleCommentFK().setDate(new Date());
	}
	public ArticleComment() {
		this.getArticleCommentFK().setDate(new Date());

	}   


  
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	@EmbeddedId
	public ArticleCommentFK getArticleCommentFK() {
		if( articleCommentFK == null)
			 articleCommentFK = new ArticleCommentFK();
		return articleCommentFK;
	}
	
	public void setArticleCommentFK(ArticleCommentFK articleCommentFK) {
		this.articleCommentFK = articleCommentFK;
	}
	
	@ManyToOne
	@JoinColumn(name="idUser" ,insertable=false,updatable=false)
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	@ManyToOne
	@JoinColumn(name="idArticle" ,insertable=false,updatable=false)
	public Article getArticle() {
		return article;
	}
	public void setArticle(Article article) {
		this.article = article;
	}
   
}
