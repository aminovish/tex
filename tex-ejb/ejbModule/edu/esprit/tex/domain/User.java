package edu.esprit.tex.domain;

import java.io.Serializable;
import java.lang.String;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: User
 *
 */
@Entity



public class User implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int id;
	private String name;
	private String email;
	private String password;
	private String adress;
	private String phone;
	private String city;
	private int rating;
	private Date dateSub;
	private int account;
	private boolean state;
	private Picture picture;
	private List<ArticleComment> userComment;
	private Long point;

	public User() {

	}   
	


	public User(String name, String email, String password, String adress,
			String phone, String city, int account, boolean state,Long point) {
		super();
		this.name = name;
		this.email = email;
		this.password = password;
		this.adress = adress;
		this.phone = phone;
		this.city = city;
		this.account = account;
		this.state = state;
		this.rating=0;
		this.dateSub=new Date();
		this.setPoint(point);
	}
	
	public User(String name, String email, String password, String adress,
			String phone, String city, int account, boolean state,Long point,Picture picture) {
		super();
		this.name = name;
		this.email = email;
		this.password = password;
		this.adress = adress;
		this.phone = phone;
		this.city = city;
		this.account = account;
		this.state = state;
		this.picture=picture;
		this.rating=0;
		this.dateSub=new Date();
		this.setPoint(point);
	}



	@Id 
    @GeneratedValue (strategy= GenerationType.IDENTITY)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}  
	@Column( unique= true )
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}   
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}   
	public String getAdress() {
		return this.adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}   
	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}   
	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@OneToOne(mappedBy="user",cascade=CascadeType.ALL)
	public Picture getPicture() {
		return picture;
	}

	public void setPicture(Picture picture) {
		this.picture = picture;
	}

	public boolean getState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	





	public int getAccount() {
		return account;
	}



	public void setAccount(int account) {
		this.account = account;
	}



	@OneToMany(mappedBy="user")
	public List<ArticleComment> getUserComment() {
		return userComment;
	}

	public void setUserComment(List<ArticleComment> userComment) {
		this.userComment = userComment;
	}



	public int getRating() {
		return rating;
	}



	public void setRating(int rating) {
		this.rating = rating;
	}



	public Date getDateSub() {
		return dateSub;
	}



	public void setDateSub(Date dateSub) {
		this.dateSub = dateSub;
	}



	public Long getPoint() {
		return point;
	}



	public void setPoint(Long point) {
		this.point = point;
	}
   
}
