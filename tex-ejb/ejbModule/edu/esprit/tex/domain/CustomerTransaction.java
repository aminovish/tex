package edu.esprit.tex.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: CustomerTransaction
 *
 */
@Entity

public class CustomerTransaction implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int id;
	private Date date;
	private Customer customer;
	private Service service;

	public CustomerTransaction() {
	
	}   
	@Id    
	@GeneratedValue (strategy= GenerationType.IDENTITY)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	@ManyToOne(cascade = CascadeType.MERGE)
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	@OneToOne
	public Service getService() {
		return service;
	}
	public void setService(Service service) {
		this.service = service;
	}
   
}
