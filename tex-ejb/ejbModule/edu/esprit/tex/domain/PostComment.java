package edu.esprit.tex.domain;

import java.io.Serializable;
import java.lang.String;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: PostComment
 *
 */
@Entity

public class PostComment implements Serializable {

	private static final long serialVersionUID = 1L;
	   
	private PostCommentFK commentFK;
    private Customer customer;
    private Post post;
	
	private String description;
	private int value;
    private Date date;
	public PostComment() {
	
	}   
	
	
	public PostComment(Customer customer,Post post,String description, int value) {
		this.getCommentFK().setIdCustomer(customer.getId());
		this.getCommentFK().setIdPost(post.getId());
		this.setCustomer(customer);
		this.setPost(post);
		this.description = description;
		this.value = value;
		this.setDate(new Date());
	}

	   
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}   
	public int getValue() {
		return this.value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@EmbeddedId
	public PostCommentFK getCommentFK() {
		if(commentFK==null)
			commentFK = new PostCommentFK();
		return commentFK;
	}

	public void setCommentFK(PostCommentFK commentFK) {
		this.commentFK = commentFK;
	}
	
	@ManyToOne
	@JoinColumn(name="idCustomer" ,referencedColumnName="id",insertable=false,updatable=false)
	
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@ManyToOne
	@JoinColumn(name="idPost" ,referencedColumnName="id",insertable=false,updatable=false)
	
	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}
	
	
   
}
