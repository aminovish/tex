package edu.esprit.tex.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ArticleCommentFK implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private int idUser;
	private int idArticle;
	private Date date;

	public ArticleCommentFK() {
		
	}

	@Column( name = "idUser")
	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	@Column( name = "idArticle")
	public int getIdArticle() {
		return idArticle;
	}

	public void setIdArticle(int idArticle) {
		this.idArticle = idArticle;
	}

	public ArticleCommentFK(int idUser, int idArticle) {
	
		this.idUser = idUser;
		this.idArticle = idArticle;
	}




	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + idArticle;
		result = prime * result + idUser;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArticleCommentFK other = (ArticleCommentFK) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (idArticle != other.idArticle)
			return false;
		if (idUser != other.idUser)
			return false;
		return true;
	}
	
	

}
