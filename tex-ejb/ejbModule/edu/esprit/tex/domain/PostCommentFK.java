package edu.esprit.tex.domain;

import java.io.Serializable;

import javax.persistence.Embeddable;


@Embeddable
public class PostCommentFK implements Serializable{
	private static final long serialVersionUID = 1L;
	private int idCustomer;
	private int idPost;
	
	
	
	public PostCommentFK(int idCustomer, int idPost) {
		super();
		this.idCustomer = idCustomer;
		this.idPost = idPost;
	}

	public int getIdCustomer() {
		return idCustomer;
	}

	public void setIdCustomer(int idCustomer) {
		this.idCustomer = idCustomer;
	}

	public int getIdPost() {
		return idPost;
	}

	public void setIdPost(int idPost) {
		this.idPost = idPost;
	}



	public PostCommentFK() {
	
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idCustomer;
		result = prime * result + idPost;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PostCommentFK other = (PostCommentFK) obj;
		if (idCustomer != other.idCustomer)
			return false;
		if (idPost != other.idPost)
			return false;
		return true;
	}
	
	

}
