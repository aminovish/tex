package edu.esprit.tex.domain;

import java.io.Serializable;
import java.lang.String;
import java.util.Date;
import java.util.List;

import javax.persistence.*;


/**
 * Entity implementation class for Entity: Article
 *
 */
@Entity



public class Article implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int id;
	private String title;
	private String description;
	private Date date;
	private Administrator admin;
	private ArticlePicture articlePicture;
	private List<ArticleComment> articleComments;
	

	public Article() {
		
	}
	public Article(String title, String description) {

		
		this.title = title;
		this.description = description;
		this.date = new Date();
	}
	
	public Article(String title, String description,ArticlePicture articlePicture) {

		
		this.title = title;
		this.description = description;
		this.date = new Date();
		this.articlePicture=articlePicture;
	}
   
	@Id 
    @GeneratedValue (strategy= GenerationType.IDENTITY)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}   
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	@ManyToOne(cascade = CascadeType.MERGE)
	public Administrator getAdmin() {
		return admin;
	}
	public void setAdmin(Administrator admin) {
		this.admin = admin;
	}
	@OneToMany(mappedBy="article")
	public List<ArticleComment> getArticleComments() {
		return articleComments;
	}
	public void setArticleComments(List<ArticleComment> articleComments) {
		this.articleComments = articleComments;
	}
	@OneToOne(mappedBy="article",cascade=CascadeType.ALL)
	public ArticlePicture getArticlePicture() {
		return articlePicture;
	}
	public void setArticlePicture(ArticlePicture articlePicture) {
		this.articlePicture = articlePicture;
	}
   
}
