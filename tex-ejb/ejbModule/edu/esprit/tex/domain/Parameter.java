package edu.esprit.tex.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Parameter
 *
 */
@Entity

public class Parameter implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int id;
	private float tokenValue;
	private int tokenNumber;
	private float percentage;
	private int maxToken;

	public Parameter() {
	
	}   
	
	public Parameter(int tokenValue, int tokenNumber, int percentage,
			int maxToken) {
		
		this.tokenValue = tokenValue;
		this.tokenNumber = tokenNumber;
		this.percentage = percentage;
		this.maxToken = maxToken;
	}

	@Id    
	@GeneratedValue (strategy= GenerationType.IDENTITY)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public float getTokenValue() {
		return this.tokenValue;
	}

	public void setTokenValue(float tokenValue) {
		this.tokenValue = tokenValue;
	}   
	public int getTokenNumber() {
		return this.tokenNumber;
	}

	public void setTokenNumber(int tokenNumber) {
		this.tokenNumber = tokenNumber;
	}   
	public float getPercentage() {
		return this.percentage;
	}

	public void setPercentage(float percentage) {
		this.percentage = percentage;
	}   
	public int getMaxToken() {
		return this.maxToken;
	}

	public void setMaxToken(int maxToken) {
		this.maxToken = maxToken;
	}
   
}
