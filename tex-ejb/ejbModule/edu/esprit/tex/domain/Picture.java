package edu.esprit.tex.domain;

import java.io.Serializable;
import java.lang.Double;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Picture
 *
 */
@Entity

public class Picture implements Serializable {

	
	private int id;
	private byte[] picturee;
	private Double sizePicture;
	private User user;
	private static final long serialVersionUID = 1L;

	public Picture() {
		super();
	}   
	public Picture(byte[] picturee, Double sizePicture) {
		super();
		this.picturee = picturee;
		this.sizePicture = sizePicture;
	}
	@Id    
	@GeneratedValue (strategy= GenerationType.IDENTITY)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	} 
	@Lob
	public byte[] getPicturee() {
		return this.picturee;
	}

	public void setPicturee(byte[] picturee) {
		this.picturee = picturee;
	}   
	public Double getSizePicture() {
		return this.sizePicture;
	}

	public void setSizePicture(Double sizePicture) {
		this.sizePicture = sizePicture;
	}
	@OneToOne (cascade=CascadeType.ALL)
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
   
}
