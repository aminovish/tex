package edu.esprit.tex.domain;

import edu.esprit.tex.domain.User;
import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Administrator
 *
 */
@Entity

public class Administrator extends User implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	private List<Article> articles;
	private List<Business> businesses;

	public Administrator() {
	
	}


	public Administrator(String name, String email, String password,
			String adress, String phone, String city, int account, boolean state, Long point) {
		super(name, email, password, adress, phone, city, account, state,point);
	
	}


	@OneToMany(mappedBy= "admin")
	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}


	@OneToMany(mappedBy= "admin")
	public List<Business> getBusinesses() {
		return businesses;
	}


	public void setBusinesses(List<Business> businesses) {
		this.businesses = businesses;
	}
	
   
}
