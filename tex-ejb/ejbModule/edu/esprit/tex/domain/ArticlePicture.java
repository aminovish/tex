package edu.esprit.tex.domain;

import java.io.Serializable;
import java.lang.Double;
import javax.persistence.*;



/**
 * Entity implementation class for Entity: Picture
 *
 */
@Entity



public class ArticlePicture implements Serializable {

	
	private int id;
	private byte[] picturee;
	private Double sizePicture;
	private Article article;
	private static final long serialVersionUID = 1L;

	public ArticlePicture() {
		super();
	}   
	public ArticlePicture(byte[] picturee, Double sizePicture) {
		super();
		this.picturee = picturee;
		this.sizePicture = sizePicture;
	}
	@Id    
	@GeneratedValue (strategy= GenerationType.IDENTITY)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	} 
	@Lob
	public byte[] getPicturee() {
		return this.picturee;
	}

	public void setPicturee(byte[] picturee) {
		this.picturee = picturee;
	}   
	public Double getSizePicture() {
		return this.sizePicture;
	}

	public void setSizePicture(Double sizePicture) {
		this.sizePicture = sizePicture;
	}
	@OneToOne
	public Article getArticle() {
		return article;
	}
	public void setArticle(Article article) {
		this.article = article;
	}
   
}
