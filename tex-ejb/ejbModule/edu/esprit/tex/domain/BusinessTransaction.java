package edu.esprit.tex.domain;

import java.io.Serializable;
import java.util.Date;


import javax.persistence.*;

/**
 * Entity implementation class for Entity: BusinessTransaction
 *
 */
@Entity

public class BusinessTransaction implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int id;
	private Date date;
	private Service service;
	private Business business;
	private int value;
	private Boolean showReceiver;
	private Boolean showSeeder;
	private int validate;
	private String  sCode;

	public BusinessTransaction() {
	
	}   
	
	
	public BusinessTransaction( Service service, Business business) {
		
		this.date = new Date();
		this.service = service;
		this.business = business;
		this.showReceiver=false;
		this.showSeeder=false;
		this.value=service.getValue();
		this.sCode=null;
		this.validate=1;
	}


	@Id    
	@GeneratedValue (strategy= GenerationType.IDENTITY)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	@OneToOne
	public Service getService() {
		return service;
	}
	public void setService(Service service) {
		this.service = service;
	}
	

	@ManyToOne(cascade = CascadeType.MERGE)
	public Business getBusiness() {
		return business;
	}
	public void setBusiness(Business business) {
		this.business = business;
	}



	public int getValue() {
		return value;
	}


	public void setValue(int value) {
		this.value = value;
	}


	public Boolean getShowSeeder() {
		return showSeeder;
	}


	public void setShowSeeder(Boolean showSeeder) {
		this.showSeeder = showSeeder;
	}


	public Boolean getShowReceiver() {
		return showReceiver;
	}


	public void setShowReceiver(Boolean showReceiver) {
		this.showReceiver = showReceiver;
	}


	public int getValidate() {
		return validate;
	}


	public void setValidate(int validate) {
		this.validate = validate;
	}


	public String getsCode() {
		return sCode;
	}


	public void setsCode(String sCode) {
		this.sCode = sCode;
	}
   
}
