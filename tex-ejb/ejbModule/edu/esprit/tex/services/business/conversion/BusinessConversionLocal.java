package edu.esprit.tex.services.business.conversion;

import java.util.List;

import javax.ejb.Local;

import edu.esprit.tex.domain.Conversion;

@Local
public interface BusinessConversionLocal {

	List<Conversion> findAllTConversion();

	void addTransactToken(Conversion conversion);

}
