package edu.esprit.tex.services.business.conversion;

import javax.ejb.Remote;

import edu.esprit.tex.domain.Conversion;

@Remote
public interface BusinessConversionRemote {
	
	void createConversion(Conversion conversion);
	void updateConversion(Conversion conversion);
	void deleteConversion(Conversion conversion);

}
