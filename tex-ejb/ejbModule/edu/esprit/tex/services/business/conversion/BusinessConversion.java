package edu.esprit.tex.services.business.conversion;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import edu.esprit.tex.domain.Conversion;

/**
 * Session Bean implementation class BusinessConversion
 */
@Stateless

public class BusinessConversion implements BusinessConversionRemote, BusinessConversionLocal {


    @PersistenceContext
    private EntityManager em;
	public void createConversion(Conversion conversion) {
		
		em.persist(conversion);
		
	}
	
	public void updateConversion(Conversion conversion)

	{
		em.merge(conversion);
	}
	
	public void deleteConversion(Conversion conversion) {

		em.remove(em.merge(conversion));
		
	}
	
public List<Conversion> findAllTConversion() {
		
		return em.createQuery("select c from Conversion c").getResultList();
}

public List<Conversion> findByDate(Date date) {

	List<Conversion> tr = null;
	String jpql = "select c from Conversion t where c.date=:z";
	Query query = em.createQuery(jpql);
	query.setParameter("z", date);
	tr = query.getResultList();
	return tr;
}

@Override
public void addTransactToken(Conversion conversion) {
	// TODO Auto-generated method stub
	
}

}

