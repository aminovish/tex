package edu.esprit.tex.services.business.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import edu.esprit.tex.domain.Business;
import edu.esprit.tex.domain.Service;

/**
 * Session Bean implementation class BusinessServices
 */
@Stateless

public class BusinessServices implements BusinessServicesRemote, BusinessServicesLocal {

	@PersistenceContext
    EntityManager em;

	public void createService(Service service) {

		em.persist(service);
	}


	public void updateService(Service service) {
		
		em.merge(service);
	}

	@Override
	public void deleteService(Service service) {

		em.remove(em.merge(service));
		
	}



	public List<Service> findByValueAndName(int value, String name) {
		Query query = em.createQuery("SELECT s FROM Service s WHERE s.value=:v and s.name=:n");
		query.setParameter("v", value);
		query.setParameter("n", name);
	    return query.getResultList();
	}



	public Service findById(int id) {
		Service service;
		service = em.find(Service.class, id);
		return service;
	}


	@Override
	public List<Service> findServicesByBusiness(Business business) {
		Query query = em.createQuery("select s from Service s where s.business=:b");
		query.setParameter("b", business);
		return query.getResultList();
	}

   
}

