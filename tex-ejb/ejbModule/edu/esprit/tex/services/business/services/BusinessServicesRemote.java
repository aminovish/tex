package edu.esprit.tex.services.business.services;

import java.util.List;

import javax.ejb.Remote;

import edu.esprit.tex.domain.Business;
import edu.esprit.tex.domain.Service;

@Remote
public interface BusinessServicesRemote {
	
	void createService(Service service);
	void  updateService(Service service);
	void  deleteService(Service service);
	List<Service >findServicesByBusiness(Business business);
	List <Service>findByValueAndName(int value, String name);
	Service findById(int id);


}
