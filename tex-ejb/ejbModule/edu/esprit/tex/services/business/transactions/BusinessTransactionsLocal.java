package edu.esprit.tex.services.business.transactions;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import edu.esprit.tex.domain.Business;
import edu.esprit.tex.domain.BusinessTransaction;
import edu.esprit.tex.domain.Service;

@Local
public interface BusinessTransactionsLocal {

	void createTransaction(Business business , Service service);
	List<BusinessTransaction> findAllTransaction ();
	List<BusinessTransaction>findByDate(Date date);
	void updateTransaction(BusinessTransaction businessTransaction);

}
