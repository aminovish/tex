package edu.esprit.tex.services.business.transactions;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;




import edu.esprit.tex.domain.Business;
import edu.esprit.tex.domain.BusinessTransaction;
import edu.esprit.tex.domain.Service;

/**
 * Session Bean implementation class BusinessTransactions
 */
@Stateless

public class BusinessTransactions implements BusinessTransactionsRemote, BusinessTransactionsLocal {
	
	@PersistenceContext
	private EntityManager em;

	public void createTransaction(Business business , Service service) {
		
		BusinessTransaction transaction=new BusinessTransaction(service, business);

		em.persist(transaction);
	}

	public List<BusinessTransaction> findAllTransaction() {
		
		return em.createQuery("select t from BusinessTransaction t").getResultList();

		/*List<BusinessTransaction> tr = null;
		String jpql = "select t from Transaction t";
		Query query = em.createQuery(jpql);
		tr = query.getResultList();
		return tr;*/
	}

	public List<BusinessTransaction> findByDate(Date date) {

		List<BusinessTransaction> tr = null;
		String jpql = "select t from BusinessTransaction t where t.date=:z";
		Query query = em.createQuery(jpql);
		query.setParameter("z", date);
		tr = query.getResultList();
		return tr;
	}


	public void updateTransaction(BusinessTransaction businessTransaction) {
		em.merge(businessTransaction);
		
	}



}
