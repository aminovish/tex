package edu.esprit.tex.services.business.transactions;

import java.util.Date;
import java.util.List;
import javax.ejb.Remote;
import edu.esprit.tex.domain.BusinessTransaction;



@Remote
public interface BusinessTransactionsRemote {
	

	List<BusinessTransaction> findAllTransaction ();
	List<BusinessTransaction>findByDate(Date date);


}
