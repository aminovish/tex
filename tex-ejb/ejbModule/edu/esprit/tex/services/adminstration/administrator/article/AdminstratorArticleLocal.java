package edu.esprit.tex.services.adminstration.administrator.article;

import java.util.List;

import javax.ejb.Local;

import edu.esprit.tex.domain.Article;

@Local
public interface AdminstratorArticleLocal {
	
    void CreateArticle(Article art);
    void UpdateArticle(Article art);
    void DeleteArticle(Article art);
    Article FindByIdArticle(int id);
    List<Article> FindAllarticle();

}
