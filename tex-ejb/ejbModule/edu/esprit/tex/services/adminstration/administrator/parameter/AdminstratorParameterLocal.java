package edu.esprit.tex.services.adminstration.administrator.parameter;

import javax.ejb.Local;

import edu.esprit.tex.domain.Parameter;

@Local
public interface AdminstratorParameterLocal {
	void CreatePrameter(Parameter parameter);
    void UpdatePrameter(Parameter parameter);
    Parameter FindParameter();


}
