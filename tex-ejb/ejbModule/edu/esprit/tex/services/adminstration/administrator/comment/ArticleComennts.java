package edu.esprit.tex.services.adminstration.administrator.comment;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import edu.esprit.tex.domain.Article;
import edu.esprit.tex.domain.ArticleComment;

/**
 * Session Bean implementation class ArticleComennts
 */
@Stateless

public class ArticleComennts implements ArticleComenntsRemote, ArticleComenntsLocal {

	@PersistenceContext
	private EntityManager em ;
	
    public ArticleComennts() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void CreateComment(ArticleComment comt) {
	
		em.persist(comt);
	}

	@Override
	public void ApdaateComment(ArticleComment comt) {
		
		em.merge(comt);
	}

	@Override
	public void DeleteComment(ArticleComment comt) {
		em.remove(em.merge(comt));
		
	}

	@Override
	public ArticleComment FindByIdDate(int id) {

		return em.find(ArticleComment.class, id);
	}

	@Override
	public List<ArticleComment> findAllComment() {
		List<ArticleComment> articleComments;
		String jpql ="select A from ArticleComment A" ;
		Query query =em.createQuery(jpql);
		articleComments=query.getResultList();
		return articleComments;
	}

	@Override
	public List<ArticleComment> findCommentByIdArtcile(Article ar) {
		List<ArticleComment> articleComments;
		String jpql ="select A from ArticleComment A where A.article=:x " ;
		Query query =em.createQuery(jpql).setParameter("x", ar);
		articleComments=query.getResultList();
		return articleComments;
	}

}
