package edu.esprit.tex.services.adminstration.administrator.article;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import edu.esprit.tex.domain.Article;

/**
 * Session Bean implementation class AdminstratorArticle
 */
@Stateless
public class AdminstratorArticle implements AdminstratorArticleRemote, AdminstratorArticleLocal {

	@PersistenceContext
  private EntityManager em ;

    public AdminstratorArticle() {
       
    }

	
	public void CreateArticle(Article art) {
		em.persist(art);
		
	}

	
	public void UpdateArticle(Article art) {
		Article managedOne= em.merge(art);
		
	}

	
	public void DeleteArticle(Article art) {
		em.remove(em.merge(art));
		
	}

	
	public Article FindByIdArticle(int id) {
		
		return em.find(Article.class, id);
	}

	
	public List<Article> FindAllarticle() {
		
		List<Article> articles= null;
		String jpql ="select A from Article A" ;
		Query query =em.createQuery(jpql);
		articles =query.getResultList();
		
		
		return articles;
	}

}
