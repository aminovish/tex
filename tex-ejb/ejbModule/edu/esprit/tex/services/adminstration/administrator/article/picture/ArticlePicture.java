package edu.esprit.tex.services.adminstration.administrator.article.picture;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Session Bean implementation class ArticlePicture
 */
@Stateless
public class ArticlePicture implements ArticlePictureRemote,
		ArticlePictureLocal {

	@PersistenceContext
	EntityManager em;

	public ArticlePicture() {

	}

	public void createArticlePicture(
			edu.esprit.tex.domain.ArticlePicture articlePicture) {
		em.persist(articlePicture);

	}

	public void updateArticlePicture(
			edu.esprit.tex.domain.ArticlePicture articlePicture) {
		em.merge(articlePicture);

	}

	public void deleteArticlePicture(
			edu.esprit.tex.domain.ArticlePicture articlePicture) {
		em.remove(em.merge(articlePicture));

	}

	public edu.esprit.tex.domain.ArticlePicture getArticlePictureById(int id) {
		// TODO Auto-generated method stub
		return em.find(edu.esprit.tex.domain.ArticlePicture.class, id);
	}

	public List<edu.esprit.tex.domain.ArticlePicture> getAllPictureArticle() {
		// TODO Auto-generated method stub
		return em.createQuery("SELECT p FROM ArticlePicture p",
				edu.esprit.tex.domain.ArticlePicture.class).getResultList();
		
	}

}
