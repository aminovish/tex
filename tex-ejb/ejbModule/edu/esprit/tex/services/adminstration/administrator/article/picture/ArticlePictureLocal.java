package edu.esprit.tex.services.adminstration.administrator.article.picture;

import java.util.List;

import javax.ejb.Local;

import edu.esprit.tex.domain.ArticlePicture;

@Local
public interface ArticlePictureLocal {
	public void createArticlePicture(ArticlePicture articlePicture);
	public void updateArticlePicture(ArticlePicture articlePicture);
	public void deleteArticlePicture(ArticlePicture articlePicture);
	public ArticlePicture getArticlePictureById(int id);
	public List<ArticlePicture> getAllPictureArticle();

}
