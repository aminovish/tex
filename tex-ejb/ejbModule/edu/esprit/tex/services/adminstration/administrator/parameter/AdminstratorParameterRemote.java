package edu.esprit.tex.services.adminstration.administrator.parameter;


import javax.ejb.Remote;
import edu.esprit.tex.domain.Parameter;

@Remote
public interface AdminstratorParameterRemote {
    void CreatePrameter(Parameter parameter);
    void UpdatePrameter(Parameter parameter);
    Parameter FindParameter();

}
