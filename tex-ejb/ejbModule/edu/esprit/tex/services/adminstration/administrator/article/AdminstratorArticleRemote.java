package edu.esprit.tex.services.adminstration.administrator.article;

import java.util.List;

import javax.ejb.Remote;

import edu.esprit.tex.domain.Article;

@Remote
public interface AdminstratorArticleRemote {
    void CreateArticle(Article art);
    void UpdateArticle(Article art);
    void DeleteArticle(Article art);
    Article FindByIdArticle(int id);
    List<Article> FindAllarticle();


}
