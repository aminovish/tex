package edu.esprit.tex.services.adminstration.administrator.comment;

import java.util.List;

import javax.ejb.Remote;

import edu.esprit.tex.domain.Article;
import edu.esprit.tex.domain.ArticleComment;

@Remote
public interface ArticleComenntsRemote {
	public void CreateComment(ArticleComment comt);
	public void ApdaateComment(ArticleComment comt);
	public void DeleteComment(ArticleComment comt);
	public ArticleComment FindByIdDate(int id);
	public List<ArticleComment> findAllComment();
	public List<ArticleComment> findCommentByIdArtcile(Article ar);

}
