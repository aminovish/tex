package edu.esprit.tex.services.adminstration.administrator.parameter;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.esprit.tex.domain.Parameter;

/**
 * Session Bean implementation class AdminstratorParameter
 */
@Stateless

public class AdminstratorParameter implements AdminstratorParameterRemote, AdminstratorParameterLocal {

	@PersistenceContext
	private EntityManager em ;

	public void CreatePrameter(Parameter parameter) {
		em.persist(parameter);
		
	}

	
	public void UpdatePrameter(Parameter parameter) {

		em.merge(parameter);
		
	}


	public Parameter FindParameter() {
		Parameter parameter;
		parameter = em.find(Parameter.class, 1);
		return parameter;
		
		
	}

}
