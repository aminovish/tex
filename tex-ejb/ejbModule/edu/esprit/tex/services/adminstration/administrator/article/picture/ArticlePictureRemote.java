package edu.esprit.tex.services.adminstration.administrator.article.picture;

import java.util.List;

import javax.ejb.Remote;

import edu.esprit.tex.domain.ArticlePicture;
import edu.esprit.tex.domain.Picture;

@Remote
public interface ArticlePictureRemote {
	
	public void createArticlePicture(ArticlePicture articlePicture);
	public void updateArticlePicture(ArticlePicture articlePicture);
	public void deleteArticlePicture(ArticlePicture articlePicture);
	public ArticlePicture getArticlePictureById(int id);
	public List<ArticlePicture> getAllPictureArticle();
	

}
