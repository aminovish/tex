package edu.esprit.tex.services.adminstration.user;

import java.util.List;

import javax.ejb.Remote;

import edu.esprit.tex.domain.Administrator;
import edu.esprit.tex.domain.Business;
import edu.esprit.tex.domain.Customer;
import edu.esprit.tex.domain.User;

@Remote
public interface UsersRemote {
	
	void create(User user);
	void update(User user);
	void delete(User user);	
	User authenticate(String login,String password );	
	List<User> findUsers();
	User findUserById(int id);
	void createAdministrator(Administrator administrator);
	void updateAdministrator(Administrator administrator);
	void deleteAdministrator(Administrator administrator);
	List<Administrator> findAdministrators();
	Administrator findAdministratorById(int id);
	void createBusiness(Business business,Administrator administrator);
	void updateBusiness(Business business);
	void deleteBusiness(Business business);
	List<Business> findBusinesss();
	Business findBusinessById(int id);
	void createCustomer(Customer customer);
	void updateCustomer(Customer customer);
	void deleteCustomer(Customer customer);
	List<Customer> findCustomers();
	Customer findCustomerById(int id);

}
