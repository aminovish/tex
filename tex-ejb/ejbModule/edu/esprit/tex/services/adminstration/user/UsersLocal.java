package edu.esprit.tex.services.adminstration.user;

import java.util.List;

import javax.ejb.Local;

import edu.esprit.tex.domain.Administrator;
import edu.esprit.tex.domain.Business;
import edu.esprit.tex.domain.BusinessTransaction;
import edu.esprit.tex.domain.Customer;
import edu.esprit.tex.domain.Service;
import edu.esprit.tex.domain.User;
import edu.esprit.tex.services.business.transactions.BusinessTransactions;

@Local
public interface UsersLocal {
	void create(User user);
	void update(User user);
	void delete(User user);	
	User authenticate(String login,String password );	
	List<User> findUsers();
	User findUserById(int id);
	List<Business> findUsersByUser(Administrator b);
	List<Service> findAllService();
	List<Service> findUserService(User user);
	void createService(Service service);
	void updateService(Service service);
	void deleteService(Service service);
	void updateBusiness(Business business);
	List<User> searchuser(String servicename, int servicevalue, String city,User user);
	Number countNotification(User user);
	List<BusinessTransaction> findAllBusinessTransactions(User user);
	List<BusinessTransaction> showBusinessTransactions(User user);
	boolean findexitmail(String mail);
	Number countAcceptedNotification(User user);
	public List<BusinessTransaction> showAcceptedBusinessTransactions(User user);
	public List<BusinessTransaction> showNewAcceptedBusinessTransactions(User user);
	public List<BusinessTransaction> findForCodeBusinessTransactions(User user);

}
