package edu.esprit.tex.services.adminstration.user;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import edu.esprit.tex.domain.Administrator;
import edu.esprit.tex.domain.Business;
import edu.esprit.tex.domain.BusinessTransaction;
import edu.esprit.tex.domain.Customer;
import edu.esprit.tex.domain.Service;
import edu.esprit.tex.domain.User;
import edu.esprit.tex.services.business.transactions.BusinessTransactions;

/**
 * Session Bean implementation class Users
 */
@Stateless

public class Users implements UsersRemote, UsersLocal {

    @PersistenceContext
    private EntityManager em;
	
    public Users() {
       
    }


	public void create(User user) {
		em.persist(user);
				
	}


	public void update(User user) {
		em.merge(user);
	}


	public void delete(User user) {
		em.remove(em.merge(user));		
	}

	
	public User authenticate(String email, String password) {
		User found=null;
		
		String jpql="select u from User u where u.email=:x and u.password=:y";
		
		Query query =em.createQuery(jpql);
		query.setParameter("x", email);
		query.setParameter("y", password);
		try{
			found=(User) query.getSingleResult();
		}catch (Exception e) {
				Logger.getLogger(Users.class.getName()).log(Level.WARNING,"failed authentification :"+ email);
				}
		
		
		return found;
	}


	
	public List<User> findUsers() {
		
		return em.createQuery("select u from User u", User.class).getResultList(); 
	}


	
	public User findUserById(int id) {
		
		return em.find(User.class, id);
	}


	
	public void createAdministrator(Administrator administrator) {
		em.persist(administrator);
		
	}


	
	public void updateAdministrator(Administrator administrator) {
		em.merge(administrator);
		
	}


	
	public void deleteAdministrator(Administrator administrator) {
		em.remove(em.merge(administrator));
		
	}


	
	public List<Administrator> findAdministrators() {
		return em.createQuery("select a from Administrator a", Administrator.class).getResultList(); 
		
	}


	
	public Administrator findAdministratorById(int id) {
		return em.find(Administrator.class, id);
	}


	
	public void createBusiness(Business business) {
		em.persist(business);
		
	}


	
	public void updateBusiness(Business business) {
		em.merge(business);
		
	}


	
	public void deleteBusiness(Business business) {
		em.remove(em.merge(business));
		
	}


	
	public List<Business> findBusinesss() {
		return em.createQuery("select b from Business b", Business.class).getResultList(); 
		
	}


	
	public Business findBusinessById(int id) {
		return em.find(Business.class, id);
	}



	public void createCustomer(Customer customer) {
		em.persist(customer);
		
	}


	
	public void updateCustomer(Customer customer) {
		em.merge(customer);
		
	}


	
	public void deleteCustomer(Customer customer) {
		em.remove(em.merge(customer));
		
	}


	
	public List<Customer> findCustomers() {
		return em.createQuery("select b from Customer b", Customer.class).getResultList(); 
		
	}


	
	public Customer findCustomerById(int id) {
		return em.find(Customer.class, id);
	}


	public void createBusiness(Business business, Administrator administrator) {
		business.setAdmin(administrator);
		em.persist(business);
		
	}
	
	public List<Service> findAllService() {
		return em.createQuery("select s from Service s", Service.class).getResultList(); 
		
	}
	

	
	@SuppressWarnings("unchecked")
	public List<Service> findUserService(User user) {
		if (user instanceof Business) {
			return em.createQuery("select s from Service s where s.business=:u").setParameter("u", user).getResultList();
			
		} else {

		
		return em.createQuery("select s from Service s where s.customer=:u").setParameter("u", user).getResultList();
		}	
	}
	
	public void createService(Service service) {

		em.persist(service);
	}


	public void updateService(Service service) {
		
		em.merge(service);
	}

	public void deleteService(Service service) {

		em.remove(em.merge(service));
	}





	public List<User> searchuser(String servicename, int serviceValue,String city,User user) {
		
		List<User> users = null ;
		if(user instanceof Business){
			Query query=em.createQuery("select DISTINCT b from Business b join b.services s where b.id != :user and s.value=:v OR s.name=:name OR b.city=:city OR s.value=:v and s.name=:name and b.city=:city OR s.value=:v and s.name=:name OR s.name=:name and b.city=:city ");
			query.setParameter("v", serviceValue);
			query.setParameter("name",servicename);
			query.setParameter("user",user.getId());
			query.setParameter("city",city);
			users=query.getResultList();		
		}else if(user instanceof Business){
			Query query=em.createQuery("select DISTINCT c from Customer c join c.services s where s.value=:v OR s.name=:name OR c.city=:city OR s.value=:v and s.name=:name and c.city=:city OR s.value=:v and s.name=:name OR s.name=:name and c.city=:city ");
			query.setParameter("v", serviceValue);
			query.setParameter("name",servicename);
			query.setParameter("city",city);
			users=query.getResultList();				
		}

		return users;
	}


	@Override
	public Number countNotification(User user) {

		Query query=em.createQuery("select count(b) from BusinessTransaction b join b.service s where s.business=:u and b.showReceiver= false and b.validate > 0");
		query.setParameter("u", user);

		Number a=0;
		a=(Number)query.getSingleResult();
		System.out.println("a="+a);
		return  a;
	}



	@SuppressWarnings("unchecked")
	public List<BusinessTransaction> findAllBusinessTransactions(User user) {
		
		List<BusinessTransaction> businessTransactions = null ;
		Query query=em.createQuery("select b from BusinessTransaction b join b.service s where s.business=:u and b.showReceiver= false and b.validate > 0");
		query.setParameter("u", user);

		businessTransactions=query.getResultList();
		return businessTransactions;
	}


	@Override
	public List<BusinessTransaction> showBusinessTransactions(User user) {
		List<BusinessTransaction> businessTransactions = null ;
		Query query=em.createQuery("select b from BusinessTransaction b join b.service s where s.business=:u and b.showReceiver= true and b.validate > 0");
		query.setParameter("u", user);

		businessTransactions=query.getResultList();
		return businessTransactions;
	}


	@Override
	public boolean findexitmail(String mail) {
		boolean exists = false;
		String jpql = "select case when (count(u) > 0)  then true else false end from User u where u.email=:mail";
		Query query = em.createQuery(jpql);
		query.setParameter("mail", mail);
		exists = (Boolean)query.getSingleResult();
		return exists;
	}


	@Override
	public Number countAcceptedNotification(User user) {
		Query query=em.createQuery("select count(b) from BusinessTransaction b  where b.business=:u and b.showSeeder= false and b.validate > 0");
		query.setParameter("u", user);
		Number a=0;
		a=(Number)query.getSingleResult();
		System.out.println("a="+a);
		return  a;
		
	}
	
	public List<BusinessTransaction> showAcceptedBusinessTransactions(User user) {
		List<BusinessTransaction> businessTransactions = null ;
		Query query=em.createQuery("select b from BusinessTransaction b where b.business=:u and b.showSeeder= true and b.validate > 0");
		query.setParameter("u", user);

		businessTransactions=query.getResultList();
		return businessTransactions;
	}
	
	public List<BusinessTransaction> showNewAcceptedBusinessTransactions(User user) {
		List<BusinessTransaction> businessTransactions = null ;
		Query query=em.createQuery("select b from BusinessTransaction b where b.business=:u and b.showSeeder= false and b.validate > 0");
		query.setParameter("u", user);

		businessTransactions=query.getResultList();
		return businessTransactions;
	}
	
	public List<BusinessTransaction> findForCodeBusinessTransactions(User user) {
		
		List<BusinessTransaction> businessTransactions = null ;
		Query query=em.createQuery("select b from BusinessTransaction b join b.service s where s.business=:u and b.showReceiver= true and b.validate > 0");
		query.setParameter("u", user);

		businessTransactions=query.getResultList();
		return businessTransactions;
	}


	public List<Business> findUsersByUser(Administrator b) {
		// TODO Auto-generated method stub
		List<Business> users;
		Query query = em
				.createQuery("select b from Business b where b.admin=:x and b.state=true");
		query.setParameter("x", b);
		users = query.getResultList();
		return users;
	}

}
