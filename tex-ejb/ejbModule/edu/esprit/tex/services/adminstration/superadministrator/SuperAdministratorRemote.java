package edu.esprit.tex.services.adminstration.superadministrator;

import java.util.List;

import javax.ejb.Remote;

import edu.esprit.tex.domain.Administrator;

@Remote
public interface SuperAdministratorRemote {
void createAd(Administrator ad);
void updateAd(Administrator ad);
void deleteAd(Administrator ad);
Administrator   FindAdById(int id);
List<Administrator> FindAllAd();

}
