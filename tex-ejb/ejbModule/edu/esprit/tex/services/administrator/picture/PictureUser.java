package edu.esprit.tex.services.administrator.picture;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.esprit.tex.domain.Picture;

@Stateless
public class PictureUser implements PictureUserRemote, PictureUserLocal {

	@PersistenceContext
	EntityManager em;

	public PictureUser() {
	
	}

	
	public void createPictureUser(Picture picture) {
		em.persist(picture);

	}

	
	public void updatePictureUser(Picture picture) {
		em.merge(picture);

	}

	
	public void deletePictureUser(Picture picture) {
		em.remove(em.merge(picture));

	}

	
	public Picture getPictureUserById(int id) {
		return em.find(Picture.class, id);
	}

	
	public List<Picture> getAllPictureUser() {

		return em.createQuery("SELECT p FROM Picture p", Picture.class)
				.getResultList();
	}

}
