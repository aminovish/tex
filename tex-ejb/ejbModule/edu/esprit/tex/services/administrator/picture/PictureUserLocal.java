package edu.esprit.tex.services.administrator.picture;

import java.util.List;

import javax.ejb.Local;

import edu.esprit.tex.domain.Picture;

@Local
public interface PictureUserLocal {

	public void createPictureUser(Picture picture);
	public void updatePictureUser(Picture picture);
	public void deletePictureUser(Picture picture);
	public Picture getPictureUserById(int id);
	public List<Picture> getAllPictureUser();
}
