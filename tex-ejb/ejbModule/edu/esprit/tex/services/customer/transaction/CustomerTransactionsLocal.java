package edu.esprit.tex.services.customer.transaction;

import javax.ejb.Local;
import edu.esprit.tex.domain.CustomerTransaction;

@Local
public interface CustomerTransactionsLocal {

	void CreateTransaction(CustomerTransaction transaction);


}
