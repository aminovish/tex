package edu.esprit.tex.services.customer.transaction;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import edu.esprit.tex.domain.CustomerTransaction;


@Remote
public interface CustomerTransactionsRemote {
	
	void CreateTransaction (CustomerTransaction transaction);
	CustomerTransaction FindByDate (Date date);
	List<CustomerTransaction> FindAllTransactions ();


}
