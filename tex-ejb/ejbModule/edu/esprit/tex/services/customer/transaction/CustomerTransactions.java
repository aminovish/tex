package edu.esprit.tex.services.customer.transaction;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import edu.esprit.tex.domain.CustomerTransaction;

/**
 * Session Bean implementation class CustomerTransaction
 */
@Stateless

public class CustomerTransactions implements CustomerTransactionsRemote,
		CustomerTransactionsLocal {

	@PersistenceContext
	private EntityManager em;

	public void CreateTransaction(CustomerTransaction transaction) {
		em.persist(transaction);

	}

	public CustomerTransaction FindByDate(Date date) {
		CustomerTransaction transaction = em.find(CustomerTransaction.class,
				date);
		return transaction;
	}

	public List<CustomerTransaction> FindAllTransactions() {
		List<CustomerTransaction> transactions = null;
		String jpql = "SELECT t FROM Transaction t";
		Query query = em.createQuery(jpql);
		transactions = query.getResultList();
		return transactions;
	}

}
