package edu.esprit.tex.services.customer.service;

import java.util.List;

import javax.ejb.Remote;

import edu.esprit.tex.domain.Customer;
import edu.esprit.tex.domain.Service;

@Remote
public interface CustomerServiceRemote {
	void CreateService (Service service);
	void UpdateService (Service service);
	void DeleteService (Service service);
	Service FindById (int id);
	List<Service> FindAllServices ();
	List<Service> FindByNameValue (String name, int value);
	List<Service> FindServiceByCustomer (Customer customer);

}
