package edu.esprit.tex.services.customer.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import edu.esprit.tex.domain.Customer;
import edu.esprit.tex.domain.Service;

@Stateless

public class CustomerService implements CustomerServiceRemote, CustomerServiceLocal {
	
	@PersistenceContext
	private EntityManager em;
	

    public CustomerService() {
	
    }

	public void CreateService(Service service) {
		em.persist(service);
		
	}

	public void UpdateService(Service service) {
		em.merge(service);
	}

	public void DeleteService(Service service) {
		em.remove(em.merge(service));
	}

	public Service FindById(int id) {
		return em.find(Service.class, id);
	}

	public List<Service> FindAllServices() {
		List<Service> services = null;
		String jpql = "select s from Service s";
		Query query = em.createQuery(jpql);
		services = query.getResultList();
		return services;
	}

	public List<Service> FindByNameValue(String name, int value) {
		List<Service> services = null;
		String jpql = "select s from Service s where s.name=:x and s.value =:y";
		Query query = em.createQuery(jpql);
		query.setParameter("x", name);
		query.setParameter("y", value);
		services =query.getResultList();
		return services;
	}


	public List<Service> FindServiceByCustomer(Customer customer) {
		Query query = em.createQuery("select s from Service s where s.customer=:c");
		query.setParameter("c", customer);

		return query.getResultList();
	}


}




