package edu.esprit.tex.services.customer.forum;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import edu.esprit.tex.domain.Post;


@Remote
public interface CustomerForumRemote {
	
	void CreatePost (Post post);
	void UpdatePost (Post post) ;
	void DeletePost (Post post);
	Post FindById (int id);
	List< Post> FindAllPosts ();
	List< Post> FindByDate (Date date);



}
