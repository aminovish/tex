package edu.esprit.tex.services.customer.forum;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import edu.esprit.tex.domain.Post;



@Stateless

public class CustomerForum implements CustomerForumRemote, CustomerForumLocal {

	@PersistenceContext
	private EntityManager em;
	
	public CustomerForum() {
		// TODO Auto-generated constructor stub
	}
	

	public void CreatePost(Post post) {

		em.persist(post);
		
	}


	public void UpdatePost(Post post) {
		
		em.merge(post);
		
	}


	public void DeletePost(Post post) {
		
		em.remove(em.merge(post));
	}


	public Post FindById(int id) {

		return em.find(Post.class, id);
	}


	public List<Post> FindAllPosts() {

		List<Post> posts =null;
		String jpql = "select p from Post p";
		Query query = em.createQuery(jpql);
		posts = query.getResultList();
		return posts;
	}


	public List<Post> FindByDate(Date date) {

		List<Post> posts =null;
		String jpql = "select p from Post p where p.date=:d";
		Query query = em.createQuery(jpql);
		query.setParameter("d", date);
		posts =query.getResultList();
		return posts;
	}

   
}
