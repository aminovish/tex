package edu.esprit.tex.services.customer.comment;

import javax.ejb.Remote;

import edu.esprit.tex.domain.Customer;
import edu.esprit.tex.domain.Post;
import edu.esprit.tex.domain.PostComment;

@Remote
public interface CustomerCommentRemote {
	
	void AddComment (PostComment c);
	void UpdateComment (PostComment c) ;
	void DeleteComment (PostComment c);
	void postComment(Customer customer,Post post,String description, int value);


}
