package edu.esprit.tex.services.customer.comment;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.esprit.tex.domain.Customer;
import edu.esprit.tex.domain.Post;
import edu.esprit.tex.domain.PostComment;

/**
 * Session Bean implementation class CustomerComment
 */
@Stateless

public class CustomerComment implements CustomerCommentRemote, CustomerCommentLocal {
  
	@PersistenceContext
	private EntityManager em;
	@Override
	public void AddComment(PostComment c) {
		em.persist(c);
		
	}

	@Override
	public void UpdateComment(PostComment c) {
		em.merge(c);
		
	}

	@Override
	public void DeleteComment(PostComment c) {
		em.remove(em.merge(c));
		
	}

	@Override
	public void postComment(Customer customer, Post post, String description,
			int value) {
		PostComment postComment=new PostComment(customer, post, description, value);
		em.persist(postComment);
		
	}



}
