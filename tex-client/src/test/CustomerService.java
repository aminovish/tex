package test;


import client.delegate.CustomerServiceDelegate;
import client.delegate.UserServiceDelegate;
import edu.esprit.tex.domain.Customer;
import edu.esprit.tex.domain.Service;
import edu.esprit.tex.domain.User;

public class CustomerService {
	public static void main(String[] args) {
		User user=null;
		user=UserServiceDelegate.getProxy().findUserById(1);
		Service s1= new Service("TEACHER", "MATHS", 10);
		s1.setCustomer((Customer) user);
		Service s2= new Service("WEBDEVELOPER", "PHP5", 20);
		Service s3= new Service("DEVELOPER", "JAVA", 30);
		CustomerServiceDelegate.CreateService(s1);
		CustomerServiceDelegate.CreateService(s2);
		CustomerServiceDelegate.CreateService(s3);
	   Service s=null;
	   s=CustomerServiceDelegate.FindById(1);
	   s.setName("PROFESSOR");
	   CustomerServiceDelegate.UpdateService(s);
	   
		
		
	}

}
