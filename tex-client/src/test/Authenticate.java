package test;

import java.util.List;

import client.delegate.UserServiceDelegate;
import edu.esprit.tex.domain.Administrator;
import edu.esprit.tex.domain.Business;
import edu.esprit.tex.domain.User;

public class Authenticate {
	public static void main(String[] args) {
		List<Administrator>administrators=null;
		administrators=UserServiceDelegate.findAdministrators();
		for (Administrator a:administrators) {
			System.out.println(a.getName());
			
		}
		User user = UserServiceDelegate.authenticate("khaled@gmail.com",
				"khaled");

		if (user != null) {
			System.out.println("ACESS GRANTED");

			if (user instanceof Administrator) {
				System.out.println("AS ADMIN");

			} else if (user instanceof Business) {
				System.out.println("AS Business");
			} else {
				System.out.println("AS Customer");
			}

		} else {
			System.out.println("ACESS DENIED");

		}
	}

}
