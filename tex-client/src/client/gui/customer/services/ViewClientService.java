package client.gui.customer.services;


import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.border.EtchedBorder;

import client.delegate.CustomerServiceDelegate;
import client.delegate.UserServiceDelegate;
import client.gui.Main.MainSuperAdmin;


import edu.esprit.tex.domain.Customer;
import edu.esprit.tex.domain.Service;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

import session.Session;

import java.awt.Color;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;


public class ViewClientService extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField tfName;
	private JTextField tfValue;
	static int id;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ViewClientService frame = new ViewClientService(id);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ViewClientService(final int id) {
		setResizable(false);
		this.id=id;
		final Customer user =(Customer) Session.getInstance().getUser();
		setTitle("Service Detail");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 640, 480);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel.setBounds(30, 30, 560, 380);
		contentPane.add(panel);
		panel.setLayout(null);
		panel.setOpaque(false);
		
		JLabel lblServiceName = new JLabel("Service name");
		lblServiceName.setForeground(Color.WHITE);
		lblServiceName.setFont(new Font("Calibri", Font.BOLD, 14));
		lblServiceName.setBounds(42, 79, 80, 14);
		panel.add(lblServiceName);
		
		JLabel lblDescription = new JLabel("Description");
		lblDescription.setForeground(Color.WHITE);
		lblDescription.setFont(new Font("Calibri", Font.BOLD, 14));
		lblDescription.setBounds(42, 109, 80, 14);
		panel.add(lblDescription);
		final JTextArea tfDescription = new JTextArea();
		tfDescription.setEditable(false);
		tfDescription.setBounds(149, 104, 258, 137);
		panel.add(tfDescription);
		
		JLabel lblValue = new JLabel("Value (by token)");
		lblValue.setForeground(Color.WHITE);
		lblValue.setFont(new Font("Calibri", Font.BOLD, 14));
		lblValue.setBounds(42, 255, 97, 14);
		panel.add(lblValue);
		
		tfName = new JTextField();
		tfName.setEditable(false);
		tfName.setBounds(149, 76, 258, 20);
		panel.add(tfName);
		tfName.setColumns(10);
		
		tfValue = new JTextField();
		tfValue.setEditable(false);
		tfValue.setColumns(10);
		tfValue.setBounds(149, 252, 258, 20);
		panel.add(tfValue);
		
		final JLabel lblerror = new JLabel("");
		lblerror.setForeground(Color.RED);
		lblerror.setBounds(10, 355, 144, 14);
		panel.add(lblerror);
		final JLabel label = new JLabel("");
		label.setBounds(10, 334, 274, 14);
		panel.add(label);
	
		
		Service service = CustomerServiceDelegate.FindById(id);
		
		tfName.setText(service.getName());
		tfValue.setText(service.getValue()+"");
		tfDescription.setText(service.getDescription());
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.setHorizontalAlignment(SwingConstants.LEADING);
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancel.setIcon(new ImageIcon(ViewClientService.class.getResource("/img/delete.png")));
		btnCancel.setFont(new Font("Calibri", Font.PLAIN, 14));
		btnCancel.setBounds(394, 325, 129, 33);
		panel.add(btnCancel);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(149, 355, 46, 14);
		panel.add(lblNewLabel);
		
		JLabel image = new JLabel( new ImageIcon(MainSuperAdmin.class.getResource("/img/background.jpg")));
		image.setBounds(0, 0, 635, 452);
		contentPane.add(image);
		
		
		
	}
}
