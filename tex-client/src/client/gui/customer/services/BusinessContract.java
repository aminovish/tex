package client.gui.customer.services;


import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import javax.swing.JButton;
import java.awt.Font;
import javax.swing.border.EtchedBorder;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

import client.delegate.AdministratorParameterDelegate;
import client.delegate.BusinessServiceDelegate;
import client.delegate.UserServiceDelegate;
import client.gui.Main.MainSuperAdmin;
import client.gui.user.registration.Registration;

import edu.esprit.tex.domain.Parameter;

import java.awt.TextArea;
import java.awt.Cursor;
import java.awt.Color;


public class BusinessContract extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BusinessContract frame = new BusinessContract();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BusinessContract() {
		setResizable(false);
		setTitle("Business contract");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 640, 480);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setName("TEX Service Agreement");
		panel.setToolTipText("");
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel.setBounds(30, 30, 560, 380);
		contentPane.add(panel);
		panel.setOpaque(false);
		panel.setLayout(null);
		final JLabel lblClickCreateAccount = new JLabel("Click Create to agree to the TEX Services Agreement and privacy");
		lblClickCreateAccount.setForeground(Color.WHITE);
		lblClickCreateAccount.setFont(new Font("Tahoma", Font.BOLD, 10));
		lblClickCreateAccount.setBounds(10, 355, 359, 25);
		panel.add(lblClickCreateAccount);
		
		JButton btnCreate = new JButton("Create");
		btnCreate.setFont(new Font("Calibri", Font.PLAIN, 14));
		btnCreate.setIcon(new ImageIcon(BusinessContract.class.getResource("/img/coins-ajouter-icone-9675-16(1).png")));
		btnCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				dispose();
				Registration registration =new Registration();
				registration.setVisible(true);
			}
		});
		btnCreate.setBounds(270, 326, 129, 30);
		panel.add(btnCreate);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancel.setIcon(new ImageIcon(BusinessContract.class.getResource("/img/supp.png")));
		btnCancel.setFont(new Font("Calibri", Font.PLAIN, 14));
		btnCancel.setBounds(409, 324, 129, 33);
		panel.add(btnCancel);
		
		String ctrp1 = "Update Fabruary 21, 2014\r\n\r\nThank you for choosing TEX!\r\n\r\nThis document is a contract between you and TEX, which describes your rights to use the\r\nplatform and its services. It is advisable to read the entire contract, as all terms are\r\nimportant and because together they constitute a legal contract that, once you accept\r\nit, you apply.\r\nIn addition, this contract contains references to documents and strategies we encourage\r\nyou to read also.\r\n\r\nWhat are the terms that I have to respect when I use them? Our goal is to create a\r\nmore secure environment and therefore we ask that, when using services, users comply\r\nwith these terms incorporated into this Agreement by this reference (the \"Agreement\").\r\nYou are not allowed to use the services in a manner that infringes the rights of third\r\nparties, including but not limited to, deliberately harming a person or entity. \r\n\r\nPercentage deductions from transactions: For each transaction TEX will retain";
		String ctrp2 = "% and\r\nthe number of tokens should not exceed";
		String ctrp3 = "token per transaction per week neither the\r\nremaining tokens  in the account shall be less then 10 tokens ";
		String ctrp4 = "\r\nthe value of one token in DT is";
		String ctrp5 = "\r\nThe initial given number of tokens: Once you're a member of TEX,";
		String ctrp6 = "token(s) will be \r\nprovided as an initial amount.";
		Parameter parameter = AdministratorParameterDelegate.FindParameter();
		Float Percentage = parameter.getPercentage();
		int TokenNumber = parameter.getTokenNumber();
		int MaxToken = parameter.getMaxToken();
		Float TokenValue = parameter.getTokenValue();
		
		
		
		
		TextArea textArea = new TextArea();
		textArea.setColumns(3);
		textArea.setEditable(false);
		textArea.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		textArea.setText(ctrp1+" "+Percentage+ctrp2+" "+MaxToken+" "+ctrp3+ctrp4+" "+TokenValue+" "+ctrp5+" "+TokenNumber+" "+ctrp6);
		textArea.setBounds(21, 38, 517, 257);
		panel.add(textArea);
		
		JLabel image = new JLabel( new ImageIcon(MainSuperAdmin.class.getResource("/img/background.jpg")));
		image.setBounds(0, 0, 635, 452);
		contentPane.add(image);
		
		
		
	}
}
