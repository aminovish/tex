package client.gui.customer.services;


import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.border.EtchedBorder;

import client.delegate.CustomerServiceDelegate;
import client.delegate.UserServiceDelegate;
import client.gui.Main.MainCustomer;
import client.gui.Main.MainSuperAdmin;


import edu.esprit.tex.domain.Customer;
import edu.esprit.tex.domain.Service;
import edu.esprit.tex.domain.User;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

import session.Session;

import java.awt.Color;
import java.util.Date;

import javax.swing.JTextArea;
import javax.swing.SwingConstants;


public class CreateClientService extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField tfName;
	private JTextField tfValue;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CreateClientService frame = new CreateClientService();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CreateClientService() {
		setResizable(false);
		final Customer user =(Customer) Session.getInstance().getUser();
		setTitle("Create service");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 640, 480);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel.setBounds(30, 30, 560, 380);
		contentPane.add(panel);
		panel.setLayout(null);
		panel.setOpaque(false);
	
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.setHorizontalAlignment(SwingConstants.LEADING);
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancel.setIcon(new ImageIcon(CreateClientService.class.getResource("/img/delete.png")));
		btnCancel.setFont(new Font("Calibri", Font.PLAIN, 14));
		btnCancel.setBounds(424, 325, 129, 33);
		panel.add(btnCancel);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(149, 355, 46, 14);
		panel.add(lblNewLabel);
		
		final JTextArea tfDescription = new JTextArea();
		tfDescription.setBounds(149, 104, 258, 128);
		panel.add(tfDescription);
		JLabel lblServiceName = new JLabel("Service name");
		lblServiceName.setForeground(Color.WHITE);
		lblServiceName.setFont(new Font("Calibri", Font.BOLD, 14));
		lblServiceName.setBounds(42, 79, 80, 14);
		panel.add(lblServiceName);
		
		JLabel lblDescription = new JLabel("Description");
		lblDescription.setForeground(Color.WHITE);
		lblDescription.setFont(new Font("Calibri", Font.BOLD, 14));
		lblDescription.setBounds(42, 109, 80, 14);
		panel.add(lblDescription);
		
		JLabel lblValue = new JLabel("Value (by token)");
		lblValue.setForeground(Color.WHITE);
		lblValue.setFont(new Font("Calibri", Font.BOLD, 14));
		lblValue.setBounds(42, 246, 97, 14);
		panel.add(lblValue);
		
		tfName = new JTextField();
		tfName.setBounds(149, 76, 258, 20);
		panel.add(tfName);
		tfName.setColumns(10);
		
		tfValue = new JTextField();
		tfValue.setColumns(10);
		tfValue.setBounds(149, 243, 258, 20);
		panel.add(tfValue);
		
		final JLabel lblerror = new JLabel("");
		lblerror.setForeground(Color.RED);
		lblerror.setBounds(10, 355, 144, 14);
		panel.add(lblerror);
		final JLabel label = new JLabel("");
		label.setBounds(10, 334, 274, 14);
		panel.add(label);
		
		JButton btnCreate = new JButton("Create");
		btnCreate.setHorizontalAlignment(SwingConstants.LEADING);
		btnCreate.setFont(new Font("Calibri", Font.PLAIN, 14));
		btnCreate.setIcon(new ImageIcon(CreateClientService.class.getResource("/img/ajouter.png")));
		btnCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				boolean just = true ;
				if(tfName.getText().equals("")){
					just=false;
					lblerror.setText(lblerror.getText() +" All Fields are required. ");
				}
				if(tfDescription.getText().equals("")){
					just=false;
					lblerror.setText(lblerror.getText() +" All Fields are required. ");
				}
				if(tfValue.getText().equals("")){
					just=false;
					lblerror.setText(lblerror.getText() +" All Fields are required. ");
				}else {
					try {
						
						Integer.parseInt(tfValue.getText());
						
					} catch (Exception e2) {
						just=false;
						lblerror.setText("Verify that you've entered the correct information.");
					}
				}
				
				
			
				
				if(just==false){
					
					
				}
				else{
				
				
				Service service = new Service();
				Customer customer =UserServiceDelegate.findCustomerById(2);
				service.setName(tfName.getText());
				service.setDescription(tfDescription.getText());
				service.setValue(Integer.parseInt(tfValue.getText()));
				service.setCustomer(user);
				Date date = new Date();
				service.setDate(date);
				
				CustomerServiceDelegate.CreateService(service);
				dispose();
				MainCustomer mainCustomer = new MainCustomer();
				mainCustomer.setVisible(true);
				
				}
				
				
			
			}
		});
		btnCreate.setBounds(278, 325, 129, 33);
		panel.add(btnCreate);
		
		JLabel image = new JLabel( new ImageIcon(MainSuperAdmin.class.getResource("/img/background.jpg")));
		image.setBounds(0, 0, 635, 452);
		contentPane.add(image);
		
		
	}
}
