package client.gui.customer;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;
import javax.swing.JButton;

import session.Session;

import client.delegate.PictureUserServiceDelegate;
import client.delegate.UserServiceDelegate;
import client.gui.Main.MainCustomer;
import client.gui.Main.MainSuperAdmin;
import edu.esprit.tex.domain.Picture;
import edu.esprit.tex.domain.User;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.awt.Font;
import java.awt.Color;

public class UpdateCustomerProfile extends JFrame {

	private JPanel contentPane;
	private JTextField tfadress;
	private JTextField tfemail;
	private JPasswordField pfpwd;
	private JPasswordField pfrepwd;
	private JTextField tfname;
	private JTextField tfphone;
	private JTextField tfcity;
	private String fileName=null;	
	private byte[] octets1;
	Double fileSize ;
	private JPasswordField pfold;
	Picture picture ;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UpdateCustomerProfile frame = new UpdateCustomerProfile();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public UpdateCustomerProfile() {
		setResizable(false);
		final User user =Session.getInstance().getUser();
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 641, 373);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		JLabel label_7 = new JLabel("City :");
		label_7.setFont(new Font("Tahoma", Font.BOLD, 11));
		label_7.setForeground(Color.WHITE);
		label_7.setBounds(45, 267, 123, 14);
		contentPane.add(label_7);
		
		tfcity = new JTextField();
		tfcity.setFont(new Font("Tahoma", Font.BOLD, 11));
		tfcity.setColumns(10);
		tfcity.setBounds(193, 264, 166, 20);
		contentPane.add(tfcity);
		
		pfold = new JPasswordField();
		pfold.setFont(new Font("Tahoma", Font.BOLD, 11));
		pfold.setText((String) null);
		pfold.setBounds(193, 63, 166, 20);
		contentPane.add(pfold);
		
		JLabel label_2 = new JLabel("Old Password :");
		label_2.setFont(new Font("Tahoma", Font.BOLD, 11));
		label_2.setForeground(Color.WHITE);
		label_2.setBounds(45, 66, 121, 14);
		contentPane.add(label_2);
		JLabel label = new JLabel("Email :");
		label.setForeground(Color.WHITE);
		label.setFont(new Font("Tahoma", Font.BOLD, 11));
		label.setBounds(47, 22, 121, 14);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("Password :");
		label_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		label_1.setForeground(Color.WHITE);
		label_1.setBounds(47, 94, 121, 14);
		contentPane.add(label_1);
		
		JLabel lblRetypePassword = new JLabel("Retype password :");
		lblRetypePassword.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblRetypePassword.setForeground(Color.WHITE);
		lblRetypePassword.setBounds(47, 132, 121, 14);
		contentPane.add(lblRetypePassword);
		
		JLabel label_3 = new JLabel("Name :");
		label_3.setFont(new Font("Tahoma", Font.BOLD, 11));
		label_3.setForeground(Color.WHITE);
		label_3.setBounds(47, 170, 121, 14);
		contentPane.add(label_3);
		
		JLabel label_4 = new JLabel("Phone :");
		label_4.setFont(new Font("Tahoma", Font.BOLD, 11));
		label_4.setForeground(Color.WHITE);
		label_4.setBounds(47, 208, 121, 14);
		contentPane.add(label_4);
		
		JLabel label_5 = new JLabel("Address :");
		label_5.setFont(new Font("Tahoma", Font.BOLD, 11));
		label_5.setForeground(Color.WHITE);
		label_5.setBounds(47, 236, 121, 14);
		contentPane.add(label_5);
		
		tfadress = new JTextField();
		tfadress.setFont(new Font("Tahoma", Font.BOLD, 11));
		tfadress.setColumns(10);
		tfadress.setBounds(193, 236, 308, 20);
		contentPane.add(tfadress);
		
		tfemail = new JTextField();
		tfemail.setFont(new Font("Tahoma", Font.BOLD, 11));
		tfemail.setColumns(10);
		tfemail.setBounds(191, 19, 166, 20);
		contentPane.add(tfemail);
		
		pfpwd = new JPasswordField();
		pfpwd.setFont(new Font("Tahoma", Font.BOLD, 11));
		pfpwd.setBounds(193, 94, 166, 20);
		contentPane.add(pfpwd);
		
		pfrepwd = new JPasswordField();
		pfrepwd.setFont(new Font("Tahoma", Font.BOLD, 11));
		pfrepwd.setBounds(193, 132, 166, 20);
		contentPane.add(pfrepwd);
		
		tfname = new JTextField();
		tfname.setFont(new Font("Tahoma", Font.BOLD, 11));
		tfname.setColumns(10);
		tfname.setBounds(193, 170, 166, 20);
		contentPane.add(tfname);
		
		tfphone = new JTextField();
		tfphone.setFont(new Font("Tahoma", Font.BOLD, 11));
		tfphone.setColumns(10);
		tfphone.setBounds(193, 208, 166, 20);
		contentPane.add(tfphone);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBounds(483, 22, 129, 120);
		contentPane.add(panel);
		
		
		
		tfemail.setText(user.getEmail());
		tfname.setText(user.getName());
		tfadress.setText(user.getAdress());
		tfphone.setText(user.getPhone()+"");
		tfcity.setText(user.getCity());
		
		final JLabel Logo = new JLabel("Your Logo");
		Logo.setHorizontalAlignment(SwingConstants.CENTER);
		Logo.setBounds(0, 0, 129, 120);
		panel.add(Logo);
		
		try {
			picture = user.getPicture();
			byte[] octets1 =picture.getPicturee();
			fileSize = picture.getSizePicture();
			ImageIcon format = new ImageIcon(octets1);
			Image images = format.getImage(); // transform it 
	        Image newimg = images.getScaledInstance(129, 120,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way
			Logo.setText("");
			Logo.setIcon(new ImageIcon(newimg));
		} catch (Exception e2) {
			
		}
		JButton button = new JButton("Browse");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				chooser.setDialogTitle("Choisir une image");
				chooser.showOpenDialog(null);
				File image=chooser.getSelectedFile();
				fileSize=  (double) image.length();
				System.out.println(""+fileSize);
				fileName=image.getAbsolutePath();
				ImageIcon format=null;
				format = new ImageIcon(fileName);			
				Image images = format.getImage(); // transform it 
		        Image newimg = images.getScaledInstance(129, 120,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way
				Logo.setText("");
				Logo.setIcon(new ImageIcon(newimg));
				
				//remplissage de octets1
				File fileU;
				FileInputStream fis = null;
				int i=0;
				fileU = new File(fileName);
					if (fileU.exists()) {
						i++;
						  try {
							fis = new FileInputStream(fileU);
						} catch (FileNotFoundException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						 octets1 = new byte[(int) fileU.length()];
						 ByteArrayOutputStream bos = new ByteArrayOutputStream();
					        byte[] buf = new byte[1024];
					        try {
					            for (int readNum; (readNum = fis.read(buf)) != -1;) {
					                bos.write(buf, 0, readNum); 
					            }
					        } catch (IOException ex) {
					        }
					         octets1 = bos.toByteArray();
					}
			}
		});
		button.setBounds(497, 166, 104, 23);
		contentPane.add(button);
		final JLabel reqemail = new JLabel("");
		reqemail.setForeground(Color.RED);
		reqemail.setFont(new Font("Calibri", Font.PLAIN, 9));
		reqemail.setBounds(191, 42, 166, 14);
		contentPane.add(reqemail);
		final JLabel req = new JLabel("All Fields required and Should be valid");
		req.setForeground(Color.RED);
		req.setFont(new Font("Calibri", Font.PLAIN, 11));
		req.setBounds(45, 300, 299, 14);
		contentPane.add(req);
		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean just = true;
				
				if((!tfemail.getText().contains("@") && !tfemail.getText().contains(".")) ||(tfemail.getText().equals("")) ){
					reqemail.setText("Email not Valid");	
					just=false;
				}
				else {
					if(tfemail.getText().equals(user.getEmail())){
						just=true;
					}
					else{
						List<User> usList;
						usList =UserServiceDelegate.findUsers();
						for(User us:usList){
							if(tfemail.getText().equals(us.getEmail())){
								reqemail.setText("This email already exists");	
								just=false;
							}
						}
					}
				}


				
				if (pfpwd.getText().equals("")) {
					
					just = false;
				} 
				if (pfrepwd.getText().equals("")) {
					
					just = false;
				} 
				if (!pfpwd.getText().equals(pfrepwd.getText())) {
					
					just = false;

				} 
				if (tfname.getText().equals("")) {
					
					just = false;
				} 
				if (tfphone.getText().equals("")) {
					
					just = false;
				} 
				if (tfadress.getText().equals("")) {
					
					just = false;
				} 
				if (tfcity.getText().equals("")) {
					
					just = false;
				} 
				try {
					int x = Integer.parseInt(tfphone.getText());
				} catch (Exception e2) {
					just=false;
				}
				if(!pfold.getText().equals(user.getPassword())){
					just=false;
					
				}
				
				
				
				
				if (just==false) {
													
					req.setText(" Please verify all fields are required  and valid ");				
					
				}else {
					user.setName(tfname.getText());
					user.setEmail(tfemail.getText());
					user.setPassword(pfpwd.getText());
					user.setPhone(tfphone.getText());
					user.setAdress(tfadress.getText());
					user.setCity(tfcity.getText());
					UserServiceDelegate.update(user);
					picture.setPicturee(octets1);
					picture.setSizePicture(fileSize);
					PictureUserServiceDelegate.updatePictureUser(picture);
					MainCustomer mainCustomer= new MainCustomer();
					mainCustomer.setVisible(true);
					dispose();
					
					
				}
			}
		});
		btnUpdate.setBounds(485, 291, 129, 33);
		contentPane.add(btnUpdate);
		
		JLabel image = new JLabel( new ImageIcon(MainSuperAdmin.class.getResource("/img/background.jpg")));
		image.setBounds(0, 0, 635, 345);
		contentPane.add(image);
		
		
		
		
	}
}
