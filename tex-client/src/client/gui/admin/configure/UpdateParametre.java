package client.gui.admin.configure;

import java.awt.EventQueue;
import java.awt.Color;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import client.delegate.AdministratorParameterDelegate;
import client.delegate.ParameterDelegate;
import client.gui.Main.MainSuperAdmin;
import edu.esprit.tex.domain.Parameter;

public class UpdateParametre extends JFrame {

	private JPanel contentPane;
	private JTextField t_tokennumber;
	private JTextField t_percentage;
	private JTextField t_max;
	private JTextField t_val;
	int x,z;
	float y,w;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UpdateParametre frame = new UpdateParametre();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public UpdateParametre() {
		setResizable(false);
		setTitle("Parameters");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 640, 480);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel.setBounds(30, 30, 560, 380);
		contentPane.add(panel);
		panel.setLayout(null);
		panel.setOpaque(false);
		
				
		JLabel lblNewLabel = new JLabel("Parameters");
		lblNewLabel.setForeground(Color.BLUE);
		lblNewLabel.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 16));
		lblNewLabel.setBounds(191, 11, 89, 26);
		panel.add(lblNewLabel);
		
		JLabel lblMaxToken = new JLabel("Max Token :");
		lblMaxToken.setForeground(Color.WHITE);
		lblMaxToken.setFont(new Font("Calibri", Font.BOLD, 14));
		lblMaxToken.setBounds(121, 154, 115, 26);
		panel.add(lblMaxToken);
		
		JLabel lblPercentage = new JLabel("Percentage :");
		lblPercentage.setForeground(Color.WHITE);
		lblPercentage.setFont(new Font("Calibri", Font.BOLD, 14));
		lblPercentage.setBounds(121, 117, 115, 26);
		panel.add(lblPercentage);
		
		JLabel lblTokenNumber = new JLabel("Token Number :");
		lblTokenNumber.setForeground(Color.WHITE);
		lblTokenNumber.setFont(new Font("Calibri", Font.BOLD, 14));
		lblTokenNumber.setBounds(121, 80, 115, 26);
		panel.add(lblTokenNumber);
		
		JLabel lblTokenValue = new JLabel("Token Value :");
		lblTokenValue.setForeground(Color.WHITE);
		lblTokenValue.setFont(new Font("Calibri", Font.BOLD, 14));
		lblTokenValue.setBounds(121, 191, 115, 26);
		panel.add(lblTokenValue);
		
		t_tokennumber = new JTextField();
		t_tokennumber.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		
		final JLabel lblAllFieldsRequired = new JLabel("All Fields are required and NUMERIC");
		lblAllFieldsRequired.setFont(new Font("Calibri", Font.BOLD, 12));
		lblAllFieldsRequired.setForeground(Color.RED);
		lblAllFieldsRequired.setVerticalAlignment(SwingConstants.TOP);
		lblAllFieldsRequired.setBounds(59, 268, 177, 26);
		panel.add(lblAllFieldsRequired);
		t_tokennumber.setBounds(246, 84, 104, 20);
		panel.add(t_tokennumber);
		t_tokennumber.setColumns(10);
		
		t_percentage = new JTextField();
		t_percentage.setBounds(246, 121, 104, 20);
		panel.add(t_percentage);
		t_percentage.setColumns(10);
		
		t_max = new JTextField();
		t_max.setColumns(10);
		t_max.setBounds(246, 157, 104, 20);
		panel.add(t_max);
		
		t_val = new JTextField();
		t_val.setColumns(10);
		t_val.setBounds(246, 194, 104, 20);
		panel.add(t_val);
		
		JButton btnOK = new JButton("OK");


		btnOK.setFont(new Font("Calibri", Font.BOLD, 12));
		btnOK.setBounds(280, 328, 130, 41);
		panel.add(btnOK);
		
		JButton btnNewButton = new JButton("Cancel");
		btnNewButton.setFont(new Font("Calibri", Font.BOLD, 12));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnNewButton.setBounds(420, 328, 130, 40);
		panel.add(btnNewButton);
		Parameter parameter =ParameterDelegate.FindParameter();
		
		t_tokennumber.setText(Integer.toString(parameter.getTokenNumber()));
		t_max.setText(Integer.toString(parameter.getMaxToken()));
		t_percentage.setText(Float.toString(parameter.getPercentage()));
		t_val.setText(Float.toString(parameter.getTokenValue()));
		
		btnOK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				boolean just = true ;
				
				if(t_tokennumber.getText().equals("")){
					just=false;
				}
				
				if(t_percentage.getText().equals("")){
					just=false;
				}
				
				if(t_max.getText().equals("")){
					just=false;
				}
				
				if(t_val.getText().equals("")){
					just=false;
				}
				try {
					x= Integer.parseInt(t_tokennumber.getText());
				    y = Float.parseFloat(t_percentage.getText());
				    z = Integer.parseInt(t_max.getText());
					w = Float.parseFloat(t_val.getText());
					
				} catch (Exception e2) {
					just=false;
				}
				if(just==false){
					
				}
				else{
	            Parameter parameter=new Parameter();
	            parameter.setId(1);
	            parameter.setTokenNumber(x);
				parameter.setPercentage(y);
				parameter.setMaxToken(z);
				parameter.setTokenValue(w);
				AdministratorParameterDelegate.UpdatePrameter(parameter);


							MainSuperAdmin frame = new MainSuperAdmin(3);
							frame.setVisible(true);
							UpdateParametre.this.dispose();

	
				}
				
			}
		});
		
		JLabel image = new JLabel( new ImageIcon(MainSuperAdmin.class.getResource("/img/background.jpg")));
		image.setBounds(0, 0, 635, 410);
		contentPane.add(image);
	}
}
