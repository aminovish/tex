package client.gui.superadmin;

import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.JCheckBox;
import javax.swing.JButton;

import client.delegate.PictureUserServiceDelegate;
import client.delegate.UserServiceDelegate;
import client.gui.Main.MainSuperAdmin;
import client.gui.admin.configure.AddParametre;
import client.gui.superadmin.registration.RegistrationSuperAdmin;

import edu.esprit.tex.domain.Administrator;
import edu.esprit.tex.domain.Picture;
import edu.esprit.tex.domain.User;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import javax.swing.SwingConstants;
import java.awt.Color;

public class CreateAdmin extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6601041004308789043L;
	private JPanel contentPane;
	private JTextField tfName;
	private JTextField tfEmail;
	private JTextField tfAdress;
	private JTextField tfPhone;
	private JTextField tfPassword;
	private JTextField tfCity;
	private String fileName=null;
	
	private byte[] octets1;
	Double fileSize ;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CreateAdmin frame = new CreateAdmin(-1);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CreateAdmin(final int state) {
		setTitle("New Administrator");
		
		
		
		
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 640, 480);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel.setBounds(30, 30, 560, 380);
		contentPane.add(panel);
		panel.setLayout(null);
		panel.setOpaque(false);
		
		JLabel lblName = new JLabel("Name :");
		lblName.setForeground(Color.WHITE);
		lblName.setFont(new Font("Calibri", Font.BOLD, 14));
		lblName.setBounds(41, 56, 67, 26);
		panel.add(lblName);
		
		tfName = new JTextField();
		tfName.setFont(new Font("Tahoma", Font.BOLD, 11));
		tfName.setToolTipText("Admin Name");
		tfName.setBounds(105, 60, 119, 20);
		panel.add(tfName);
		tfName.setColumns(10);
		
		JLabel lblEmail = new JLabel("E-mail :");
		lblEmail.setForeground(Color.WHITE);
		lblEmail.setFont(new Font("Calibri", Font.BOLD, 14));
		lblEmail.setBounds(41, 90, 67, 26);
		panel.add(lblEmail);
		
		JLabel lblAdress = new JLabel("Adress :");
		lblAdress.setForeground(Color.WHITE);
		lblAdress.setFont(new Font("Calibri", Font.BOLD, 14));
		lblAdress.setBounds(41, 127, 67, 26);
		panel.add(lblAdress);
		
		JLabel lblPassword = new JLabel("Password :");
		lblPassword.setForeground(Color.WHITE);
		lblPassword.setFont(new Font("Calibri", Font.BOLD, 14));
		lblPassword.setBounds(41, 239, 67, 26);
		panel.add(lblPassword);
		
		JLabel lblPhone = new JLabel("Phone :");
		lblPhone.setForeground(Color.WHITE);
		lblPhone.setFont(new Font("Calibri", Font.BOLD, 14));
		lblPhone.setBounds(41, 202, 67, 26);
		panel.add(lblPhone);
		
		final JCheckBox CBstate = new JCheckBox("State");
		CBstate.setFont(new Font("Tahoma", Font.BOLD, 11));
		CBstate.setForeground(Color.WHITE);
		CBstate.setBounds(424, 224, 97, 23);
		panel.add(CBstate);
		CBstate.setOpaque(false);
		
		tfEmail = new JTextField();
		tfEmail.setFont(new Font("Tahoma", Font.BOLD, 11));
		tfEmail.setToolTipText("");
		tfEmail.setColumns(10);
		tfEmail.setBounds(105, 93, 119, 20);
		panel.add(tfEmail);
		
		tfAdress = new JTextField();
		tfAdress.setFont(new Font("Tahoma", Font.BOLD, 11));
		tfAdress.setToolTipText("");
		tfAdress.setColumns(10);
		tfAdress.setBounds(105, 130, 119, 20);
		panel.add(tfAdress);
		
		tfPhone = new JTextField();
		tfPhone.setFont(new Font("Tahoma", Font.BOLD, 11));
		tfPhone.setToolTipText("");
		tfPhone.setColumns(10);
		tfPhone.setBounds(105, 203, 119, 20);
		panel.add(tfPhone);
		
		tfPassword = new JTextField();
		tfPassword.setFont(new Font("Tahoma", Font.BOLD, 11));
		tfPassword.setToolTipText("");
		tfPassword.setColumns(10);
		tfPassword.setBounds(105, 242, 119, 20);
		panel.add(tfPassword);
		
		JButton btnCreate = new JButton("Create");

		btnCreate.setBounds(432, 346, 89, 23);
		panel.add(btnCreate);
		
		JButton btnBrows = new JButton("Browse");

		btnBrows.setBounds(414, 194, 89, 23);
		panel.add(btnBrows);
		
		final JLabel req1 = new JLabel("");
		req1.setBounds(41, 297, 302, 38);
		panel.add(req1);
		
		JLabel lblCity = new JLabel("City :");
		lblCity.setForeground(Color.WHITE);
		lblCity.setFont(new Font("Calibri", Font.BOLD, 14));
		lblCity.setBounds(41, 165, 67, 26);
		panel.add(lblCity);
		
		tfCity = new JTextField();
		tfCity.setFont(new Font("Tahoma", Font.BOLD, 11));
		tfCity.setToolTipText("");
		tfCity.setColumns(10);
		tfCity.setBounds(105, 166, 119, 20);
		panel.add(tfCity);
		
		JPanel panel_1 = new JPanel();
		panel_1.setLayout(null);
		panel_1.setBounds(392, 56, 129, 120);
		panel.add(panel_1);
		
		final JLabel Logo = new JLabel("Your Logo");
		Logo.setBounds(0, 0, 129, 120);
		panel_1.add(Logo);
		Logo.setHorizontalAlignment(SwingConstants.CENTER);
		btnBrows.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser chooser = new JFileChooser();
				chooser.setDialogTitle("Choisir une image");
				chooser.showOpenDialog(null);
				File image=chooser.getSelectedFile();
				fileSize=  (double) image.length();
				System.out.println(""+fileSize);
				fileName=image.getAbsolutePath();
				ImageIcon format=null;
				format = new ImageIcon(fileName);			
				Image images = format.getImage(); // transform it 
		        Image newimg = images.getScaledInstance(129, 120,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way
				Logo.setText("");
				Logo.setIcon(new ImageIcon(newimg));
				
				//remplissage de octets1
				File fileU;
				FileInputStream fis = null;
				int i=0;
				fileU = new File(fileName);
					if (fileU.exists()) {
						i++;
						  try {
							fis = new FileInputStream(fileU);
						} catch (FileNotFoundException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						 octets1 = new byte[(int) fileU.length()];
						 ByteArrayOutputStream bos = new ByteArrayOutputStream();
					        byte[] buf = new byte[1024];
					        try {
					            for (int readNum; (readNum = fis.read(buf)) != -1;) {
					                bos.write(buf, 0, readNum); 
					            }
					        } catch (IOException ex) {
					        }
					         octets1 = bos.toByteArray();
					}}
		});
		if(state >-1){
			Administrator administrator =null;
			
			administrator=UserServiceDelegate.findAdministratorById(state);
		
			tfName.setText(administrator.getName());
			tfEmail.setText(administrator.getEmail());
			tfAdress.setText(administrator.getAdress());
			tfPassword.setText(administrator.getPassword());
			tfPhone.setText(administrator.getPhone());
			tfCity.setText(administrator.getCity());
			CBstate.setSelected(administrator.getState());
			try {				
				Picture picture=administrator.getPicture();
				byte[] octets =picture.getPicturee();
				ImageIcon format = new ImageIcon(octets);
				Image images = format.getImage(); // transform it 
		        Image newimg = images.getScaledInstance(129, 120,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way
				Logo.setText("");
				Logo.setIcon(new ImageIcon(newimg));
				
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			
			btnCreate.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					
					
					boolean just = true;
					if (tfEmail.getText().equals("")) {
						req1.setText("- Email is not valid");
						just = false;
					} 
					if (tfPassword.getText().equals("")) {
						
						just = false;
					} 

					if (tfName.getText().equals("")) {
						
						just = false;
					} 
					if (tfPhone.getText().equals("")) {
						
						just = false;
					} 
					if (tfAdress.getText().equals("")) {
						
						just = false;
					} 
					if (tfCity.getText().equals("")) {
						
						just = false;
					} 
					try {
						int x = Integer.parseInt(tfPhone.getText());
					} catch (Exception e) {
						req1.setText("- The phone number you entered isn't valid");
						just=false;
					}
					if (just==false) {
											
						
						
						
					} else {
						
						Administrator administrator =new Administrator(tfName.getText(),tfEmail.getText(), tfPassword.getText(), tfAdress.getText(), tfPhone.getText(), tfCity.getText(), 0, CBstate.isSelected(),null);
						administrator.setId(state);
						UserServiceDelegate.updateAdministrator(administrator);
						List<User> users = UserServiceDelegate.findUsers();
						for(User user2 :users){
							if(tfEmail.getText().equals(user2.getEmail())){
								Picture picture = new Picture(octets1,fileSize);
								picture.setUser(administrator);
								PictureUserServiceDelegate.updatePictureUser(picture);
							}
						}
						
						MainSuperAdmin frame = new MainSuperAdmin(2);
						frame.setVisible(true);
						CreateAdmin.this.dispose();
						
					}
					
				}
			});
			
		}else{
			
		
		btnCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				boolean just = true;
				if (tfEmail.getText().equals("")) {
					req1.setText("- Email is not valid");
					just = false;
				} 
				if (tfPassword.getText().equals("")) {
					
					just = false;
				} 

				if (tfName.getText().equals("")) {
					
					just = false;
				} 
				if (tfPhone.getText().equals("")) {
					
					just = false;
				} 
				if (tfAdress.getText().equals("")) {
					
					just = false;
				} 
				if (tfCity.getText().equals("")) {
					
					just = false;
				} 
				try {
					int x = Integer.parseInt(tfPhone.getText());
				} catch (Exception e) {
					req1.setText("- The phone number you entered isn't valid");
					just=false;
				}
				if (just==false) {
										
					
					
					
				} else {

					Administrator administrator =new Administrator(tfName.getText(),tfEmail.getText(), tfPassword.getText(), tfAdress.getText(), tfPhone.getText(), tfCity.getText(), 0, CBstate.isSelected(),null);
					
					UserServiceDelegate.createAdministrator(administrator);
					List<User> users = UserServiceDelegate.findUsers();
					for(User user2 :users){
						if(tfEmail.getText().equals(user2.getEmail())){
							Picture picture = new Picture(octets1,fileSize);
							picture.setUser(user2);
							PictureUserServiceDelegate.createPictureUser(picture);}
					}
					
					MainSuperAdmin frame = new MainSuperAdmin(2);
					frame.setVisible(true);
					CreateAdmin.this.dispose();
				}
				

				
			}
		});}
		
		JLabel image = new JLabel( new ImageIcon(MainSuperAdmin.class.getResource("/img/background.jpg")));
		image.setBounds(0, 0, 635, 452);
		contentPane.add(image);
	}
}
