package client.gui.superadmin.registration;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import client.delegate.PictureUserServiceDelegate;
import client.delegate.UserServiceDelegate;
import client.gui.Main.MainSuperAdmin;
import client.gui.admin.configure.AddParametre;
import client.gui.user.authentication.Authentication;
import edu.esprit.tex.domain.Picture;
import edu.esprit.tex.domain.User;
import javax.swing.JEditorPane;
import javax.swing.JTextPane;

public class RegistrationSuperAdmin extends JFrame {

	private JPanel contentPane;
	private JTextField tfemail;
	private JPasswordField pfpwd;
	private JPasswordField pfrepwd;
	private JTextField tfphone;
	private JTextField tfname;
	private JTextField tfadress;
	private JTextField tfcity;
	
	private String fileName=null;
	
	private byte[] octets1;
	Double fileSize ;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RegistrationSuperAdmin frame = new RegistrationSuperAdmin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RegistrationSuperAdmin() {
		setResizable(false);
		setTitle("Super Admin Registration");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 641, 425);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		JLabel image = new JLabel( new ImageIcon(MainSuperAdmin.class.getResource("/img/background.jpg")));
		image.setBackground(Color.RED);
		image.setBounds(0, 0, 641, 397);
		

		JLabel lblEmail = new JLabel("* Email :");
		lblEmail.setBounds(26, 19, 121, 14);
		lblEmail.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblEmail.setForeground(Color.WHITE);

		tfemail = new JTextField();
		tfemail.setBounds(172, 19, 166, 20);
		tfemail.setColumns(10);

		JLabel lblPass = new JLabel("* Password :");
		lblPass.setBounds(26, 57, 121, 14);
		lblPass.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPass.setForeground(Color.WHITE);

		JLabel lblRetypePassword = new JLabel("* Retype password :");
		lblRetypePassword.setBounds(26, 95, 121, 14);
		lblRetypePassword.setForeground(Color.WHITE);
		lblRetypePassword.setFont(new Font("Tahoma", Font.BOLD, 11));

		pfpwd = new JPasswordField();
		pfpwd.setBounds(172, 57, 166, 20);

		pfrepwd = new JPasswordField();
		pfrepwd.setBounds(172, 95, 166, 20);

		tfphone = new JTextField();
		tfphone.setBounds(172, 171, 166, 20);
		tfphone.setColumns(10);

		JLabel lblPhone = new JLabel("* Phone :");
		lblPhone.setBounds(26, 171, 121, 14);
		lblPhone.setForeground(Color.WHITE);
		lblPhone.setFont(new Font("Tahoma", Font.BOLD, 11));

		JPanel panel = new JPanel();
		panel.setBounds(455, 19, 129, 120);

		

		JLabel lblName = new JLabel("* Name :");
		lblName.setBounds(26, 133, 121, 14);
		lblName.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblName.setForeground(Color.WHITE);

		tfname = new JTextField();
		tfname.setBounds(172, 133, 166, 20);
		tfname.setColumns(10);

		JLabel lblAdress = new JLabel("* Adress :");
		lblAdress.setBounds(26, 199, 121, 14);
		lblAdress.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblAdress.setForeground(Color.WHITE);

		tfadress = new JTextField();
		tfadress.setBounds(172, 199, 308, 20);
		tfadress.setColumns(10);
		contentPane.setLayout(null);
		final JLabel lblFields = new JLabel("* All fields are required");
		lblFields.setBounds(26, 268, 318, 19);
		lblFields.setVerticalAlignment(SwingConstants.TOP);
		lblFields.setForeground(Color.RED);
		lblFields.setFont(new Font("Calibri", Font.BOLD, 12));
		contentPane.add(lblFields);

		JLabel lblCity = new JLabel("* City :");
		lblCity.setBounds(24, 230, 123, 14);
		lblCity.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCity.setForeground(Color.WHITE);
		contentPane.add(lblCity);
		contentPane.add(tfadress);
		contentPane.add(tfemail);
		contentPane.add(pfpwd);
		contentPane.add(pfrepwd);
		contentPane.add(tfname);

		tfcity = new JTextField();
		tfcity.setBounds(172, 227, 166, 20);
		tfcity.setColumns(10);
		contentPane.add(tfcity);
		contentPane.add(tfphone);
		contentPane.add(panel);
		panel.setLayout(null);

		final JLabel Logo = new JLabel("Your Logo");
		Logo.setHorizontalAlignment(SwingConstants.CENTER);
		Logo.setBounds(0, 0, 129, 120);
		panel.add(Logo);

		JButton btnExit = new JButton("Exit");
		btnExit.setBounds(491, 264, 103, 23);
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		contentPane.add(lblEmail);
		contentPane.add(lblPass);
		contentPane.add(lblRetypePassword);
		contentPane.add(lblName);
		contentPane.add(lblPhone);
		contentPane.add(lblAdress);

		
		
		contentPane.add(btnExit);
		
		final JLabel req1 = new JLabel("- Enter a valid email address");
		req1.setBounds(49, 285, 268, 14);
		req1.setFont(new Font("Calibri", Font.BOLD, 12));
		req1.setForeground(Color.RED);
		contentPane.add(req1);
		
		final JLabel req2 = new JLabel("- Please type a password and then retype it to confirm");
		req2.setBounds(49, 310, 268, 14);
		req2.setFont(new Font("Calibri", Font.BOLD, 12));
		req2.setForeground(Color.RED);
		contentPane.add(req2);
		
		final JLabel req3 = new JLabel("- Enter a valid phone number");
		req3.setBounds(49, 338, 268, 14);
		req3.setFont(new Font("Calibri", Font.BOLD, 12));
		req3.setForeground(Color.RED);
		contentPane.add(req3);
		JButton btnYourPhoto = new JButton("Browse");
		btnYourPhoto.setBounds(465, 149, 104, 23);
		btnYourPhoto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser chooser = new JFileChooser();
				chooser.setDialogTitle("Choisir une image");
				chooser.showOpenDialog(null);
				File image=chooser.getSelectedFile();
				fileSize=  (double) image.length();
				System.out.println(""+fileSize);
				fileName=image.getAbsolutePath();
				ImageIcon format=null;
				format = new ImageIcon(fileName);			
				Image images = format.getImage(); // transform it 
		        Image newimg = images.getScaledInstance(129, 120,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way
				Logo.setText("");
				Logo.setIcon(new ImageIcon(newimg));
				
				//remplissage de octets1
				File fileU;
				FileInputStream fis = null;
				int i=0;
				fileU = new File(fileName);
					if (fileU.exists()) {
						i++;
						  try {
							fis = new FileInputStream(fileU);
						} catch (FileNotFoundException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						 octets1 = new byte[(int) fileU.length()];
						 ByteArrayOutputStream bos = new ByteArrayOutputStream();
					        byte[] buf = new byte[1024];
					        try {
					            for (int readNum; (readNum = fis.read(buf)) != -1;) {
					                bos.write(buf, 0, readNum); 
					            }
					        } catch (IOException ex) {
					        }
					         octets1 = bos.toByteArray();
					}
			}
		});
		
		JButton btnRegistre = new JButton("Registre");
		btnRegistre.setBounds(354, 264, 109, 23);
		btnRegistre.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				boolean just = true;
				if (tfemail.getText().equals("")) {
					req1.setText("- Email is not valid");
					just = false;
				} 
				if (pfpwd.getText().equals("")) {
					
					just = false;
				} 
				if (pfrepwd.getText().equals("")) {
					
					just = false;
				} 
				if (!pfpwd.getText().equals(pfrepwd.getText())) {
					
					just = false;
					req2.setText("- Passwords don't match, please type a password and then retype it to confirm");
				} 
				if (tfname.getText().equals("")) {
					
					just = false;
				} 
				if (tfphone.getText().equals("")) {
					
					just = false;
				} 
				if (tfadress.getText().equals("")) {
					
					just = false;
				} 
				if (tfcity.getText().equals("")) {
					
					just = false;
				} 
				try {
					Long x = Long.getLong(tfphone.getText());
				} catch (Exception e) {
					req3.setText("- The phone number you entered isn't valid");
					just=false;
				}
				if (just==false) {
										
					
					
					
				} else {

					User user = new User(tfname.getText(), tfemail.getText(), pfpwd.getText(), tfadress.getText(), tfphone.getText(), tfcity.getText(), 0, true, null, null);
					Picture picture = new Picture(octets1,fileSize);
					picture.setUser(user);
					user.setPicture(picture);
					UserServiceDelegate.create(user);
					
					AddParametre parametre= new AddParametre();
					parametre.setVisible(true);
					RegistrationSuperAdmin.this.dispose();
					
				}
			}
		});
		contentPane.add(btnRegistre);
		contentPane.add(btnYourPhoto);
		
		JEditorPane dtrpnCautionIfYou = new JEditorPane();
		dtrpnCautionIfYou.setForeground(Color.WHITE);
		dtrpnCautionIfYou.setBackground(Color.RED);
		dtrpnCautionIfYou.setFont(new Font("Tahoma", Font.BOLD, 11));
		dtrpnCautionIfYou.setText("CAUTION:                                                                If you lose the master password, it will be impossible to restore the contents of data files. Choose it carefully.");
		dtrpnCautionIfYou.setBounds(357, 310, 241, 76);
		contentPane.add(dtrpnCautionIfYou);
		contentPane.add(image);

	}
}
