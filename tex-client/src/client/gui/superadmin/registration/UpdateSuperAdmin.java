package client.gui.superadmin.registration;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.swing.AbstractButton;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import client.delegate.PictureUserServiceDelegate;
import client.delegate.UserServiceDelegate;
import client.gui.Main.MainSuperAdmin;
import edu.esprit.tex.domain.Picture;
import edu.esprit.tex.domain.User;
import javax.swing.JEditorPane;
import javax.swing.JTextPane;

import session.Session;

public class UpdateSuperAdmin extends JFrame {

	private JPanel contentPane;
	private JTextField tfemail;
	private JPasswordField pfpwd;
	private JPasswordField pfrepwd;
	private JTextField tfphone;
	private JTextField tfname;
	private JTextField tfadress;
	private JTextField tfcity;
	
	private String fileName=null;
	
	private byte[] octets1;
	Double fileSize ;
	private JPasswordField oldPass;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UpdateSuperAdmin frame = new UpdateSuperAdmin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public UpdateSuperAdmin() {
		final User user =Session.getInstance().getUser();
		
		setResizable(false);
		setTitle("Super Admin Update Profil");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 641, 425);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		JLabel image = new JLabel( new ImageIcon(MainSuperAdmin.class.getResource("/img/background.jpg")));
		image.setBounds(0, 0, 635, 397);
		
		

		JLabel lblEmail = new JLabel("Email :");
		lblEmail.setForeground(Color.WHITE);
		lblEmail.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblEmail.setBounds(26, 19, 121, 14);

		tfemail = new JTextField();
		tfemail.setBounds(172, 19, 166, 20);
		tfemail.setColumns(10);
		tfemail.setEditable(false);

		JLabel lblPass = new JLabel("Password :");
		lblPass.setForeground(Color.WHITE);
		lblPass.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPass.setBounds(28, 81, 121, 14);

		JLabel lblRetypePassword = new JLabel("Reenter password :");
		lblRetypePassword.setForeground(Color.WHITE);
		lblRetypePassword.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblRetypePassword.setBounds(26, 112, 121, 14);

		pfpwd = new JPasswordField();
		pfpwd.setBounds(172, 78, 166, 20);

		pfrepwd = new JPasswordField();
		pfrepwd.setBounds(174, 109, 166, 20);

		tfphone = new JTextField();
		tfphone.setBounds(174, 195, 166, 20);
		tfphone.setColumns(10);

		JLabel lblPhone = new JLabel("Phone :");
		lblPhone.setForeground(Color.WHITE);
		lblPhone.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPhone.setBounds(28, 195, 121, 14);

		JPanel panel = new JPanel();
		panel.setBounds(455, 19, 129, 120);

		

		JLabel lblName = new JLabel("Name :");
		lblName.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblName.setForeground(Color.WHITE);
		lblName.setBounds(28, 157, 121, 14);

		tfname = new JTextField();
		tfname.setBounds(174, 157, 166, 20);
		tfname.setColumns(10);

		JLabel lblAdress = new JLabel("Adress :");
		lblAdress.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblAdress.setForeground(Color.WHITE);
		lblAdress.setBounds(28, 223, 121, 14);

		tfadress = new JTextField();
		tfadress.setBounds(174, 223, 308, 20);
		tfadress.setColumns(10);

		JLabel lblCity = new JLabel("City :");
		lblCity.setForeground(Color.WHITE);
		lblCity.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCity.setBounds(26, 254, 123, 14);
		contentPane.add(lblCity);
		contentPane.add(tfadress);
		contentPane.add(tfemail);
		contentPane.add(pfpwd);
		contentPane.add(pfrepwd);
		contentPane.add(tfname);

		tfcity = new JTextField();
		tfcity.setBounds(174, 251, 166, 20);
		tfcity.setColumns(10);
		contentPane.add(tfcity);
		contentPane.add(tfphone);
		contentPane.add(panel);
		panel.setLayout(null);

		final JLabel Logo = new JLabel("Your Logo");
		Logo.setHorizontalAlignment(SwingConstants.CENTER);
		Logo.setBounds(0, 0, 129, 120);
		panel.add(Logo);
		

		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnExit.setBounds(512, 326, 103, 23);

		
		contentPane.setLayout(null);
		contentPane.add(lblEmail);
		contentPane.add(lblPass);
		contentPane.add(lblRetypePassword);
		contentPane.add(lblName);
		contentPane.add(lblPhone);
		contentPane.add(lblAdress);

		
		
		contentPane.add(btnExit);
		JButton btnYourPhoto = new JButton("Browse");
		btnYourPhoto.setBounds(465, 149, 104, 23);
		btnYourPhoto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
			}
		});
		
		try {				
			Picture picture=user.getPicture();
			byte[] octets =picture.getPicturee();
			ImageIcon format = new ImageIcon(octets);
			Image images = format.getImage(); // transform it 
	        Image newimg = images.getScaledInstance(129, 120,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way
			Logo.setText("");
			Logo.setIcon(new ImageIcon(newimg));
			octets1=picture.getPicturee();
			fileSize=picture.getSizePicture();
			
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		

		
		JLabel lblFields = new JLabel("* All fields are required");
		lblFields.setVerticalAlignment(SwingConstants.TOP);
		lblFields.setForeground(Color.RED);
		lblFields.setFont(new Font("Calibri", Font.PLAIN, 11));
		lblFields.setBounds(20, 278, 318, 19);
		contentPane.add(lblFields);
		
		final JLabel req1 = new JLabel("- Enter a valid email address");
		req1.setForeground(Color.RED);
		req1.setFont(new Font("Calibri", Font.PLAIN, 11));
		req1.setBounds(43, 295, 268, 14);
		contentPane.add(req1);
		
		final JLabel req2 = new JLabel("- Please type a password and then retype it to confirm");
		req2.setForeground(Color.RED);
		req2.setFont(new Font("Calibri", Font.PLAIN, 11));
		req2.setBounds(43, 320, 268, 14);
		contentPane.add(req2);
		
		final JLabel req3 = new JLabel("- Enter a valid phone number");
		req3.setForeground(Color.RED);
		req3.setFont(new Font("Calibri", Font.PLAIN, 11));
		req3.setBounds(43, 348, 268, 14);
		contentPane.add(req3);
		
		JLabel lblOldPassword = new JLabel("Old Password :");
		lblOldPassword.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblOldPassword.setForeground(Color.WHITE);
		lblOldPassword.setBounds(26, 53, 121, 14);
		contentPane.add(lblOldPassword);
		
		oldPass = new JPasswordField();
		oldPass.setText((String) null);
		oldPass.setBounds(172, 50, 166, 20);
		contentPane.add(oldPass);
		
		tfadress.setText(user.getAdress());
		tfcity.setText(user.getCity());
		tfemail.setText(user.getEmail());
		tfname.setText(user.getName());
		tfphone.setText(user.getPhone());
		
		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean just = true;
				if (tfemail.getText().equals("")) {
					req1.setText("");
					req3.setText("");
					req2.setText("- Email is not valid");
					just = false;
				} 
				if (pfpwd.getText().equals("")) {
					
					just = false;
					req1.setText("");
					req3.setText("");
					req2.setText("empty Password");
				} 
				if (pfrepwd.getText().equals("")) {
					
					just = false;
					req1.setText("");
					req3.setText("");
					req2.setText("empty Password");
				} 
				if (oldPass.getText().equals("")) {
					
					just = false;
					req1.setText("");
					req3.setText("");
					req2.setText("empty Password");
				} 
				
				if (!oldPass.getText().equals(user.getPassword())) {
					
					just = false;
					req1.setText("");
					req3.setText("");
					req2.setText("- You must insert a valid password");
				} 
				
				if (!pfpwd.getText().equals(pfrepwd.getText())) {
					
					just = false;
					req1.setText("");
					req3.setText("");
					req2.setText("- Passwords don't match, please type a password and then retype it to confirm");
				} 
				if (tfname.getText().equals("")) {
					
					just = false;
					req1.setText("");
					req3.setText("");
					req2.setText("s");
				
				} 
				if (tfphone.getText().equals("")) {
					
					just = false;
				} 
				if (tfadress.getText().equals("")) {
					
					just = false;
				} 
				if (tfcity.getText().equals("")) {
					
					just = false;
				} 
				try {
					int x = Integer.parseInt(tfphone.getText());
				} catch (Exception e1) {
					
					req3.setText("- The phone number you entered isn't valid");
					just=false;
				}
				if (just==false) {
										
					
					
					
				} else {

					
					user.setName(tfname.getText());
					user.setEmail(tfemail.getText());
					user.setPassword(pfpwd.getText());
					user.setPhone(tfphone.getText());
					user.setAdress(tfadress.getText());
					user.setCity(tfcity.getText());
					user.setState(true);
					UserServiceDelegate.update(user);
					List<User> users = UserServiceDelegate.findUsers();
					for(User user2 :users){
						if(tfemail.getText().equals(user2.getEmail())){
							Picture picture = new Picture(octets1,fileSize);
							picture.setId(1);
							picture.setUser(user2);
							PictureUserServiceDelegate.updatePictureUser(picture);
						}
					}
					MainSuperAdmin mainSuperAdmin=new MainSuperAdmin(1);
					mainSuperAdmin.setVisible(true);
					dispose();
				}
			}
		});
		btnUpdate.setBounds(413, 326, 89, 23);
		contentPane.add(btnUpdate);
		
		JButton button = new JButton("Browse");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				chooser.setDialogTitle("Choisir une image");
				chooser.showOpenDialog(null);
				File image=chooser.getSelectedFile();
				fileSize=  (double) image.length();
				System.out.println(""+fileSize);
				fileName=image.getAbsolutePath();
				ImageIcon format=null;
				format = new ImageIcon(fileName);			
				Image images = format.getImage(); // transform it 
		        Image newimg = images.getScaledInstance(129, 120,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way
				Logo.setText("");
				Logo.setIcon(new ImageIcon(newimg));
				
				//remplissage de octets1
				File fileU;
				FileInputStream fis = null;
				int i=0;
				fileU = new File(fileName);
					if (fileU.exists()) {
						i++;
						  try {
							fis = new FileInputStream(fileU);
						} catch (FileNotFoundException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						 octets1 = new byte[(int) fileU.length()];
						 ByteArrayOutputStream bos = new ByteArrayOutputStream();
					        byte[] buf = new byte[1024];
					        try {
					            for (int readNum; (readNum = fis.read(buf)) != -1;) {
					                bos.write(buf, 0, readNum); 
					            }
					        } catch (IOException ex) {
					        }
					         octets1 = bos.toByteArray();
					}}
		});
		button.setBounds(465, 156, 104, 23);
		contentPane.add(button);
		
		contentPane.add(image);

		
	}
}
