package client.gui.superadmin;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import client.delegate.UserServiceDelegate;
import client.gui.Main.MainSuperAdmin;
import edu.esprit.tex.domain.Administrator;
import edu.esprit.tex.domain.User;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ViewAdmin extends JFrame {

	private JPanel contentPane;
	static int id;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ViewAdmin frame = new ViewAdmin(id);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ViewAdmin(int id) {
		setTitle("Administrator Detail");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 640, 480);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel.setBounds(30, 30, 560, 380);
		contentPane.add(panel);
		panel.setLayout(null);
		panel.setOpaque(false);
		
		JLabel lblName = new JLabel("Name :");
		lblName.setFont(new Font("Calibri", Font.BOLD, 14));
		lblName.setBounds(41, 56, 67, 26);
		panel.add(lblName);
		
		JLabel lblEmail = new JLabel("E-mail :");
		lblEmail.setFont(new Font("Calibri", Font.BOLD, 14));
		lblEmail.setBounds(41, 90, 67, 26);
		panel.add(lblEmail);
		
		JLabel lblAdress = new JLabel("Adress :");
		lblAdress.setFont(new Font("Calibri", Font.BOLD, 14));
		lblAdress.setBounds(41, 157, 67, 26);
		panel.add(lblAdress);
		
		JLabel lblPassword = new JLabel("Password :");
		lblPassword.setFont(new Font("Calibri", Font.BOLD, 14));
		lblPassword.setBounds(41, 230, 67, 26);
		panel.add(lblPassword);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBounds(435, 41, 76, 85);
		panel.add(panel_1);
		
		JLabel lblPhone = new JLabel("Phone :");
		lblPhone.setFont(new Font("Calibri", Font.BOLD, 14));
		lblPhone.setBounds(41, 193, 67, 26);
		panel.add(lblPhone);
		
		JButton btnOK = new JButton("OK");


		btnOK.setBounds(432, 346, 89, 23);
		panel.add(btnOK);
		
		JLabel tfName = new JLabel("");
		tfName.setBounds(105, 62, 119, 14);
		panel.add(tfName);
		
		JLabel tfEmail = new JLabel("");
		tfEmail.setBounds(105, 96, 119, 14);
		panel.add(tfEmail);
		
		JLabel tfAdress = new JLabel("");
		tfAdress.setBounds(105, 163, 119, 14);
		panel.add(tfAdress);
		
		JLabel tfPhone = new JLabel("");
		tfPhone.setBounds(105, 194, 119, 26);
		panel.add(tfPhone);
		
		JLabel tfPassword = new JLabel("");
		tfPassword.setBounds(118, 236, 98, 23);
		panel.add(tfPassword);
		
		JLabel lblNewLabel = new JLabel("City :");
		lblNewLabel.setFont(new Font("Calibri", Font.BOLD, 14));
		lblNewLabel.setBounds(41, 121, 67, 20);
		panel.add(lblNewLabel);
		
		JLabel tfCity = new JLabel("");
		tfCity.setBounds(92, 121, 76, 20);
		panel.add(tfCity);
		Administrator administrator=UserServiceDelegate.findAdministratorById(id);
		
		tfName.setText(administrator.getName());
		tfAdress.setText(administrator.getAdress());
		tfCity.setText(administrator.getCity());
		tfEmail.setText(administrator.getEmail());
		tfPassword.setText(administrator.getPassword());
		tfPhone.setText(administrator.getPhone());
		
		btnOK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				dispose();
			}
		});
		
		JLabel image = new JLabel( new ImageIcon(MainSuperAdmin.class.getResource("/img/background.jpg")));
		image.setBounds(0, 0, 635, 452);
		contentPane.add(image);
	}

}
