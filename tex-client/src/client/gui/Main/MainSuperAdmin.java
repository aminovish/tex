package client.gui.Main;

import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.table.DefaultTableModel;

import session.Session;

import client.delegate.ParameterDelegate;
import client.delegate.PictureUserServiceDelegate;
import client.delegate.UserServiceDelegate;
import client.gui.admin.configure.UpdateParametre;
import client.gui.superadmin.CreateAdmin;
import client.gui.superadmin.ViewAdmin;
import client.gui.superadmin.registration.UpdateSuperAdmin;
import client.gui.user.authentication.Authentication;
import edu.esprit.tex.domain.Administrator;
import edu.esprit.tex.domain.Parameter;
import edu.esprit.tex.domain.Picture;
import edu.esprit.tex.domain.User;
import javax.swing.JTextField;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;
import javax.swing.border.MatteBorder;

public class MainSuperAdmin extends JFrame {

	private JPanel contentPane;
	private JTable table;
	static int x = 0;
	private JTextField Name;
	private JTextField Email;
	private JTextField Adress;
	private JTextField textField;
	private JTextField Phone;
	private JTextField City;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainSuperAdmin frame = new MainSuperAdmin(x);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainSuperAdmin(int x) {

		setResizable(false);
		setTitle("Super Adminstrator");
		final User user = Session.getInstance().getUser();

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 800, 480);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel.setBounds(185, 30, 599, 380);
		contentPane.add(panel);
		panel.setLayout(null);
		panel.setOpaque(false);

		final JPanel panel_2 = new JPanel();
		panel_2.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_2.setBounds(1, 1, 594, 378);
		panel.add(panel_2);
		panel_2.setLayout(null);
		panel_2.setVisible(false);
		panel_2.setOpaque(false);

		final JLabel lblName = new JLabel("Name :");
		lblName.setForeground(new Color(255, 255, 255));
		lblName.setFont(new Font("Calibri", Font.BOLD, 14));
		lblName.setBounds(30, 11, 72, 28);
		panel_2.add(lblName);

		final JLabel lblEmail = new JLabel("E-mail :");
		lblEmail.setForeground(Color.WHITE);
		lblEmail.setFont(new Font("Calibri", Font.BOLD, 14));
		lblEmail.setBounds(30, 50, 72, 28);
		panel_2.add(lblEmail);

		final JLabel lblAdress = new JLabel("Adress :");
		lblAdress.setForeground(Color.WHITE);
		lblAdress.setFont(new Font("Calibri", Font.BOLD, 14));
		lblAdress.setBounds(30, 89, 72, 28);
		panel_2.add(lblAdress);

		final JLabel lblPhone = new JLabel("Phone  :");
		lblPhone.setForeground(Color.WHITE);
		lblPhone.setFont(new Font("Calibri", Font.BOLD, 14));
		lblPhone.setBounds(30, 174, 72, 28);
		panel_2.add(lblPhone);

		final JPanel panel_3 = new JPanel();
		
		panel_3.setBounds(324, 11, 129, 120);
		panel_2.add(panel_3);
		

		final JButton btnEdit = new JButton("Edit");

		btnEdit.setBounds(495, 344, 89, 23);
		panel_2.add(btnEdit);

		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {

							UpdateSuperAdmin frame = new UpdateSuperAdmin();
							frame.setVisible(true);

						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});

		final JPanel panel_4 = new JPanel();
		panel_4.setBounds(1, 1, 594, 378);
		panel.add(panel_4);
		panel_4.setLayout(null);
		panel_4.setVisible(false);
		panel_4.setOpaque(false);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 475, 356);
		panel_4.add(scrollPane);
		

		
		List<Administrator> administrators = UserServiceDelegate
				.findAdministrators();
		Object[][] data = new Object[administrators.size()][6];
		int i = 0;
		for (Administrator a : administrators) {
			data[i][0] = a.getId();
			
			try {				
				Picture picture=a.getPicture();
				byte[] octets =picture.getPicturee();
				ImageIcon format = new ImageIcon(octets);
				Image images = format.getImage(); // transform it 
		        Image newimg = images.getScaledInstance(50, 50,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way
		        data[i][1] = new ImageIcon(newimg);
				
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			
			
			data[i][2] = a.getName();
			data[i][3] = a.getEmail();
			data[i][4] = a.getPhone();
			data[i][5] = a.getState();

			i++;
		}

		table = new JTable();

		table.setModel(new DefaultTableModel(data, new String[] { "ID",
				"Picture", "Name", "Email", "Phone", "State" }) {
			Class[] columnTypes = new Class[] { Integer.class, ImageIcon.class,
					String.class, String.class, String.class, Boolean.class };

			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
	
		table.getColumnModel().getColumn(0).setPreferredWidth(32);
		table.getColumnModel().getColumn(3).setResizable(false);
		table.getColumnModel().getColumn(3).setPreferredWidth(98);
		table.getColumnModel().getColumn(4).setResizable(false);
		table.getColumnModel().getColumn(5).setResizable(false);
		table.getColumnModel().getColumn(5).setPreferredWidth(36);
		table.getColumnModel().getColumn(1).setPreferredWidth(50);
		table.setRowHeight(50);
		table.removeColumn(table.getColumnModel().getColumn(0));
		
		scrollPane.setViewportView(table);

		final JButton btnCreate = new JButton("New");
		btnCreate.setBounds(495, 55, 89, 23);
		panel_4.add(btnCreate);

		final JButton btnDelete = new JButton("Delete");
		btnDelete.setBounds(495, 123, 89, 23);
		panel_4.add(btnDelete);

		final JButton btnView = new JButton("View");

		btnView.setBounds(495, 150, 89, 23);
		panel_4.add(btnView);

		JButton btnUpdate = new JButton("Update");
		btnUpdate.setBounds(495, 89, 89, 23);
		panel_4.add(btnUpdate);

		final JPanel panel_5 = new JPanel();
		panel_5.setBounds(1, 1, 594, 378);
		panel.add(panel_5);
		panel_5.setLayout(null);
		panel_5.setVisible(false);
		panel_5.setOpaque(false);

		JLabel lblNumberOfStrat = new JLabel("Number of strat Token :");
		lblNumberOfStrat.setForeground(Color.WHITE);
		lblNumberOfStrat.setBounds(21, 36, 168, 32);
		panel_5.add(lblNumberOfStrat);

		JLabel lblTokenValue = new JLabel("token Value :");
		lblTokenValue.setForeground(Color.WHITE);
		lblTokenValue.setBounds(21, 79, 124, 32);
		panel_5.add(lblTokenValue);

		JLabel lblPercentatge = new JLabel("Percentatge :");
		lblPercentatge.setForeground(Color.WHITE);
		lblPercentatge.setBounds(21, 122, 124, 25);
		panel_5.add(lblPercentatge);

		JLabel lblMaxTokenTo = new JLabel("Max Token to convert :");
		lblMaxTokenTo.setForeground(Color.WHITE);
		lblMaxTokenTo.setBounds(21, 158, 168, 25);
		panel_5.add(lblMaxTokenTo);

		JButton btnEdit_1 = new JButton("Edit");

		btnEdit_1.setBounds(495, 344, 89, 23);
		panel_5.add(btnEdit_1);

		JLabel NbToken = new JLabel("");
		NbToken.setFont(new Font("Tahoma", Font.BOLD, 11));
		NbToken.setBackground(Color.WHITE);
		NbToken.setBounds(154, 41, 114, 23);
		panel_5.add(NbToken);

		JLabel TokenValue = new JLabel("");
		TokenValue.setFont(new Font("Tahoma", Font.BOLD, 11));
		TokenValue.setBounds(155, 79, 135, 32);
		panel_5.add(TokenValue);

		JLabel Percentage = new JLabel("");
		Percentage.setFont(new Font("Tahoma", Font.BOLD, 11));
		Percentage.setBounds(155, 118, 135, 32);
		panel_5.add(Percentage);

		JLabel MaxToken = new JLabel("");
		MaxToken.setFont(new Font("Tahoma", Font.BOLD, 11));
		MaxToken.setBounds(154, 154, 135, 32);
		panel_5.add(MaxToken);
		;

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, Color.RED, new Color(0, 128, 128)));
		panel_1.setBounds(10, 30, 165, 380);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		panel_1.setOpaque(false);

		JButton btnProfile = new JButton("  Profile");
		btnProfile.setFont(new Font("Tahoma", Font.PLAIN, 10));
		btnProfile.setIcon(new ImageIcon(MainSuperAdmin.class.getResource("/img/profile.png")));

		btnProfile.setBounds(10, 62, 145, 42);
		panel_1.add(btnProfile);

		JButton btnAdmin = new JButton("Adminstrators");
		btnAdmin.setFont(new Font("Tahoma", Font.PLAIN, 10));
		btnAdmin.setIcon(new ImageIcon(MainSuperAdmin.class.getResource("/img/people.png")));

		btnAdmin.setBounds(10, 115, 145, 42);
		panel_1.add(btnAdmin);

		JButton btnParameters = new JButton("Parameters");
		btnParameters.setFont(new Font("Tahoma", Font.PLAIN, 10));
		btnParameters.setVerticalAlignment(SwingConstants.TOP);
		btnParameters.setIcon(new ImageIcon(MainSuperAdmin.class.getResource("/img/service-manager-icone-5775-32.png")));

		btnParameters.setBounds(10, 168, 145, 42);
		panel_1.add(btnParameters);
		
		JButton btnSingOut = new JButton("Sing Out");
		btnSingOut.setIcon(new ImageIcon(MainSuperAdmin.class.getResource("/img/move_participant_to_waiting.png")));
		btnSingOut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MainSuperAdmin.this.dispose();
				Authentication authentication = new Authentication();
				
				authentication.setVisible(true);
				
			}
		});
		btnSingOut.setBounds(10, 335, 145, 34);
		panel_1.add(btnSingOut);

		final JFrame kFrame = new JFrame("Attention");
		kFrame.setSize(300, 300);
		kFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		if (x == 0) {
			panel_2.setVisible(false);

			panel_3.setVisible(false);

			panel_4.setVisible(false);

			panel_5.setVisible(false);

		} else if (x == 1) {
			panel_2.setVisible(true);

			panel_3.setVisible(true);

			panel_4.setVisible(false);

			panel_5.setVisible(false);

		} else if (x == 2) {
			panel_2.setVisible(false);

			panel_3.setVisible(false);

			panel_4.setVisible(true);

			panel_5.setVisible(false);

		} else if (x == 3) {
			panel_2.setVisible(false);

			panel_3.setVisible(false);

			panel_4.setVisible(false);

			panel_5.setVisible(true);

		}
		
		panel_3.setOpaque(false);
		final JLabel Logo = new JLabel("Your Logo");
		Logo.setHorizontalAlignment(SwingConstants.CENTER);
		Logo.setBounds(0, 0, 129, 120);
		panel_3.add(Logo);
		try {				
			Picture picture=user.getPicture();
			byte[] octets =picture.getPicturee();
			ImageIcon format = new ImageIcon(octets);
			Image images = format.getImage(); // transform it 
	        Image newimg = images.getScaledInstance(129, 120,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way
			Logo.setText("");
			Logo.setIcon(new ImageIcon(newimg));
			
			Name = new JTextField();
			Name.setFont(new Font("Tahoma", Font.BOLD, 11));
			Name.setBounds(81, 15, 129, 20);
			panel_2.add(Name);
			Name.setColumns(10);
			Name.setEditable(false);
			
			Email = new JTextField();
			Email.setFont(new Font("Tahoma", Font.BOLD, 11));
			Email.setBounds(81, 54, 129, 20);
			panel_2.add(Email);
			Email.setColumns(10);
			Email.setEditable(false);
			
			Adress = new JTextField();
			Adress.setFont(new Font("Tahoma", Font.BOLD, 11));
			Adress.setBounds(81, 93, 129, 20);
			panel_2.add(Adress);
			Adress.setColumns(10);
			Adress.setEditable(false);
			
			Phone = new JTextField();
			Phone.setFont(new Font("Tahoma", Font.BOLD, 11));
			Phone.setBounds(81, 178, 129, 20);
			panel_2.add(Phone);
			Phone.setColumns(10);
			Phone.setEditable(false);
			

			
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		

		
		JLabel lblCity = new JLabel("City :");
		lblCity.setForeground(Color.WHITE);
		lblCity.setFont(new Font("Calibri", Font.BOLD, 14));
		lblCity.setBounds(30, 128, 72, 28);
		panel_2.add(lblCity);
		
		City = new JTextField();
		City.setFont(new Font("Tahoma", Font.BOLD, 11));
		City.setText((String) null);
		City.setEditable(false);
		City.setColumns(10);
		City.setBounds(81, 132, 129, 20);
		panel_2.add(City);
		
		Name.setText(user.getName());
		Email.setText(user.getEmail());
		Adress.setText(user.getAdress());
		Phone.setText(user.getPhone());
		City.setText(user.getCity());

		Parameter parameter = ParameterDelegate.FindParameter();

		NbToken.setText(parameter.getTokenNumber() + "");
		TokenValue.setText(parameter.getTokenValue() + "");
		Percentage.setText(parameter.getPercentage() + "");
		MaxToken.setText(parameter.getMaxToken() + "");
		
	
		JLabel image = new JLabel( new ImageIcon(MainSuperAdmin.class.getResource("/img/background.jpg")));
		image.setBounds(0, 0, MainSuperAdmin.this.getWidth(), MainSuperAdmin.this.getHeight());
		contentPane.add(image);

		btnProfile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel_2.setVisible(true);

				panel_3.setVisible(true);

				panel_4.setVisible(false);

				panel_5.setVisible(false);

			}
		});

		btnAdmin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				panel_2.setVisible(false);

				panel_3.setVisible(false);

				panel_4.setVisible(true);

				panel_5.setVisible(false);

			}
		});

		btnParameters.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				panel_2.setVisible(false);

				panel_3.setVisible(false);

				panel_4.setVisible(false);

				panel_5.setVisible(true);
			}
		});

		btnCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {

							CreateAdmin frame = new CreateAdmin(-1);
							frame.setVisible(true);

						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});

			}
		});

		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							if (table.getSelectedRow() < 0) {
								int anser = JOptionPane.showConfirmDialog(
										kFrame,
										"you most select an Adminstrator",
										"View", JOptionPane.CLOSED_OPTION);
								if (anser == JOptionPane.OK_CANCEL_OPTION) {
									kFrame.dispose();

								}
							} else {
								int id = Integer.parseInt(String.valueOf(table
										.getModel().getValueAt(
												table.getSelectedRow(), 0)));
								CreateAdmin frame = new CreateAdmin(id);
								frame.setVisible(true);

							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});

			}
		});

		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				int id = Integer.parseInt(String.valueOf(table.getModel()
						.getValueAt(table.getSelectedRow(), 0)));

				Administrator administrator = UserServiceDelegate.getProxy()
						.findAdministratorById(id);

				List<Picture> pictures = PictureUserServiceDelegate
						.getAllPictureUser();
				for (Picture p : pictures) {
					if (p.getUser().getId() == administrator.getId()) {
						PictureUserServiceDelegate.deletePictureUser(p);
					}
				}
				UserServiceDelegate.getProxy().delete(administrator);
				MainSuperAdmin mainSuperAdmin = new MainSuperAdmin(2);
				mainSuperAdmin.setVisible(true);

			}
		});

		btnView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							if (table.getSelectedRow() < 0) {
								int anser = JOptionPane.showConfirmDialog(
										kFrame,
										"you most select an Adminstrator",
										"View", JOptionPane.CLOSED_OPTION);
								if (anser == JOptionPane.OK_CANCEL_OPTION) {
									kFrame.dispose();

								}
							} else {
								int id = Integer.parseInt(String.valueOf(table
										.getModel().getValueAt(
												table.getSelectedRow(), 0)));
								ViewAdmin frame = new ViewAdmin(id);
								frame.setVisible(true);

							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});

			}
		});
		btnEdit_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {

							UpdateParametre frame = new UpdateParametre();
							frame.setVisible(true);

						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
				

			}
		});

	}
}
