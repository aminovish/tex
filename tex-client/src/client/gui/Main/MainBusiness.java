package client.gui.Main;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.table.DefaultTableModel;

import session.Session;
import client.delegate.BusinessServiceDelegate;
import client.delegate.PictureUserServiceDelegate;
import client.delegate.UserServiceDelegate;
import client.gui.business.service.BusinessCreateUpdateService;
import client.gui.business.service.ServiceView;
import client.gui.user.authentication.Authentication;
import edu.esprit.tex.domain.Business;
import edu.esprit.tex.domain.Picture;
import edu.esprit.tex.domain.Service;
import edu.esprit.tex.domain.User;

import java.awt.Color;

public class MainBusiness extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JTextField code;
	private JTextField Phone;
	private JTextField Adress;
	private JTextField Email;
	private JTextField Name;
	Business user = (Business) Session.getInstance().getUser();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					MainBusiness frame = new MainBusiness();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainBusiness() {
		setResizable(false);
		setTitle("Business");
		final Business user = (Business) Session.getInstance().getUser();
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 790, 480);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		final JFrame kFrame = new JFrame("Notification");
		kFrame.setSize(300, 300);
		kFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

		JPanel panel = new JPanel();
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel.setBounds(179, 30, 585, 380);
		contentPane.add(panel);
		panel.setLayout(null);
		panel.setOpaque(false);

		final JPanel panel_2 = new JPanel();
		panel_2.setBounds(1, 1, 584, 378);
		panel.add(panel_2);
		panel_2.setLayout(null);
		panel_2.setVisible(false);
		panel_2.setOpaque(false);

		final JLabel lblName = new JLabel("Name :");
		lblName.setFont(new Font("Calibri", Font.BOLD, 14));
		lblName.setBounds(30, 11, 72, 28);
		panel_2.add(lblName);

		final JLabel lblEmail = new JLabel("E-mail :");
		lblEmail.setFont(new Font("Calibri", Font.BOLD, 14));
		lblEmail.setBounds(30, 50, 72, 28);
		panel_2.add(lblEmail);

		final JLabel lblAdress = new JLabel("Adress :");
		lblAdress.setFont(new Font("Calibri", Font.BOLD, 14));
		lblAdress.setBounds(30, 89, 72, 28);
		panel_2.add(lblAdress);

		final JLabel lblPhone = new JLabel("Phone  :");
		lblPhone.setFont(new Font("Calibri", Font.BOLD, 14));
		lblPhone.setBounds(30, 128, 72, 28);
		panel_2.add(lblPhone);

		final JLabel lblDescription = new JLabel("Description :");
		lblDescription.setFont(new Font("Calibri", Font.BOLD, 14));
		lblDescription.setBounds(30, 207, 89, 28);
		panel_2.add(lblDescription);

		final JLabel lblTaxCode = new JLabel("Tax Code :");
		lblTaxCode.setFont(new Font("Calibri", Font.BOLD, 14));
		lblTaxCode.setBounds(30, 168, 72, 28);
		panel_2.add(lblTaxCode);

		final JPanel panel_3 = new JPanel();
		panel_3.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null,
				null, null));
		panel_3.setBounds(447, 11, 129, 120);
		panel_2.add(panel_3);
		panel_3.setLayout(null);

		final JLabel Logo = new JLabel("Logo");
		Logo.setHorizontalAlignment(SwingConstants.CENTER);
		Logo.setBounds(0, 0, 129, 120);
		panel_3.add(Logo);
		panel_3.setVisible(false);

		final JButton btnEdit = new JButton("Edit");
		btnEdit.setBounds(324, 333, 89, 23);
		panel_2.add(btnEdit);

		code = new JTextField();
		code.setEditable(false);
		code.setColumns(10);
		code.setBounds(134, 172, 205, 20);
		panel_2.add(code);

		Phone = new JTextField();
		Phone.setEditable(false);
		Phone.setColumns(10);
		Phone.setBounds(134, 132, 205, 20);
		panel_2.add(Phone);

		Adress = new JTextField();
		Adress.setEditable(false);
		Adress.setColumns(10);
		Adress.setBounds(134, 93, 205, 20);
		panel_2.add(Adress);

		Email = new JTextField();
		Email.setEditable(false);
		Email.setColumns(10);
		Email.setBounds(134, 54, 205, 20);
		panel_2.add(Email);

		Name = new JTextField();
		Name.setEditable(false);
		Name.setColumns(10);
		Name.setBounds(134, 15, 205, 20);
		panel_2.add(Name);

		final JTextArea textdes = new JTextArea();
		textdes.setEditable(false);
		textdes.setBounds(134, 209, 279, 120);
		panel_2.add(textdes);

		final JPanel panel_4 = new JPanel();
		panel_4.setBounds(1, 1, 584, 378);
		panel.add(panel_4);
		panel_4.setLayout(null);
		panel_4.setVisible(false);
		panel_4.setOpaque(false);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 432, 259);
		panel_4.add(scrollPane);

		table = new JTable();
		List<Service> services = BusinessServiceDelegate
				.findServicesByBusiness(user);
		Object[][] data = new Object[services.size()][4];
		int i = 0;
		for (Service s : services) {
			data[i][0] = s.getId();
			data[i][1] = s.getName();
			data[i][2] = s.getValue();
			data[i][3] = s.getDate();
			i++;
		}
		table.setModel(new DefaultTableModel(data, new String[] { "Id",
				"Service Name", "Value", "Date" }));
		table.removeColumn(table.getColumnModel().getColumn(0));

		scrollPane.setViewportView(table);

		final JButton btnNew = new JButton("New");
		btnNew.setHorizontalAlignment(SwingConstants.LEADING);
		btnNew.setIcon(new ImageIcon(MainBusiness.class
				.getResource("/img/ajouter.png")));

		btnNew.setBounds(452, 23, 129, 33);
		panel_4.add(btnNew);

		final JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							if (table.getSelectedRow() < 0) {
								int anser = JOptionPane.showConfirmDialog(
										kFrame, "You must select a Service",
										"Notification",
										JOptionPane.CLOSED_OPTION);
								if (anser == JOptionPane.OK_CANCEL_OPTION) {
									kFrame.dispose();

								}
							} else {
								int id = Integer.parseInt(String.valueOf(table
										.getModel().getValueAt(
												table.getSelectedRow(), 0)));

								Service service = BusinessServiceDelegate
										.findById(id);
								BusinessServiceDelegate.deleteService(service);

							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});

			}
		});
		btnDelete.setHorizontalAlignment(SwingConstants.LEADING);
		btnDelete.setIcon(new ImageIcon(MainBusiness.class
				.getResource("/img/delete.png")));
		btnDelete.setBounds(452, 111, 129, 33);
		panel_4.add(btnDelete);

		final JButton btnView = new JButton("View");
		btnView.setHorizontalAlignment(SwingConstants.LEADING);
		btnView.setIcon(new ImageIcon(MainBusiness.class
				.getResource("/img/info-icon.png")));
		btnView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							if (table.getSelectedRow() < 0) {
								int anser = JOptionPane.showConfirmDialog(
										kFrame, "You must select a Service",
										"Notification",
										JOptionPane.CLOSED_OPTION);
								if (anser == JOptionPane.OK_CANCEL_OPTION) {
									kFrame.dispose();

								}
							} else {
								int id = Integer.parseInt(String.valueOf(table
										.getModel().getValueAt(
												table.getSelectedRow(), 0)));

								ServiceView businessView = new ServiceView(id);
								dispose();
								businessView.setVisible(true);

							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});

			}
		});
		btnView.setBounds(452, 155, 129, 33);
		panel_4.add(btnView);

		JButton btnUpdate = new JButton("Update");
		btnUpdate.setHorizontalAlignment(SwingConstants.LEADING);
		btnUpdate.setIcon(new ImageIcon(MainBusiness.class
				.getResource("/img/edit.png")));
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							if (table.getSelectedRow() < 0) {
								int anser = JOptionPane.showConfirmDialog(
										kFrame, "You must select a Service",
										"Notification",
										JOptionPane.CLOSED_OPTION);
								if (anser == JOptionPane.OK_CANCEL_OPTION) {
									kFrame.dispose();

								}
							} else {
								int id = Integer.parseInt(String.valueOf(table
										.getModel().getValueAt(
												table.getSelectedRow(), 0)));

								BusinessCreateUpdateService frame = new BusinessCreateUpdateService(
										id);
								dispose();
								frame.setVisible(true);

							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});

			}

		});
		btnUpdate.setBounds(452, 67, 129, 33);
		panel_4.add(btnUpdate);

		btnNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {

							BusinessCreateUpdateService frame = new BusinessCreateUpdateService(
									-1);
							dispose();
							frame.setVisible(true);

						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});

			}
		});

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, Color.RED, new Color(0, 102, 102)));
		panel_1.setBounds(10, 30, 162, 380);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		panel_1.setOpaque(false);

		JButton btnProfile = new JButton("Profile");
		btnProfile.setHorizontalAlignment(SwingConstants.LEADING);
		btnProfile.setIcon(new ImageIcon(MainBusiness.class
				.getResource("/img/people.png")));

		btnProfile.setBounds(10, 62, 129, 33);
		panel_1.add(btnProfile);

		JButton btnService = new JButton("Service");
		btnService.setHorizontalAlignment(SwingConstants.LEADING);
		btnService.setIcon(new ImageIcon(MainBusiness.class
				.getResource("/img/service.png")));

		btnService.setBounds(10, 115, 129, 33);
		panel_1.add(btnService);

		JButton btnSingOut = new JButton("Sing out");
		btnSingOut.setIcon(new ImageIcon(MainBusiness.class.getResource("/img/out.png")));
		btnSingOut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Authentication authentication = new Authentication();
				MainBusiness.this.dispose();
				
				
				authentication.setVisible(true);
			}
		});
		btnSingOut.setBounds(10, 330, 129, 39);
		panel_1.add(btnSingOut);

		// Description.setText(business.getDescription());
		// Phone.setText(business.getPhone());
		// Adress.setText(business.getAdress());
		// Email.setText(business.getEmail());
		// Name.setText(business.getName());
		// label.setText(business.getTaxCode());

		btnProfile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel_2.setVisible(true);
				panel_3.setVisible(false);
				panel_4.setVisible(false);
				Name.setText(user.getName());
				Email.setText(user.getEmail());
				Adress.setText(user.getAdress());
				Phone.setText(user.getPhone());
				code.setText(user.getTaxCode());
				textdes.setText(user.getDescription());

				try {
					System.out.println("one");
					Picture picture = new Picture();
					System.out.println("2");
					picture = PictureUserServiceDelegate
							.getPictureUserById(user.getId());
					System.out.println("3");
					ImageIcon format = new ImageIcon(picture.getPicturee());
					System.out.println("4");
					Logo.setIcon(format);
					Logo.setText("");

				} catch (Exception e1) {

				}

			}
		});

		btnService.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel_2.setVisible(false);

				panel_3.setVisible(false);

				panel_4.setVisible(true);

			}
		});
		
		JLabel image = new JLabel( new ImageIcon(MainSuperAdmin.class.getResource("/img/background.jpg")));
		image.setBounds(0, 0, 784, 452);
		contentPane.add(image);

	}
}
