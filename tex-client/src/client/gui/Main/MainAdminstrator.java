package client.gui.Main;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.table.DefaultTableModel;

import session.Session;
import client.delegate.AdminArticleDelegate;
import client.delegate.PictureUserServiceDelegate;
import client.delegate.UserServiceDelegate;
import client.gui.adminisrator.article.AdministratorArticle;
import client.gui.business.BuisinessView;
import client.gui.user.authentication.Authentication;
import edu.esprit.tex.domain.Administrator;
import edu.esprit.tex.domain.Article;
import edu.esprit.tex.domain.Business;
import edu.esprit.tex.domain.Picture;
import edu.esprit.tex.domain.User;

public class MainAdminstrator extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JTable table_1;
	private JTextField Name;
	private JTextField Email;
	private JTextField Adress;
	private JTextField Phone;
	private JTextField City;
	private JTable table_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainAdminstrator frame = new MainAdminstrator();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainAdminstrator() {
		setResizable(false);
		Administrator administrator =(Administrator) Session.getInstance().getUser();
		setTitle("Administrateur");
		final User user = Session.getInstance().getUser();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 705, 482);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		final JFrame kFrame = new JFrame("Notification");
		kFrame.setSize(300, 300);
		kFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

		final JPanel panel = new JPanel();
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel.setBounds(193, 30, 486, 380);
		contentPane.add(panel);
		panel.setLayout(null);
		panel.setOpaque(false);

		final JPanel Business = new JPanel();
		Business.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		Business.setBounds(1, 1, 475, 378);
		panel.add(Business);
		Business.setLayout(null);
		Business.setVisible(false);
		Business.setOpaque(false);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(24, 11, 441, 309);
		Business.add(scrollPane);
		List<Business> businesses = UserServiceDelegate.findBusinesss();
		int y = 0;
		int x = 0;
		for (Business bu : businesses) {
			if (!bu.getState()) {
				y++;
			} else {
				x++;
			}
		}
		Object[][] donnee2 = new Object[x][5];
		Object[][] donnee = new Object[y][5];
		int i = 0;
		int j = 0;
		for (Business b : businesses) {
			if (!b.getState()) {
				donnee[i][0] = b.getName();
				donnee[i][1] = b.getEmail();
				donnee[i][2] = ((Business) b).getTaxCode();
				donnee[i][3] = b.getPhone();
				donnee[i][4] = b.getId();
				i++;

			} else {
				if (b.getAdmin().getId()== administrator.getId()) {
					donnee2[j][0] = b.getName();
					donnee2[j][1] = b.getEmail();
					donnee2[j][2] = ((Business) b).getTaxCode();
					donnee2[j][3] = b.getPhone();
					donnee2[j][4] = b.getId();
					j++;
				}
			}
		}

		table = new JTable();
		table.setModel(new DefaultTableModel(donnee2, new String[] { "Name",
				"Email", "Tax Code", "Phone", "id" }));
		table.removeColumn(table.getColumnModel().getColumn(4));

		scrollPane.setViewportView(table);

		final JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							if (table.getSelectedRow() < 0) {
								int anser = JOptionPane.showConfirmDialog(
										kFrame, "You must select a Business",
										"Validate", JOptionPane.CLOSED_OPTION);
								if (anser == JOptionPane.OK_CANCEL_OPTION) {
									kFrame.dispose();

								}
							} else {
								int id = Integer.parseInt(String.valueOf(table
										.getModel().getValueAt(
												table.getSelectedRow(), 4)));
								Business business = UserServiceDelegate
										.findBusinessById(id);
								UserServiceDelegate.deleteBusiness(business);
								int anser = JOptionPane.showConfirmDialog(
										kFrame, "Business Deleted", "Validate",
										JOptionPane.CLOSED_OPTION);
								MainAdminstrator mainAdminstrator = new MainAdminstrator();
								dispose();
								mainAdminstrator.setVisible(true);

							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});

			}
		});
		btnDelete.setIcon(new ImageIcon(MainAdminstrator.class
				.getResource("/img/delete.png")));
		btnDelete.setBounds(260, 331, 129, 33);
		Business.add(btnDelete);

		final JButton btnView = new JButton("View");
		btnView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							if (table.getSelectedRow() < 0) {
								int anser = JOptionPane.showConfirmDialog(
										kFrame, "You must select a Business",
										"Validate", JOptionPane.CLOSED_OPTION);
								if (anser == JOptionPane.OK_CANCEL_OPTION) {
									kFrame.dispose();

								}
							} else {
								int id = Integer.parseInt(String.valueOf(table
										.getModel().getValueAt(
												table.getSelectedRow(), 4)));
								Business business = UserServiceDelegate
										.findBusinessById(id);
								BuisinessView buisinessView = new BuisinessView(id);
								buisinessView.setVisible(true);

							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
				
				
			}
		});
		btnView.setIcon(new ImageIcon(MainAdminstrator.class
				.getResource("/img/info-icon.png")));
		btnView.setBounds(103, 331, 129, 33);
		Business.add(btnView);

		final JPanel Profile = new JPanel();
		Profile.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		Profile.setBounds(1, 1, 475, 378);
		panel.add(Profile);
		Profile.setLayout(null);
		Profile.setVisible(false);
		Profile.setOpaque(false);

		Name = new JTextField();
		Name.setEditable(false);
		Name.setBounds(91, 15, 180, 20);
		Profile.add(Name);
		Name.setColumns(10);

		Email = new JTextField();
		Email.setEditable(false);
		Email.setColumns(10);
		Email.setBounds(91, 54, 180, 20);
		Profile.add(Email);

		Adress = new JTextField();
		Adress.setEditable(false);
		Adress.setColumns(10);
		Adress.setBounds(91, 132, 180, 20);
		Profile.add(Adress);

		Phone = new JTextField();
		Phone.setEditable(false);
		Phone.setColumns(10);
		Phone.setBounds(91, 93, 180, 20);
		Profile.add(Phone);

		City = new JTextField();
		City.setEditable(false);
		City.setColumns(10);
		City.setBounds(91, 164, 180, 20);
		Profile.add(City);

		final JLabel lblName = new JLabel("Name :");
		lblName.setFont(new Font("Calibri", Font.BOLD, 14));
		lblName.setBounds(30, 11, 72, 28);
		Profile.add(lblName);

		final JLabel lblEmail = new JLabel("E-mail :");
		lblEmail.setFont(new Font("Calibri", Font.BOLD, 14));
		lblEmail.setBounds(30, 50, 72, 28);
		Profile.add(lblEmail);

		final JLabel lblAdress = new JLabel("Adress :");
		lblAdress.setFont(new Font("Calibri", Font.BOLD, 14));
		lblAdress.setBounds(30, 128, 72, 28);
		Profile.add(lblAdress);

		final JLabel lblPhone = new JLabel("Phone  :");
		lblPhone.setFont(new Font("Calibri", Font.BOLD, 14));
		lblPhone.setBounds(30, 89, 72, 28);
		Profile.add(lblPhone);

		final JPanel panel_3 = new JPanel();
		panel_3.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null,
				null, null));
		panel_3.setBounds(324, 11, 129, 120);
		Profile.add(panel_3);
		panel_3.setLayout(null);

		JLabel lblLogo = new JLabel("Logo");
		lblLogo.setHorizontalAlignment(SwingConstants.CENTER);
		lblLogo.setBounds(0, 0, 129, 120);
		panel_3.add(lblLogo);

		final JButton btnEdit = new JButton("Edit");
		btnEdit.setIcon(new ImageIcon(MainAdminstrator.class
				.getResource("/img/modifier-crayon-ecrivez-icone-6700-16.png")));
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnEdit.setBounds(364, 333, 89, 23);
		Profile.add(btnEdit);

		JLabel lblCity = new JLabel("City :");
		lblCity.setFont(new Font("Calibri", Font.BOLD, 14));
		lblCity.setBounds(30, 167, 46, 14);
		Profile.add(lblCity);

		final JPanel Activation = new JPanel();
		Activation.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		Activation.setBounds(0, 0, 486, 379);
		panel.add(Activation);
		Activation.setLayout(null);
		Activation.setVisible(false);
		Activation.setOpaque(false);

		final JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(10, 84, 466, 193);
		Activation.add(scrollPane_2);

		table_2 = new JTable();
		table_2.setModel(new DefaultTableModel(donnee, new String[] { "Name",
				"Email", "Tax Code", "Phone", "id" }));
		table_2.removeColumn(table_2.getColumnModel().getColumn(4));
		scrollPane_2.setViewportView(table_2);

		final JButton button = new JButton("Validate");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							if (table_2.getSelectedRow() < 0) {
								int anser = JOptionPane.showConfirmDialog(
										kFrame, "You must select a Business",
										"Validate", JOptionPane.CLOSED_OPTION);
								if (anser == JOptionPane.OK_CANCEL_OPTION) {
									kFrame.dispose();

								}
							} else {
								int id = Integer.parseInt(String
										.valueOf(table_2.getModel().getValueAt(
												table_2.getSelectedRow(), 4)));
								Business business = UserServiceDelegate
										.findBusinessById(id);
								business.setState(true);
								business.setAdmin((Administrator) Session
										.getInstance().getUser());
								UserServiceDelegate.update(business);

								int anser = JOptionPane.showConfirmDialog(
										kFrame, "Business Validated",
										"Validate", JOptionPane.CLOSED_OPTION);
								MainAdminstrator mainAdminstrator = new MainAdminstrator();
								dispose();
								mainAdminstrator.setVisible(true);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});

			}
		});
		button.setHorizontalAlignment(SwingConstants.LEADING);
		button.setIcon(new ImageIcon(MainAdminstrator.class
				.getResource("/img/validate.png")));
		button.setBounds(347, 288, 129, 33);
		Activation.add(button);

		final JButton button_1 = new JButton("Details");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							if (table_2.getSelectedRow() < 0) {
								int anser = JOptionPane.showConfirmDialog(
										kFrame, "You must select a Business",
										"Validate", JOptionPane.CLOSED_OPTION);
								if (anser == JOptionPane.OK_CANCEL_OPTION) {
									kFrame.dispose();

								}
							} else {
								int id = Integer.parseInt(String.valueOf(table_2
										.getModel().getValueAt(
												table_2.getSelectedRow(), 4)));
								Business business = UserServiceDelegate
										.findBusinessById(id);
								BuisinessView buisinessView = new BuisinessView(id);
								buisinessView.setVisible(true);

							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
				
			}
		});
		button_1.setHorizontalAlignment(SwingConstants.LEADING);
		button_1.setIcon(new ImageIcon(MainAdminstrator.class
				.getResource("/img/info-icon.png")));
		button_1.setBounds(211, 288, 129, 33);
		Activation.add(button_1);

		final JPanel Article = new JPanel();
		Article.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		Article.setBounds(1, 1, 475, 378);
		Article.setBackground(new Color(240, 240, 240));
		Article.setOpaque(false);

		panel.add(Article);
		Article.setLayout(null);
		Article.setVisible(false);

		final JButton btnNew_1 = new JButton("New");
		btnNew_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AdministratorArticle administratorArticle = new AdministratorArticle();
				administratorArticle.setVisible(true);
				
			}
		});
		btnNew_1.setBounds(319, 72, 89, 23);
		Article.add(btnNew_1);

		final JButton btnUpdate = new JButton("Update");
		btnUpdate.setBounds(319, 106, 89, 23);
		Article.add(btnUpdate);

		final JButton btnDelete_1 = new JButton("Delete");
		btnDelete_1.setBounds(227, 5, 63, 23);
		Article.add(btnDelete_1);

		final JButton btnView_1 = new JButton("View");

		btnView_1.setBounds(319, 174, 89, 23);
		btnDelete_1.setBounds(319, 140, 89, 23);
		Article.add(btnView_1);

		final JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(24, 11, 285, 356);
		Article.add(scrollPane_1);
		int w=0;
		List<edu.esprit.tex.domain.Article> articles = AdminArticleDelegate.FindAllarticle();
		for(Article article3:articles){
			if(article3.getAdmin().getId()==administrator.getId()){
				w++;
			}
		}
		Object[][] donnee3 = new Object[w][3];
		int n=0;
		for(Article article3:articles){
			if(article3.getAdmin().getId()==administrator.getId()){
				donnee3[n][0]=article3.getId();
				donnee3[n][1]=article3.getTitle();
				donnee3[n][2]=article3.getDate();
				n++;
			}
		}
		table_1 = new JTable();
		table_1.setModel(new DefaultTableModel(donnee3, new String[] {
				"Id", "Title", "Date"}));
		scrollPane_1.setViewportView(table_1);

		btnView_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});

		final JLabel Logo = new JLabel("");
		Logo.setHorizontalAlignment(SwingConstants.CENTER);
		Logo.setIcon(new ImageIcon(MainAdminstrator.class
				.getResource("/img/tex.png")));
		Logo.setBounds(10, 150, 466, 189);
		panel.add(Logo);

		final JLabel lblWelcome = new JLabel("Welcome :" + user.getName());
		lblWelcome.setHorizontalAlignment(SwingConstants.CENTER);
		lblWelcome.setFont(new Font("Gisha", Font.BOLD | Font.ITALIC, 37));
		lblWelcome.setBounds(10, 20, 466, 125);
		panel.add(lblWelcome);

		JPanel Panel = new JPanel();
		Panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null,
				null));
		Panel.setBounds(10, 30, 173, 380);
		contentPane.add(Panel);
		Panel.setLayout(null);
		Panel.setOpaque(false);

		JButton btnProfile = new JButton("Profile");
		btnProfile.setHorizontalAlignment(SwingConstants.LEADING);
		btnProfile.setIcon(new ImageIcon(MainAdminstrator.class
				.getResource("/img/people.png")));

		btnProfile.setBounds(10, 62, 153, 42);
		Panel.add(btnProfile);

		JButton btnArticle = new JButton("Article");
		btnArticle.setHorizontalAlignment(SwingConstants.LEADING);
		btnArticle.setIcon(new ImageIcon(MainAdminstrator.class
				.getResource("/img/article.png")));

		btnArticle.setBounds(10, 168, 153, 42);
		Panel.add(btnArticle);

		btnArticle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				Article.setVisible(true);
				Activation.setVisible(false);
				Business.setVisible(false);
				Profile.setVisible(false);

			}
		});

		JButton btnbusiness = new JButton("Business List");
		btnbusiness.setHorizontalAlignment(SwingConstants.LEADING);
		btnbusiness.setIcon(new ImageIcon(MainAdminstrator.class
				.getResource("/img/texte-padding-top-icone-7068-32.png")));

		btnbusiness.setBounds(10, 115, 153, 42);
		Panel.add(btnbusiness);

		JButton btnNewButton = new JButton("Pending");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Activation.setVisible(true);
				Profile.setVisible(false);
				Business.setVisible(false);
				Article.setVisible(false);

			}
		});
		btnNewButton.setHorizontalAlignment(SwingConstants.LEADING);
		btnNewButton.setIcon(new ImageIcon(MainAdminstrator.class
				.getResource("/img/move_participant_to_waiting.png")));
		btnNewButton.setBounds(10, 221, 153, 42);
		Panel.add(btnNewButton);
		
		JButton btnDeconnexion = new JButton("Sign Out");
		btnDeconnexion.setHorizontalAlignment(SwingConstants.LEADING);
		btnDeconnexion.setIcon(new ImageIcon(MainAdminstrator.class.getResource("/img/out.png")));

		btnDeconnexion.setBounds(10, 327, 153, 42);
		Panel.add(btnDeconnexion);

		// Phone.setText(customer.getPhone());
		// Adress.setText(customer.getAdress());
		// Email.setText(customer.getEmail());
		// Name.setText(customer.getName());

		btnProfile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Profile.setVisible(true);
				panel_3.setVisible(true);
				Business.setVisible(false);
				Article.setVisible(false);
				Activation.setVisible(false);
				Name.setText(Session.getInstance().getUser().getName());
				Email.setText(Session.getInstance().getUser().getEmail());
				Phone.setText(Session.getInstance().getUser().getPhone());
				Adress.setText(Session.getInstance().getUser().getAdress());
				City.setText(Session.getInstance().getUser().getCity());
				try {
					Picture picture = new Picture();
					picture = PictureUserServiceDelegate
							.getPictureUserById(Session.getInstance().getUser()
									.getId());
					ImageIcon format = new ImageIcon(picture.getPicturee());
					Image images = format.getImage(); // transform it 
			        Image newimg = images.getScaledInstance(129, 120,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way
					Logo.setText("");
					Logo.setIcon(new ImageIcon(newimg));
				} catch (Exception e2) {

				}

			}
		});

		btnbusiness.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Business.setVisible(true);
				Profile.setVisible(false);
				Article.setVisible(false);
				Activation.setVisible(false);

			}
		});
		
		btnDeconnexion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
				Authentication authentication = new Authentication();
				authentication.setVisible(true);
				
			}
		});
		
		JLabel image = new JLabel( new ImageIcon(MainSuperAdmin.class.getResource("/img/background.jpg")));
		image.setBounds(0, 0, 699, 454);
		contentPane.add(image);
	}
}
