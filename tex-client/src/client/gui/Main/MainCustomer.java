package client.gui.Main;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.table.DefaultTableModel;


import client.delegate.BusinessServiceDelegate;
import client.delegate.CustomerForumDelegate;
import client.delegate.CustomerServiceDelegate;
import client.delegate.PictureUserServiceDelegate;
import client.delegate.UserServiceDelegate;
import client.gui.business.service.BusinessCreateUpdateService;
import client.gui.customer.UpdateCustomerProfile;
import client.gui.customer.services.CreateClientService;
import client.gui.customer.services.ModifyClientService;
import client.gui.customer.services.ViewClientService;
import client.gui.user.authentication.Authentication;
import edu.esprit.tex.domain.Customer;
import edu.esprit.tex.domain.Picture;
import edu.esprit.tex.domain.Post;
import edu.esprit.tex.domain.Service;
import edu.esprit.tex.domain.User;

import java.awt.Color;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;

import session.Session;
import javax.swing.JTextField;

public class MainCustomer extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JTable table_1;
	private JTextField Name;
	private JTextField Email;
	private JTextField Adress;
	private JTextField Phone;
	private JTextField City;
	private JTextField Token;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainCustomer frame = new MainCustomer();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainCustomer() {
		setTitle("Customer");
		setResizable(false);
		final User user =Session.getInstance().getUser();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 826, 480);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		final JFrame kFrame = new JFrame("Notification");
		kFrame.setSize(300, 300);
		kFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		JPanel panel = new JPanel();
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel.setBounds(188, 30, 612, 380);
		contentPane.add(panel);
		panel.setLayout(null);
		panel.setOpaque(false);
		
		final JPanel Profile = new JPanel();
		Profile.setBounds(1, 1, 601, 378);
		panel.add(Profile);
		Profile.setLayout(null);
		Profile.setOpaque(false);
		Profile.setVisible(false);
		
		final JLabel lblName = new JLabel("Name :");
		lblName.setFont(new Font("Calibri", Font.BOLD, 14));
		lblName.setBounds(30, 11, 72, 28);
		Profile.add(lblName);
		
		
		final JLabel lblEmail = new JLabel("E-mail :");
		lblEmail.setFont(new Font("Calibri", Font.BOLD, 14));
		lblEmail.setBounds(30, 50, 72, 28);
		Profile.add(lblEmail);
		
		final JLabel lblAdress = new JLabel("Adress :");
		lblAdress.setFont(new Font("Calibri", Font.BOLD, 14));
		lblAdress.setBounds(30, 89, 72, 28);
		Profile.add(lblAdress);
		
		final JLabel lblPhone = new JLabel("Phone  :");
		lblPhone.setFont(new Font("Calibri", Font.BOLD, 14));
		lblPhone.setBounds(30, 159, 72, 28);
		Profile.add(lblPhone);
		
		
		final JPanel panel_3 = new JPanel();
		panel_3.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel_3.setBounds(440, 11, 129, 120);
		Profile.add(panel_3);
		panel_3.setLayout(null);
		
		final JLabel Logo = new JLabel("Logo");
		Logo.setHorizontalAlignment(SwingConstants.CENTER);
		Logo.setBounds(0, 0, 129, 120);
		panel_3.add(Logo); 
		
		final JButton btnEdit = new JButton("Edit");
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UpdateCustomerProfile updateCustomerProfile = new UpdateCustomerProfile();
				updateCustomerProfile.setVisible(true);
			}
		});
		btnEdit.setIcon(new ImageIcon(MainCustomer.class.getResource("/img/edit.png")));
		btnEdit.setHorizontalAlignment(SwingConstants.LEADING);
		btnEdit.setBounds(458, 334, 111, 33);
		Profile.add(btnEdit);
		
		Token = new JTextField();
		Token.setEditable(false);
		Token.setColumns(10);
		Token.setBounds(112, 304, 210, 20);
		Profile.add(Token);
		
		Name = new JTextField();
		Name.setEditable(false);
		Name.setBounds(109, 15, 210, 20);
		Profile.add(Name);
		Name.setColumns(10);
		
		Email = new JTextField();
		Email.setEditable(false);
		Email.setColumns(10);
		Email.setBounds(109, 50, 210, 20);
		Profile.add(Email);
		
		Adress = new JTextField();
		Adress.setEditable(false);
		Adress.setColumns(10);
		Adress.setBounds(109, 93, 210, 20);
		Profile.add(Adress);
		
		Phone = new JTextField();
		Phone.setEditable(false);
		Phone.setColumns(10);
		Phone.setBounds(112, 163, 210, 20);
		Profile.add(Phone);
		
		City = new JTextField();
		City.setEditable(false);
		City.setColumns(10);
		City.setBounds(112, 132, 210, 20);
		Profile.add(City);
		
		JLabel lblCity = new JLabel("City");
		lblCity.setFont(new Font("Calibri", Font.BOLD, 14));
		lblCity.setBounds(30, 128, 72, 28);
		Profile.add(lblCity);
		
		JLabel lblTokens = new JLabel("Tokens : ");
		lblTokens.setFont(new Font("Calibri", Font.BOLD, 14));
		lblTokens.setBounds(30, 300, 72, 28);
		Profile.add(lblTokens);
		
		
			

		final JPanel Posts = new JPanel();
		Posts.setBounds(1, 1, 601, 378);
		Posts.setBackground(new Color(240, 240, 240));
		Posts.setOpaque(false);
		
		panel.add(Posts);
		Posts.setVisible(false);
		Posts.setLayout(null);
		
		final JButton btnNew_1 = new JButton("New");
		btnNew_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNew_1.setIcon(new ImageIcon(MainCustomer.class.getResource("/img/ajouter.png")));
		btnNew_1.setHorizontalAlignment(SwingConstants.LEADING);
		btnNew_1.setBounds(460, 45, 129, 33);
		Posts.add(btnNew_1);
		
		
		
		final JButton btnUpdate = new JButton("Update");
		btnUpdate.setIcon(new ImageIcon(MainCustomer.class.getResource("/img/article.png")));
		btnUpdate.setHorizontalAlignment(SwingConstants.LEADING);
		btnUpdate.setBounds(460, 89, 129, 33);
		Posts.add(btnUpdate);
		
		
		final JButton btnDelete_1 = new JButton("Delete");
		btnDelete_1.setIcon(new ImageIcon(MainCustomer.class.getResource("/img/delete.png")));
		btnDelete_1.setHorizontalAlignment(SwingConstants.LEADING);
		btnDelete_1.setBounds(460, 133, 129, 33);
		Posts.add(btnDelete_1);
		
		
			
			final JButton btnView_1 = new JButton("View");
			btnView_1.setIcon(new ImageIcon(MainCustomer.class.getResource("/img/info-icon.png")));
			btnView_1.setHorizontalAlignment(SwingConstants.LEADING);
			btnView_1.setBounds(460, 177, 129, 33);
			Posts.add(btnView_1);
			
			
			final JScrollPane scrollPane_1 = new JScrollPane();
			scrollPane_1.setBounds(24, 11, 412, 276);
			Posts.add(scrollPane_1);
			
			
			table_1 = new JTable();
			int nb=0;
			List<Post> posts = CustomerForumDelegate.FindAllPosts();
			for(Post post:posts){
				if(post.getCustomers().getId()==user.getId()){
					nb++;
				}
			}
			Object[][] data2 = new Object[nb][4];
			int j=0;
			for(Post post:posts){
				if(post.getCustomers().getId()==user.getId()){
					data2[j][0]=post.getId();
					data2[j][1]=post.getTitle();
					data2[j][2]=post.getDate();
					data2[j][3]=post.getValue();
				}
			}
			
			table_1.setModel(new DefaultTableModel(
				data2 ,
				new String[] {
					"Id", "Title", "Date", "Value"
				}
			));
			table_1.removeColumn(table_1.getColumnModel().getColumn(0));
			scrollPane_1.setViewportView(table_1);
		
		final JPanel Service = new JPanel();
		Service.setBounds(1, 1,  601, 378);
		panel.add(Service);
		Service.setLayout(null);
		Service.setOpaque(false);
		Service.setVisible(false);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(24, 11, 406, 257);
		Service.add(scrollPane);
		table = new JTable();
		List<Service> services = CustomerServiceDelegate.FindServiceByCustomer((Customer)user);
		Object[][] data=new Object[services.size()][4];
		int i=0;
		for(Service s:services){
			data[i][0]=s.getId();
			data[i][1]=s.getName();
			data[i][2]=s.getValue();
			data[i][3]=s.getDate();
			i++;
		}        
		
		table.setModel(new DefaultTableModel(data, new String[] { "Id",
				"Service Name", "Value", "Date" }));
		table.removeColumn(table.getColumnModel().getColumn(0));
		
		
		scrollPane.setViewportView(table);
		
		final JButton btnNew = new JButton("New");
		btnNew.setHorizontalAlignment(SwingConstants.LEADING);
		btnNew.setIcon(new ImageIcon(MainCustomer.class.getResource("/img/ajouter.png")));
		btnNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					EventQueue.invokeLater(new Runnable() {
						public void run() {
							try {
								CreateClientService frame = new CreateClientService();
								frame.setVisible(true);
								dispose();
								
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					});
			}
		});
		btnNew.setBounds(440, 28, 129, 33);
		Service.add(btnNew);
		
		
		final JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							if (table.getSelectedRow() < 0) {
								int anser = JOptionPane.showConfirmDialog(
										kFrame,
										"You must select a Service",
										"Notification", JOptionPane.CLOSED_OPTION);
								if (anser == JOptionPane.OK_CANCEL_OPTION) {
									kFrame.dispose();

								}
							} else {
								int id = Integer.parseInt(String.valueOf(table
										.getModel().getValueAt(
												table.getSelectedRow(), 0)));
								Service service = CustomerServiceDelegate.FindById(id);
								CustomerServiceDelegate.DeleteService(service);
								MainCustomer frame = new MainCustomer();
								dispose();
								frame.setVisible(true);
								
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});
		btnDelete.setIcon(new ImageIcon(MainCustomer.class.getResource("/img/delete.png")));
		btnDelete.setHorizontalAlignment(SwingConstants.LEADING);
		btnDelete.setBounds(440, 160, 129, 33);
		Service.add(btnDelete);
		
		
		final JButton btnView = new JButton("View");
		btnView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							if (table.getSelectedRow() < 0) {
								int anser = JOptionPane.showConfirmDialog(
										kFrame,
										"You must select a Service",
										"Notification", JOptionPane.CLOSED_OPTION);
								if (anser == JOptionPane.OK_CANCEL_OPTION) {
									kFrame.dispose();

								}
							} else {
								int id = Integer.parseInt(String.valueOf(table
										.getModel().getValueAt(
												table.getSelectedRow(), 0)));

								ViewClientService frame = new ViewClientService(id);
								frame.setVisible(true);
							
								
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
				
			}
		});
		btnView.setIcon(new ImageIcon(MainCustomer.class.getResource("/img/info-icon.png")));
		btnView.setHorizontalAlignment(SwingConstants.LEADING);
		btnView.setBounds(440, 116, 129, 33);
		Service.add(btnView);
		
		JButton btnUpdate_1 = new JButton("Update");
		btnUpdate_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							if (table.getSelectedRow() < 0) {
								int anser = JOptionPane.showConfirmDialog(
										kFrame,
										"You must select a Service",
										"Notification", JOptionPane.CLOSED_OPTION);
								if (anser == JOptionPane.OK_CANCEL_OPTION) {
									kFrame.dispose();

								}
							} else {
								int id = Integer.parseInt(String.valueOf(table
										.getModel().getValueAt(
												table.getSelectedRow(), 0)));

								ModifyClientService frame = new ModifyClientService(id);
								frame.setVisible(true);
								dispose();
								
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});
		btnUpdate_1.setHorizontalAlignment(SwingConstants.LEADING);
		btnUpdate_1.setIcon(new ImageIcon(MainCustomer.class.getResource("/img/edit.png")));
		btnUpdate_1.setBounds(440, 72, 129, 33);
		Service.add(btnUpdate_1);
		
		
		
		final JPanel welcome = new JPanel();
		welcome.setBounds(0, 0, 602, 379);
		panel.add(welcome);
		welcome.setLayout(null);
		welcome.setOpaque(false);
		
		JLabel lblWelcome = new JLabel("Welcome : "+user.getName());
		lblWelcome.setFont(new Font("Calibri", Font.BOLD, 30));
		lblWelcome.setBounds(82, 25, 360, 69);
		welcome.add(lblWelcome);
		
		JLabel label = new JLabel("");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setIcon(new ImageIcon(MainCustomer.class.getResource("/img/tex.png")));
		label.setBounds(53, 121, 389, 186);
		welcome.add(label);
		
		
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, Color.RED, new Color(0, 102, 102)));
		panel_1.setBounds(10, 30, 168, 380);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		panel_1.setOpaque(false);
		
		JButton btnProfile = new JButton("Profile");
		btnProfile.setIcon(new ImageIcon(MainCustomer.class.getResource("/img/people.png")));

		btnProfile.setBounds(10, 62, 129, 33);
		panel_1.add(btnProfile);
		
		JButton btnService = new JButton("Service");
		btnService.setHorizontalAlignment(SwingConstants.LEADING);
		btnService.setIcon(new ImageIcon(MainCustomer.class.getResource("/img/service.png")));

		btnService.setBounds(10, 115, 129, 33);
		panel_1.add(btnService);
		
		JButton btnPosts = new JButton("Posts");
		btnPosts.setVisible(false);
		btnPosts.setIcon(new ImageIcon(MainCustomer.class.getResource("/img/post.png")));
		btnPosts.setHorizontalAlignment(SwingConstants.LEADING);
		
				btnPosts.setBounds(10, 168, 129, 33);
				panel_1.add(btnPosts);
				
				JButton btnSingOut = new JButton("Sing Out");
				btnSingOut.setIcon(new ImageIcon(MainCustomer.class.getResource("/img/move_participant_to_waiting.png")));
				btnSingOut.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						MainCustomer.this.dispose();
						Authentication authentication = new Authentication();
						authentication.setVisible(true);
					}
				});
				btnSingOut.setBounds(10, 330, 129, 39);
				panel_1.add(btnSingOut);
				
				btnPosts.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						Posts.setVisible(false);
						Profile.setVisible(false);
						Service.setVisible(false);
						welcome.setVisible(false);
					}
				});
		
		
		
		btnProfile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				welcome.setVisible(false);
				Profile.setVisible(true);
				panel_3.setVisible(true);
				Service.setVisible(false);
				Posts.setVisible(false);
				Name.setText(user.getName());
				Email.setText(user.getEmail());
				Adress.setText(user.getAdress());
				Phone.setText(user.getPhone()+"");
				City.setText(user.getCity());
				Token.setText(user.getAccount()+"");
				
				
				try {
					Picture picture = user.getPicture();
					byte[] octets =picture.getPicturee();
					ImageIcon format = new ImageIcon(octets);
					Image images = format.getImage(); // transform it 
			        Image newimg = images.getScaledInstance(129, 120,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way
					Logo.setText("");
					Logo.setIcon(new ImageIcon(newimg));
				} catch (Exception e2) {
					
				}
				
				
			}
		});
		
		btnService.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Profile.setVisible(false);
				
				Service.setVisible(true);
				
				Posts.setVisible(false);
				welcome.setVisible(false);
				
				
			}
		});
		
		JLabel image = new JLabel( new ImageIcon(MainSuperAdmin.class.getResource("/img/background.jpg")));
		image.setBounds(0, 0, 820, 452);
		contentPane.add(image);
		
	}
}
