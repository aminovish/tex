package client.gui.adminisrator.article;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;

import client.delegate.AdminArticleDelegate;
import client.gui.Main.MainAdminstrator;
import client.gui.Main.MainSuperAdmin;

import edu.esprit.tex.domain.Article;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Date;
import java.awt.Color;

public class AdminArticleUpdate extends JFrame {

	private JPanel contentPane;
	private JTextField title;
	private JTextArea desc;
	static int id ;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdminArticleUpdate frame = new AdminArticleUpdate(id);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AdminArticleUpdate(int id) {
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 640, 480);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		panel.setBounds(15, 16, 604, 431);
		panel.setOpaque(false);
		
		title = new JTextField();
		title.setBounds(100, 11, 273, 20);
		title.setColumns(10);
		
		
		
		desc = new JTextArea();
		desc.setBounds(100, 61, 273, 279);
		
		JLabel lblNewLabel = new JLabel("Title");
		lblNewLabel.setBounds(10, 13, 44, 17);
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Calibri", Font.BOLD, 14));
		
		JLabel lblDescription = new JLabel("Description");
		lblDescription.setBounds(10, 61, 68, 17);
		lblDescription.setForeground(Color.WHITE);
		lblDescription.setFont(new Font("Calibri", Font.BOLD, 14));
		panel.setLayout(null);
		panel.add(lblNewLabel);
		panel.add(lblDescription);
		panel.add(title);
		panel.add(desc);
		final Article article = AdminArticleDelegate.FindByIdArticle(id);
		title.setText(article.getTitle());
		desc.setText(article.getDescription());
		
		JButton btnNewButton = new JButton("Update");
		btnNewButton.setBounds(425, 387, 129, 33);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				article.setTitle(title.getText());
				article.setDescription(desc.getText());
				Date date = new Date();
				AdminArticleDelegate.UpdateArticle(article);
				MainAdminstrator frame = new MainAdminstrator();
				frame.setVisible(true);
				dispose();
			}
		});
		btnNewButton.setHorizontalAlignment(SwingConstants.LEADING);
		btnNewButton.setIcon(new ImageIcon(AdminArticleUpdate.class.getResource("/img/ajouter.png")));
		btnNewButton.setFont(new Font("Calibri", Font.BOLD, 14));
		panel.add(btnNewButton);
		contentPane.setLayout(null);
		contentPane.add(panel);
		
		JLabel image = new JLabel( new ImageIcon(MainSuperAdmin.class.getResource("/img/background.jpg")));
		image.setBounds(0, 0, 635, 447);
		contentPane.add(image);
		contentPane.add(image);
	}
}
