package client.gui.adminisrator.article;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import client.delegate.AdminArticleDelegate;
import client.delegate.AdminArticlePictureDelegate;
import client.delegate.PictureUserServiceDelegate;
import client.gui.Main.MainSuperAdmin;
import edu.esprit.tex.domain.Administrator;
import edu.esprit.tex.domain.Article;
import edu.esprit.tex.domain.ArticlePicture;
import edu.esprit.tex.domain.Picture;
import edu.esprit.tex.domain.User;

import java.awt.Color;
import javax.swing.border.BevelBorder;
import javax.swing.SwingConstants;

import session.Session;
import javax.swing.border.EtchedBorder;

public class AdministratorArticle extends JFrame {

	private JPanel contentPane;
	private JTextField tfTitles;
	
	private String fileName=null;	
	private byte[] octets1;
	Double fileSize ;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdministratorArticle frame = new AdministratorArticle();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AdministratorArticle() {
		setTitle("New Article");
		final User user = Session.getInstance().getUser();
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 640, 480);
		contentPane = new JPanel();
		contentPane.setToolTipText("");
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel.setBounds(30, 30, 560, 380);
		contentPane.add(panel);
		panel.setLayout(null);
		panel.setOpaque(false);

		JButton btnNewButton = new JButton("Add Article");
		btnNewButton.setHorizontalAlignment(SwingConstants.LEADING);

		btnNewButton.setIcon(new ImageIcon(AdministratorArticle.class.getResource("/img/ajouter.png")));
		btnNewButton.setBounds(409, 328, 129, 33);
		panel.add(btnNewButton);

		tfTitles = new JTextField();
		tfTitles.setBounds(98, 39, 266, 24);
		panel.add(tfTitles);
		tfTitles.setColumns(10);

		JLabel lblNewLabel = new JLabel("Title");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Calibri", Font.BOLD, 14));
		lblNewLabel.setBounds(10, 39, 115, 24);
		panel.add(lblNewLabel);

		JLabel lblDescripton = new JLabel("Descripton");
		lblDescripton.setForeground(Color.WHITE);
		lblDescripton.setFont(new Font("Calibri", Font.BOLD, 14));
		lblDescripton.setBounds(10, 106, 78, 14);
		panel.add(lblDescripton);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(98, 100, 266, 194);
		panel.add(scrollPane);

		final JTextArea textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
		
		final JLabel req = new JLabel("");
		req.setForeground(Color.RED);
		req.setFont(new Font("Calibri", Font.PLAIN, 11));
		req.setBounds(10, 341, 240, 14);
		panel.add(req);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(409, 22, 129, 120);
		panel.add(panel_1);
		panel_1.setLayout(null);
		panel_1.setVisible(false);
		
		final JLabel Logo = new JLabel("Logo");
		Logo.setHorizontalAlignment(SwingConstants.CENTER);
		Logo.setBounds(0, 0, 129, 120);
		panel_1.add(Logo);
		
		JButton btnBrowser = new JButton("Browser");
		btnBrowser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				chooser.setDialogTitle("Choisir une image");
				chooser.showOpenDialog(null);
				File image=chooser.getSelectedFile();
				fileSize=  (double) image.length();
				System.out.println(""+fileSize);
				fileName=image.getAbsolutePath();
				ImageIcon format=null;
				format = new ImageIcon(fileName);			
				Image images = format.getImage(); // transform it 
		        Image newimg = images.getScaledInstance(129, 120,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way
				Logo.setText("");
				Logo.setIcon(new ImageIcon(newimg));
				
				//remplissage de octets1
				File fileU;
				FileInputStream fis = null;
				int i=0;
				fileU = new File(fileName);
					if (fileU.exists()) {
						i++;
						  try {
							fis = new FileInputStream(fileU);
						} catch (FileNotFoundException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						 octets1 = new byte[(int) fileU.length()];
						 ByteArrayOutputStream bos = new ByteArrayOutputStream();
					        byte[] buf = new byte[1024];
					        try {
					            for (int readNum; (readNum = fis.read(buf)) != -1;) {
					                bos.write(buf, 0, readNum); 
					            }
					        } catch (IOException ex) {
					        }
					         octets1 = bos.toByteArray();
					}
			}
		});
		btnBrowser.setBounds(419, 154, 92, 23);
		panel.add(btnBrowser);
		btnBrowser.setVisible(false);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean just=true;
				if (tfTitles.getText().equals("")){
					just=false;
				}
				if(textArea.getText().equals("")){
					just=false;
				}
				if(just==false){
					req.setText("All fields are required");				
				} 
				else {
				Article article = new Article();
				article.setTitle(tfTitles.getText());
				article.setDescription(textArea.getText());
				Date date = new Date();
				article.setDate(date);
				article.setAdmin((Administrator)user);
				AdminArticleDelegate.CreateArticle(article);
				List<Article> articles = AdminArticleDelegate.FindAllarticle();
				System.out.println("done1");
				for(Article ar : articles){
					if(tfTitles.getText().equals(ar.getTitle())){
						
							ArticlePicture picture = new ArticlePicture(octets1,fileSize);
							picture.setArticle(ar);
							AdminArticlePictureDelegate.createArticlePicture(picture);
							System.out.println("done2");
					
					}
				}
				
				}

			}
		});
		
		JLabel image = new JLabel( new ImageIcon(MainSuperAdmin.class.getResource("/img/background.jpg")));
		image.setBounds(0, 0, 635, 452);
		contentPane.add(image);
	}
}
