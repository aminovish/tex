package client.gui.user.registration;

import java.awt.EventQueue;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.omg.CORBA.REBIND;

import client.delegate.ParameterDelegate;
import client.delegate.PictureUserServiceDelegate;
import client.delegate.UserServiceDelegate;
import client.gui.Main.MainSuperAdmin;
import edu.esprit.tex.domain.Business;
import edu.esprit.tex.domain.Customer;
import edu.esprit.tex.domain.Parameter;
import edu.esprit.tex.domain.Picture;
import edu.esprit.tex.domain.User;
import java.awt.Color;
import java.awt.Font;

public class Registration extends JFrame {
	


	private JPanel contentPane;
	private JTextField tfemail;
	private JPasswordField pfpwd;
	private JPasswordField pfrepwd;
	private JTextField tfphone;
	private JTextField tfname;
	private JTextField tfadress;
	private JTextField tfcity;
	private JTextField tfcode;
	
	private String fileName=null;	
	private byte[] octets1;
	Double fileSize ;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Registration frame = new Registration();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Registration() {

		setResizable(false);
		setTitle("Customer Registration");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 641, 599);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		JLabel image = new JLabel( new ImageIcon(MainSuperAdmin.class.getResource("/img/background.jpg")));
		image.setFont(new Font("Tahoma", Font.PLAIN, 12));
		image.setBounds(0, 0, 635, 581);
		

		JLabel lblNewLabel = new JLabel("Email :");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setBounds(26, 19, 121, 14);

		tfemail = new JTextField();
		tfemail.setFont(new Font("Tahoma", Font.BOLD, 11));
		tfemail.setBounds(172, 19, 166, 20);
		tfemail.setColumns(10);

		JLabel lblNewLabel_1 = new JLabel("Password :");
		lblNewLabel_1.setForeground(Color.WHITE);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_1.setBounds(26, 57, 121, 14);

		JLabel lblRetypePassword = new JLabel("Reenter password :");
		lblRetypePassword.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblRetypePassword.setForeground(Color.WHITE);
		lblRetypePassword.setBounds(26, 95, 121, 14);

		pfpwd = new JPasswordField();
		pfpwd.setFont(new Font("Tahoma", Font.BOLD, 11));
		pfpwd.setBounds(172, 57, 166, 20);

		pfrepwd = new JPasswordField();
		pfrepwd.setFont(new Font("Tahoma", Font.BOLD, 11));
		pfrepwd.setBounds(172, 95, 166, 20);

		tfphone = new JTextField();
		tfphone.setFont(new Font("Tahoma", Font.BOLD, 11));
		tfphone.setBounds(172, 171, 166, 20);
		tfphone.setColumns(10);

		JLabel lblNewLabel_2 = new JLabel("Phone :");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_2.setForeground(Color.WHITE);
		lblNewLabel_2.setBounds(26, 171, 121, 14);

		JPanel panel = new JPanel();
		panel.setBounds(455, 11, 129, 120);

		

		JLabel lblName = new JLabel("Name :");
		lblName.setForeground(Color.WHITE);
		lblName.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblName.setBounds(26, 133, 121, 14);

		tfname = new JTextField();
		tfname.setFont(new Font("Tahoma", Font.BOLD, 11));
		tfname.setBounds(172, 133, 166, 20);
		tfname.setColumns(10);

		JLabel lblAdress = new JLabel("Address :");
		lblAdress.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblAdress.setForeground(Color.WHITE);
		lblAdress.setBounds(26, 199, 121, 14);

		tfadress = new JTextField();
		tfadress.setFont(new Font("Tahoma", Font.BOLD, 11));
		tfadress.setBounds(172, 199, 308, 20);
		tfadress.setColumns(10);
		final JLabel req = new JLabel("All Fields required");
		req.setFont(new Font("Calibri", Font.BOLD, 13));
		req.setForeground(Color.RED);
		req.setBounds(26, 527, 299, 19);
		contentPane.add(req);

		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnExit.setBounds(493, 527, 103, 23);

		

		JLabel lblCity = new JLabel("City :");
		lblCity.setForeground(Color.WHITE);
		lblCity.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCity.setBounds(24, 230, 123, 14);

		tfcity = new JTextField();
		tfcity.setFont(new Font("Tahoma", Font.BOLD, 11));
		tfcity.setBounds(172, 227, 166, 20);
		tfcity.setColumns(10);
		contentPane.setLayout(null);
		contentPane.add(lblNewLabel);
		contentPane.add(lblNewLabel_1);
		contentPane.add(lblRetypePassword);
		contentPane.add(lblName);
		contentPane.add(lblNewLabel_2);
		contentPane.add(lblAdress);
		contentPane.add(tfadress);
		contentPane.add(tfemail);
		contentPane.add(pfpwd);
		contentPane.add(pfrepwd);
		contentPane.add(tfname);
		contentPane.add(tfphone);
		contentPane.add(panel);
		panel.setLayout(null);

		final JLabel Logo = new JLabel("Your Logo"); /// baddelha image mouch logo x)
		
		Logo.setHorizontalAlignment(SwingConstants.CENTER);
		Logo.setBounds(0, 0, 129, 120);
		panel.add(Logo);
		JButton btnYourPhoto = new JButton("Browse");
		btnYourPhoto.setBounds(465, 142, 104, 23);
		btnYourPhoto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser chooser = new JFileChooser();
				chooser.setDialogTitle("Choisir une image");
				chooser.showOpenDialog(null);
				File image=chooser.getSelectedFile();
				fileSize=  (double) image.length();
				System.out.println(""+fileSize);
				fileName=image.getAbsolutePath();
				ImageIcon format=null;
				format = new ImageIcon(fileName);			
				Image images = format.getImage(); // transform it 
		        Image newimg = images.getScaledInstance(129, 120,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way
				Logo.setText("");
				Logo.setIcon(new ImageIcon(newimg));
				
				//remplissage de octets1
				File fileU;
				FileInputStream fis = null;
				int i=0;
				fileU = new File(fileName);
					if (fileU.exists()) {
						i++;
						  try {
							fis = new FileInputStream(fileU);
						} catch (FileNotFoundException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						 octets1 = new byte[(int) fileU.length()];
						 ByteArrayOutputStream bos = new ByteArrayOutputStream();
					        byte[] buf = new byte[1024];
					        try {
					            for (int readNum; (readNum = fis.read(buf)) != -1;) {
					                bos.write(buf, 0, readNum); 
					            }
					        } catch (IOException ex) {
					        }
					         octets1 = bos.toByteArray();
					}
					
					
					
				
				
				
			}
		});
		contentPane.add(btnYourPhoto);
		
		contentPane.add(btnExit);
		contentPane.add(lblCity);
		contentPane.add(tfcity);
		

		final JLabel lblTaxCode = new JLabel("Tax code :");
		lblTaxCode.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTaxCode.setForeground(Color.WHITE);
		lblTaxCode.setBounds(26, 312, 121, 14);
		lblTaxCode.setVisible(false);
		contentPane.add(lblTaxCode);

		tfcode = new JTextField();
		tfcode.setColumns(10);
		tfcode.setBounds(172, 309, 166, 20);
		tfcode.setVisible(false);
		contentPane.add(tfcode);

		final JLabel lblD = new JLabel("Description :");
		lblD.setForeground(Color.WHITE);
		lblD.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblD.setBounds(26, 340, 121, 14);
		lblD.setVisible(false);
		contentPane.add(lblD);

		final JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(172, 340, 386, 167);
		scrollPane.setVisible(false);
		contentPane.add(scrollPane);

		final JTextArea textdesc = new JTextArea();
		scrollPane.setViewportView(textdesc);
		textdesc.setVisible(false);
		textdesc.setTabSize(1);

		final JRadioButton rbtncustomer = new JRadioButton("Customer");
		rbtncustomer.setForeground(Color.WHITE);
		rbtncustomer.setSelected(true);
		rbtncustomer.setOpaque(false);
		rbtncustomer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTaxCode.setVisible(false);
				tfcode.setVisible(false);
				lblD.setVisible(false);
				scrollPane.setVisible(false);
				textdesc.setVisible(false);
			}
		});
		rbtncustomer.setBounds(172, 254, 109, 23);
		contentPane.add(rbtncustomer);

		final JRadioButton rbtnbusiness = new JRadioButton("Business");
		rbtnbusiness.setForeground(Color.WHITE);
		rbtnbusiness.setOpaque(false);
		rbtnbusiness.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTaxCode.setVisible(true);
				tfcode.setVisible(true);
				lblD.setVisible(true);
				scrollPane.setVisible(true);
				textdesc.setVisible(true);
				if (rbtnbusiness.isSelected()) {

				}
			}
		});
		rbtnbusiness.setBounds(302, 254, 109, 23);
		contentPane.add(rbtnbusiness);
		ButtonGroup group = new ButtonGroup();
		group.add(rbtnbusiness);
		group.add(rbtncustomer);
		final JLabel reqemail = new JLabel("");
		reqemail.setFont(new Font("Calibri", Font.PLAIN, 9));
		reqemail.setBounds(172, 38, 166, 14);
		contentPane.add(reqemail);
		
		JButton btnRegistre = new JButton("Sign up");
		btnRegistre.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				boolean just = true;
				
				if((!tfemail.getText().contains("@") && !tfemail.getText().contains(".")) ||(tfemail.getText().equals("")) ){
					reqemail.setText("Email not Valid");	
					just=false;
				}
				else {
					List<User> usList;
					usList =UserServiceDelegate.findUsers();
					for(User user:usList){
						if(tfemail.getText().equals(user.getEmail())){
							reqemail.setText("This email already exists");	
							just=false;
						}
					}
				}


				
				if (pfpwd.getText().equals("")) {
					
					just = false;
				} 
				if (pfrepwd.getText().equals("")) {
					
					just = false;
				} 
				if (!pfpwd.getText().equals(pfrepwd.getText())) {
					
					just = false;

				} 
				if (tfname.getText().equals("")) {
					
					just = false;
				} 
				if (tfphone.getText().equals("")) {
					
					just = false;
				} 
				if (tfadress.getText().equals("")) {
					
					just = false;
				} 
				if (tfcity.getText().equals("")) {
					
					just = false;
				} 
				try {
					int x = Integer.parseInt(tfphone.getText());
				} catch (Exception e) {
					
					just=false;
				}
				if(rbtnbusiness.isSelected()){
					if(tfcode.getText().equals("")){
						just= false;
					}
					if(textdesc.getText().equals("")){
						just= false;
					}
				}
				
				
				
				if (just==false) {
					req.setText(" Please verify all fields are required and valid");				
					
					
					
				} else {
					
					if(rbtnbusiness.isSelected()){
								
								Business business = new Business();
								business.setName(tfname.getText());
								business.setEmail(tfemail.getText());
								business.setPassword(pfpwd.getText());
								business.setPhone(tfphone.getText());
								business.setAdress(tfadress.getText());
								business.setCity(tfcity.getText());
								business.setTaxCode(tfcode.getText());
								business.setDescription(textdesc.getText());
								UserServiceDelegate.createBusiness(business, null);
								List<Business> businesses = UserServiceDelegate.findBusinesss();
								for (Business b : businesses){
									if(tfemail.getText().equals(b.getEmail())){
										Picture picture = new Picture(octets1,fileSize);
										picture.setUser(b);
										PictureUserServiceDelegate.createPictureUser(picture);										
									}
								}
								dispose();
							
						}
					else {
						Parameter parameter= ParameterDelegate.FindParameter();
						Customer customer = new Customer();
						customer.setName(tfname.getText());
						customer.setEmail(tfemail.getText());
						customer.setPassword(pfpwd.getText());
						customer.setPhone(tfphone.getText());
						customer.setAdress(tfadress.getText());
						customer.setCity(tfcity.getText());
						customer.setState(true);
						customer.setAccount(parameter.getTokenNumber());
						UserServiceDelegate.createCustomer(customer);
						List<Customer> customers = UserServiceDelegate.findCustomers();
						for (Customer c : customers){
							if(tfemail.getText().equals(c.getEmail())){
								Picture picture = new Picture(octets1,fileSize);
								picture.setUser(c);
								PictureUserServiceDelegate.createPictureUser(picture);
							}
						}
						
						dispose();
						
						}
					}
					
				}
				
			
		});
		btnRegistre.setBounds(356, 527, 109, 23);
		contentPane.add(btnRegistre);

		contentPane.add(image);
		
		
		
		

	}
}
