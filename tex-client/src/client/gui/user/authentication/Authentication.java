package client.gui.user.authentication;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import session.Session;
import client.delegate.UserServiceDelegate;
import client.gui.Main.MainAdminstrator;
import client.gui.Main.MainBusiness;
import client.gui.Main.MainCustomer;
import client.gui.Main.MainSuperAdmin;
import client.gui.customer.services.BusinessContract;
import client.gui.superadmin.registration.RegistrationSuperAdmin;
import client.gui.user.registration.Registration;
import edu.esprit.tex.domain.Administrator;
import edu.esprit.tex.domain.Business;
import edu.esprit.tex.domain.Customer;
import edu.esprit.tex.domain.User;
import javax.swing.JTextPane;
import javax.swing.JEditorPane;

public class Authentication extends JFrame {

	private JPanel contentPane;
	private JTextField tflogin;
	private JPasswordField pfpwd;
	private JButton btnLogIn;
	private JButton btnExit;
	private JLabel jbmessage;
	private JPanel panel;
	private JEditorPane wlcom;
	private JLabel lblTunisien;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					List<User> usList;
					usList = UserServiceDelegate.findUsers();
					if (usList.size() == 0) {
						RegistrationSuperAdmin registrationSuperAdmin = new RegistrationSuperAdmin();
						registrationSuperAdmin.setVisible(true);
					} else {
						Authentication frame = new Authentication();
						frame.setVisible(true);
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Authentication() {
		setResizable(false);
		setTitle("Authentication");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 579, 301);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(135, 206, 250));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		panel = new JPanel();
		panel.setBounds(32, 22, 116, 107);
		
		contentPane.add(panel);
		JLabel image1 = new JLabel( new ImageIcon(Authentication.class.getResource("/img/MotDePasseGeneral.png")));
		panel.add(image1);
		panel.setOpaque(false);



		JLabel lblEmail = new JLabel("Email :");
		lblEmail.setForeground(Color.WHITE);
		lblEmail.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
		lblEmail.setBounds(256, 66, 94, 21);
		contentPane.add(lblEmail);

		JLabel lblPassword = new JLabel("Password :");
		lblPassword.setForeground(Color.WHITE);
		lblPassword.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
		lblPassword.setBounds(256, 99, 94, 21);
		contentPane.add(lblPassword);

		tflogin = new JTextField();
		tflogin.setBounds(360, 66, 193, 20);
		contentPane.add(tflogin);
		tflogin.setColumns(10);

		pfpwd = new JPasswordField();
		pfpwd.setBounds(360, 100, 193, 20);
		contentPane.add(pfpwd);

		btnLogIn = new JButton("Log In");
		btnLogIn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String email = tflogin.getText();
				String password = pfpwd.getText();
				User user = UserServiceDelegate.authenticate(email, password);

				if (user != null) {
					Session.getInstance().setUser(user);

					if (user instanceof Administrator) {
						if (!user.getState()) {
							jbmessage.setText("Your Account is Not Valide Yet");
						} else {
							EventQueue.invokeLater(new Runnable() {
								public void run() {
									try {

										MainAdminstrator frame = new MainAdminstrator();
										dispose();
										frame.setVisible(true);
									} catch (Exception e) {
										e.printStackTrace();
									}
								}
							});
						}

					} else if (user instanceof Business) {
						EventQueue.invokeLater(new Runnable() {
							public void run() {
								try {

									MainBusiness frame = new MainBusiness();
									dispose();
									frame.setVisible(true);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						});
					} else if (user instanceof Customer) {
						EventQueue.invokeLater(new Runnable() {
							public void run() {
								try {

									MainCustomer frame = new MainCustomer();
									dispose();
									frame.setVisible(true);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						});

					} else if (user instanceof User) {
						EventQueue.invokeLater(new Runnable() {
							public void run() {
								try {

									MainSuperAdmin frame = new MainSuperAdmin(0);
									dispose();
									frame.setVisible(true);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						});

					}

				} else {
					jbmessage.setText("Please verify your email and password");

				}
			}
		});
		btnLogIn.setBounds(365, 228, 89, 23);
		contentPane.add(btnLogIn);

		btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnExit.setBounds(464, 228, 89, 23);
		contentPane.add(btnExit);

		jbmessage = new JLabel("");
		jbmessage.setForeground(Color.RED);
		jbmessage.setBounds(313, 144, 184, 14);
		contentPane.add(jbmessage);


		JButton btnCustomerRegistration = new JButton("Registration");
		btnCustomerRegistration.setForeground(new Color(0, 0, 0));
	

		btnCustomerRegistration.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				BusinessContract frame1 = new BusinessContract();
				frame1.setVisible(true);

			}
		});
		btnCustomerRegistration.setBounds(21, 228, 184, 23);
		contentPane.add(btnCustomerRegistration);
		
		lblTunisien = new JLabel("Tunisian");
		lblTunisien.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 14));
		lblTunisien.setForeground(Color.RED);
		lblTunisien.setBounds(95, 172, 74, 23);
		contentPane.add(lblTunisien);
		
		wlcom = new JEditorPane();
		wlcom.setForeground(Color.WHITE);
		wlcom.setFont(new Font("Tahoma", Font.BOLD, 13));
		wlcom.setText("Welcom to                  Exchange Service");
		wlcom.setBounds(21, 171, 276, 39);
		contentPane.add(wlcom);
		wlcom.setOpaque(false);
				JLabel image = new JLabel( new ImageIcon(Authentication.class.getResource("/img/background.jpg")));
				image.setBounds(0, 0, 573, 273);
				contentPane.add(image);
		

	}
}
