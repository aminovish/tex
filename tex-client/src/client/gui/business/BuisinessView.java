package client.gui.business;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import session.Session;

import client.delegate.ParameterDelegate;
import client.delegate.PictureUserServiceDelegate;
import client.delegate.UserServiceDelegate;
import client.gui.Main.MainSuperAdmin;
import edu.esprit.tex.domain.Business;
import edu.esprit.tex.domain.Customer;
import edu.esprit.tex.domain.Parameter;
import edu.esprit.tex.domain.Picture;
import edu.esprit.tex.domain.User;

public class BuisinessView extends JFrame {

	private JPanel contentPane;
	private JTextField tfemail;
	private JTextField tfphone;
	private JTextField tfname;
	private JTextField tfadress;
	private JTextField tfcity;
	private JTextField tfcode;
	
	private String fileName=null;	
	private byte[] octets1;
	Double fileSize ;
	
	static int id;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BuisinessView frame = new BuisinessView(id);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BuisinessView(int id) {
		setResizable(false);
		setTitle("Customer Registration");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 641, 599);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		JLabel image = new JLabel( new ImageIcon(MainSuperAdmin.class.getResource("/img/background.jpg")));
		image.setFont(new Font("Tahoma", Font.PLAIN, 12));
		image.setBounds(0, 0, 635, 581);
		

		JLabel lblNewLabel = new JLabel("Email :");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setBounds(26, 19, 121, 14);

		tfemail = new JTextField();
		tfemail.setFont(new Font("Tahoma", Font.BOLD, 11));
		tfemail.setBounds(172, 19, 166, 20);
		tfemail.setColumns(10);

		tfphone = new JTextField();
		tfphone.setFont(new Font("Tahoma", Font.BOLD, 11));
		tfphone.setBounds(172, 171, 166, 20);
		tfphone.setColumns(10);

		JLabel lblNewLabel_2 = new JLabel("Phone :");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_2.setForeground(Color.WHITE);
		lblNewLabel_2.setBounds(26, 171, 121, 14);

		JPanel panel = new JPanel();
		panel.setBounds(455, 11, 129, 120);

		

		JLabel lblName = new JLabel("Name :");
		lblName.setForeground(Color.WHITE);
		lblName.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblName.setBounds(26, 133, 121, 14);

		tfname = new JTextField();
		tfname.setFont(new Font("Tahoma", Font.BOLD, 11));
		tfname.setBounds(172, 133, 166, 20);
		tfname.setColumns(10);

		JLabel lblAdress = new JLabel("Address :");
		lblAdress.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblAdress.setForeground(Color.WHITE);
		lblAdress.setBounds(26, 199, 121, 14);

		tfadress = new JTextField();
		tfadress.setFont(new Font("Tahoma", Font.BOLD, 11));
		tfadress.setBounds(172, 199, 308, 20);
		tfadress.setColumns(10);

		JButton btnExit = new JButton("Exit");
		btnExit.setHorizontalAlignment(SwingConstants.LEADING);
		btnExit.setIcon(new ImageIcon(BuisinessView.class.getResource("/img/delete.png")));
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnExit.setBounds(429, 527, 129, 33);

		

		JLabel lblCity = new JLabel("City :");
		lblCity.setForeground(Color.WHITE);
		lblCity.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCity.setBounds(24, 230, 123, 14);

		tfcity = new JTextField();
		tfcity.setFont(new Font("Tahoma", Font.BOLD, 11));
		tfcity.setBounds(172, 227, 166, 20);
		tfcity.setColumns(10);
		contentPane.setLayout(null);
		contentPane.add(lblNewLabel);
		contentPane.add(lblName);
		contentPane.add(lblNewLabel_2);
		contentPane.add(lblAdress);
		contentPane.add(tfadress);
		contentPane.add(tfemail);
		contentPane.add(tfname);
		contentPane.add(tfphone);
		contentPane.add(panel);
		panel.setLayout(null);

		final JLabel Logo = new JLabel("Your Logo"); /// baddelha image mouch logo x)
		
		Logo.setHorizontalAlignment(SwingConstants.CENTER);
		Logo.setBounds(0, 0, 129, 120);
		panel.add(Logo);
		
		contentPane.add(btnExit);
		contentPane.add(lblCity);
		contentPane.add(tfcity);
		

		final JLabel lblTaxCode = new JLabel("Tax code :");
		lblTaxCode.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTaxCode.setForeground(Color.WHITE);
		lblTaxCode.setBounds(26, 312, 121, 14);
		lblTaxCode.setVisible(false);
		contentPane.add(lblTaxCode);

		tfcode = new JTextField();
		tfcode.setColumns(10);
		tfcode.setBounds(172, 309, 166, 20);
		tfcode.setVisible(true);
		contentPane.add(tfcode);

		final JLabel lblD = new JLabel("Description :");
		lblD.setForeground(Color.WHITE);
		lblD.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblD.setBounds(26, 340, 121, 14);
		lblD.setVisible(false);
		contentPane.add(lblD);

		final JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(172, 340, 386, 167);
		scrollPane.setVisible(false);
		contentPane.add(scrollPane);

		final JTextArea textdesc = new JTextArea();
		scrollPane.setViewportView(textdesc);
		textdesc.setVisible(true);
		textdesc.setTabSize(1);
		ButtonGroup group = new ButtonGroup();
		final JLabel reqemail = new JLabel("");
		reqemail.setFont(new Font("Calibri", Font.PLAIN, 9));
		reqemail.setBounds(172, 38, 166, 14);
		contentPane.add(reqemail);

		contentPane.add(image);
		Business business = UserServiceDelegate.findBusinessById(id);
		tfemail.setText(business.getEmail());
		tfname.setText(business.getName());
		tfphone.setText(business.getPhone());
		tfadress.setText(business.getAdress());
		tfcity.setText(business.getCity());
		tfcode.setText(business.getTaxCode());
		textdesc.setText(business.getDescription());
		try {
			Picture picture = new Picture();
			picture = PictureUserServiceDelegate
					.getPictureUserById(business.getId());
			ImageIcon format = new ImageIcon(picture.getPicturee());
			Image images = format.getImage(); // transform it 
	        Image newimg = images.getScaledInstance(129, 120,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way
			Logo.setText("");
			Logo.setIcon(new ImageIcon(newimg));
		} catch (Exception e2) {

		}
		
		
		
		
		

	
	}

}
