package client.gui.business.service;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import client.delegate.BusinessServiceDelegate;
import client.delegate.UserServiceDelegate;
import client.gui.Main.MainBusiness;
import client.gui.Main.MainSuperAdmin;
import edu.esprit.tex.domain.Administrator;
import edu.esprit.tex.domain.Business;
import edu.esprit.tex.domain.Service;
import edu.esprit.tex.domain.User;
import javax.swing.JEditorPane;

import session.Session;
import javax.swing.JScrollPane;
import javax.swing.ImageIcon;
import javax.swing.JTextArea;
import java.awt.Color;

public class BusinessCreateUpdateService extends JFrame {

	private JPanel contentPane;
	private JTextField tfName;
	private JTextField tfValue;
	static int state;
	Business business = (Business) Session.getInstance().getUser();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BusinessCreateUpdateService frame = new BusinessCreateUpdateService(-1);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BusinessCreateUpdateService(final int state) {
		setTitle("Service");	
		
		
		
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 640, 480);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel.setBounds(30, 30, 560, 380);
		contentPane.add(panel);
		panel.setLayout(null);
		panel.setOpaque(false);
		
		JLabel lblName = new JLabel("Name :");
		lblName.setForeground(Color.WHITE);
		lblName.setFont(new Font("Calibri", Font.BOLD, 14));
		lblName.setBounds(41, 56, 67, 26);
		panel.add(lblName);
		
		tfName = new JTextField();
		tfName.setToolTipText("");
		tfName.setBounds(131, 59, 298, 20);
		panel.add(tfName);
		tfName.setColumns(10);
		
		JLabel lblValue = new JLabel("Value :");
		lblValue.setForeground(Color.WHITE);
		lblValue.setFont(new Font("Calibri", Font.BOLD, 14));
		lblValue.setBounds(41, 90, 67, 26);
		panel.add(lblValue);
		
		JLabel lblDescription = new JLabel("Description :");
		lblDescription.setForeground(Color.WHITE);
		lblDescription.setFont(new Font("Calibri", Font.BOLD, 14));
		lblDescription.setBounds(41, 127, 97, 26);
		panel.add(lblDescription);
		
		tfValue = new JTextField();
		tfValue.setToolTipText("");
		tfValue.setColumns(10);
		tfValue.setBounds(131, 93, 298, 20);
		panel.add(tfValue);
		
		JButton btnCreate = new JButton("Create");
		btnCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnCreate.setIcon(new ImageIcon(BusinessCreateUpdateService.class.getResource("/img/ajouter.png")));

		btnCreate.setBounds(410, 336, 129, 33);
		panel.add(btnCreate);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(131, 127, 298, 179);
		panel.add(scrollPane);
		
		final JTextArea epDescription = new JTextArea();
		scrollPane.setViewportView(epDescription);
		if(state >-1){
			Service service = BusinessServiceDelegate.findById(state);
			
			
			
		
			tfName.setText(service.getName());
			tfValue.setText(service.getValue()+"");
			epDescription.setText(service.getDescription()+"");
			btnCreate.setText("Update");
			btnCreate.setIcon(new ImageIcon(BusinessCreateUpdateService.class.getResource("/img/validate.png")));
			

			btnCreate.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					Service service =new Service(tfName.getText(),epDescription.getText(),Integer.parseInt(tfValue.getText()) );
					Date date = new Date();
					service.setDate(date);					
					service.setBusiness(business);
					BusinessServiceDelegate.updateService(service);
					MainBusiness frame = new MainBusiness();
					frame.setVisible(true);
					BusinessCreateUpdateService.this.dispose();
					
				}
			});
			
		}else{
		
			btnCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Service service =new Service(tfName.getText(),epDescription.getText(),Integer.parseInt(tfValue.getText()) );
				Date date = new Date();
				service.setDate(date);
				service.setBusiness(business);
				BusinessServiceDelegate.createService(service);
				MainBusiness frame = new MainBusiness();
				frame.setVisible(true);
				BusinessCreateUpdateService.this.dispose();
				
			}
		});}
		
		JLabel image = new JLabel( new ImageIcon(MainSuperAdmin.class.getResource("/img/background.jpg")));
		image.setBounds(0, 0, 635, 452);
		contentPane.add(image);
	}
}
