package client.gui.business.service;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.ImageIcon;

import client.delegate.BusinessServiceDelegate;
import client.delegate.UserServiceDelegate;
import client.gui.Main.MainSuperAdmin;

import edu.esprit.tex.domain.Business;
import edu.esprit.tex.domain.Service;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class ServiceView extends JFrame {

	private JPanel contentPane;
	private JTextField tfName;
	private JTextField tfValue;
	static int id ;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ServiceView frame = new ServiceView(id);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ServiceView(int id) {
		setTitle("Service Detail");
		setResizable(false);
		this.id=id;
		Service service = BusinessServiceDelegate.findById(id);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 640, 480);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		
		
		JPanel panel = new JPanel();
		panel.setBounds(30, 23, 579, 397);
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel.setOpaque(false);
		contentPane.add(panel);
		panel.setLayout(null);

		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(131, 127, 298, 179);
		panel.add(scrollPane);
		
		JTextArea textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
		textArea.setText(service.getDescription());
		textArea.setEditable(false);
		
		JLabel label = new JLabel("Name :");
		label.setForeground(Color.WHITE);
		label.setBounds(41, 56, 67, 26);
		label.setFont(new Font("Calibri", Font.BOLD, 14));
		panel.add(label);
		
		tfName = new JTextField();
		tfName.setBounds(131, 59, 298, 20);
		tfName.setEditable(false);
		tfName.setToolTipText("");
		tfName.setColumns(10);
		panel.add(tfName);
		
		JLabel label_1 = new JLabel("Value :");
		label_1.setForeground(Color.WHITE);
		label_1.setBounds(41, 90, 67, 26);
		label_1.setFont(new Font("Calibri", Font.BOLD, 14));
		panel.add(label_1);
		
		JLabel label_2 = new JLabel("Description :");
		label_2.setForeground(Color.WHITE);
		label_2.setBounds(41, 127, 97, 26);
		label_2.setFont(new Font("Calibri", Font.BOLD, 14));
		panel.add(label_2);
		
		tfValue = new JTextField();
		tfValue.setBounds(131, 93, 298, 20);
		tfValue.setEditable(false);
		tfValue.setToolTipText("");
		tfValue.setColumns(10);
		panel.add(tfValue);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.setBounds(410, 336, 129, 33);

		btnCancel.setIcon(new ImageIcon(ServiceView.class.getResource("/img/delete.png")));
		panel.add(btnCancel);
		tfName.setText(service.getName());
		tfValue.setText(service.getValue()+"");
		
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
		JLabel image = new JLabel( new ImageIcon(MainSuperAdmin.class.getResource("/img/background.jpg")));
		image.setBounds(0, 0, 635, 452);
		contentPane.add(image);

		

	
	}

}
