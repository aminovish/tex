package client.delegate;



import java.util.List;

import client.locator.ServiceLocator;
import edu.esprit.tex.domain.Administrator;
import edu.esprit.tex.domain.Business;
import edu.esprit.tex.domain.Customer;
import edu.esprit.tex.domain.User;
import edu.esprit.tex.services.adminstration.user.UsersRemote;



public class UserServiceDelegate {
	private static final String jndiName ="ejb:tex/tex-ejb/Users!edu.esprit.tex.services.adminstration.user.UsersRemote";
	
	
	public static UsersRemote getProxy (){
		return (UsersRemote) ServiceLocator.getInstance().getProxy(jndiName);
		
	}

	
	public static void create(User user) {
	
		getProxy().create(user);
	}
	public static void update(User user) {
		getProxy().update(user);
	}


	public static void delete(User user) {
		getProxy().delete(user);		
	}
	public static List<User> findUsers() {
		
		return getProxy().findUsers(); 
	}


	
	public static User findUserById(int id) {
		
		return getProxy().findUserById(id);
	}

	public static User authenticate(String email, String password) {
	
		return getProxy().authenticate(email, password);
	}

	public static void createAdministrator(Administrator administrator) {
		getProxy().createAdministrator(administrator);
		
	}


	
	public static void updateAdministrator(Administrator administrator) {
		getProxy().updateAdministrator(administrator);
		
	}


	
	public static void deleteAdministrator(Administrator administrator) {
		getProxy().deleteAdministrator(administrator);
		
	}


	
	public static List<Administrator> findAdministrators() {

		return getProxy().findAdministrators();
	}


	
	public static Administrator findAdministratorById(int id) {
		return 	getProxy().findAdministratorById(id);
	}


	public static void createBusiness(Business business, Administrator administrator) {
		getProxy().createBusiness(business, administrator);
		
	}
	



	
	public static void updateBusiness(Business business) {
		getProxy().updateBusiness(business);
		
	}


	
	public static void deleteBusiness(Business business) {
		getProxy().deleteBusiness(business);
		
	}


	
	public static List<Business> findBusinesss() {

		return getProxy().findBusinesss();
	}


	
	public static Business findBusinessById(int id) {
		return 	getProxy().findBusinessById(id);
	}



	public static void createCustomer(Customer customer) {
		getProxy().createCustomer(customer);
		
	}


	
	public static void updateCustomer(Customer customer) {
		getProxy().updateCustomer(customer);
		
	}


	
	public static void deleteCustomer(Customer customer) {
		getProxy().deleteCustomer(customer);
		
	}


	
	public static List<Customer> findCustomers() {
		return 	getProxy().findCustomers(); 
		
	}


	
	public static Customer findCustomerById(int id) {
		return 	getProxy().findCustomerById(id);
	}


}
