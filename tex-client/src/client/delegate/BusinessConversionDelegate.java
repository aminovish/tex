package client.delegate;

import client.locator.ServiceLocator;
import edu.esprit.tex.domain.Conversion;
import edu.esprit.tex.services.business.conversion.BusinessConversionRemote;

public class BusinessConversionDelegate {

	private static final String jndiName ="ejb:tex/tex-ejb/BusinessConversion!edu.esprit.tex.services.business.conversion.BusinessConversionRemote";
	
	public static BusinessConversionRemote getProxy (){
		return  (BusinessConversionRemote) ServiceLocator.getInstance().getProxy(jndiName);
		
	}
	void createConversion(Conversion conversion){
		getProxy().createConversion(conversion);
	}
	public BusinessConversionDelegate() {
		
	}

}