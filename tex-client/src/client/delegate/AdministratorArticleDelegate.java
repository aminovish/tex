package client.delegate;

import java.util.List;
import client.locator.ServiceLocator;
import edu.esprit.tex.domain.Article;
import edu.esprit.tex.services.adminstration.administrator.article.AdminstratorArticleRemote;




public class AdministratorArticleDelegate {

	private static final String jndiName ="ejb:tex/tex-ejb/AdminstratorArticle!edu.esprit.tex.services.adminstration.administrator.article.AdminstratorArticleRemote";
	
	public static AdminstratorArticleRemote getProxy (){
		return  (AdminstratorArticleRemote) ServiceLocator.getInstance().getProxy(jndiName);
		
	}

	public static void CreateArticle(Article art) {
		getProxy().CreateArticle(art);
		
	}

	
	public static void UpdateArticle(Article art) {
		getProxy().UpdateArticle(art);
		
	}

	
	public static void DeleteArticle(Article art) {
		getProxy().DeleteArticle(art);
		
	}

	
	public static Article FindByIdArticle(int id) {
		
		return getProxy().FindByIdArticle(id);
	}

	
	public static List<Article> FindAllarticle() {
		
		return getProxy().FindAllarticle();
	}


}
