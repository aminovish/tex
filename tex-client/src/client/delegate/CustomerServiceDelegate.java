package client.delegate;

import java.util.List;

import client.locator.ServiceLocator;
import edu.esprit.tex.domain.Customer;
import edu.esprit.tex.domain.Service;
import edu.esprit.tex.services.customer.service.CustomerServiceRemote;

public class CustomerServiceDelegate {
	private static final String jndiName = "ejb:tex/tex-ejb/CustomerService!edu.esprit.tex.services.customer.service.CustomerServiceRemote";

	public static CustomerServiceRemote getProxy() {
		return (CustomerServiceRemote) ServiceLocator.getInstance().getProxy(
				jndiName);

	}

	public static void CreateService(Service service) {

		getProxy().CreateService(service);

	}

	public static void UpdateService(Service service) {

		getProxy().UpdateService(service);
	}

	public static void DeleteService(Service service) {
		getProxy().DeleteService(service);
	}

	public static List<Service> FindAllServices() {

		return getProxy().FindAllServices();

	}

	public static List<Service> FindByValueAndName(int value, String name) {

		return getProxy().FindByNameValue(name, value);
	}

	public static Service FindById(int id) {

		return getProxy().FindById(id);
	}
	public static List<Service> FindServiceByCustomer(Customer customer) {

		return getProxy().FindServiceByCustomer(customer);
	}

}
