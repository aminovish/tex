package client.delegate;

import java.util.Date;
import java.util.List;

import client.locator.ServiceLocator;
import edu.esprit.tex.domain.Post;
import edu.esprit.tex.services.customer.forum.CustomerForumRemote;

public class CustomerForumDelegate {
	private static final String jndiName = "ejb:tex/tex-ejb/CustomerForum!edu.esprit.tex.services.customer.forum.CustomerForumRemote";

	public static CustomerForumRemote getProxy() {
		return (CustomerForumRemote) ServiceLocator.getInstance().getProxy(jndiName);

	}

	public static void CreatePost(Post post) {

		getProxy().CreatePost(post);

	}

	public static void UpdatePost(Post post) {

		getProxy().UpdatePost(post);
	}

	public static void  DeletePost(Post post) {
	}
	public static Post FindById(int id) {

		return getProxy().FindById(id);
	}
public static List<Post> FindAllPosts() {

		
		return getProxy().FindAllPosts();
	}

	public static List<Post> FindByDate(Date date) {

		
		return getProxy().FindByDate(date);
	}
}

	