package client.delegate;

import client.locator.ServiceLocator;
import edu.esprit.tex.domain.PostComment;
import edu.esprit.tex.services.customer.comment.CustomerCommentRemote;

public class CustomerCommentDelegate {
	private static final String jndiName = "ejb:tex/tex-ejb/CustomerComment!edu.esprit.tex.services.customer.comment.CustomerCommentRemote";

	public static CustomerCommentRemote getProxy() {
		return (CustomerCommentRemote) ServiceLocator.getInstance().getProxy(jndiName);

	}

	public static void AddComment(PostComment c) {

		getProxy().AddComment(c);

	}

	public static void UpdateComment(PostComment c) {

		getProxy().UpdateComment(c);
	}

	public static void  DeleteComment(PostComment c) {
		getProxy().DeleteComment(c);
	}
}

	