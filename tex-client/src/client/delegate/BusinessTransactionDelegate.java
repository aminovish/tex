package client.delegate;

import java.util.Date;
import java.util.List;

import client.locator.ServiceLocator;
import edu.esprit.tex.domain.BusinessTransaction;
import edu.esprit.tex.services.business.transactions.BusinessTransactionsRemote;

public class BusinessTransactionDelegate {
	private static final String jndiName = "ejb:tex/tex-ejb/BusinessTransactions!edu.esprit.tex.services.business.transactions.BusinessTransactionsRemote";
	public static BusinessTransactionsRemote getProxy() {
		return (BusinessTransactionsRemote) ServiceLocator.getInstance().getProxy(
				jndiName);

	}
	
	public static void createTransaction(BusinessTransaction bt1) {

		//getProxy().createTransaction(bt1);
	}

	public static List<BusinessTransaction> findAllTransaction() {

		
		return getProxy().findAllTransaction();
	}

	public static List<BusinessTransaction> findByDate(Date date) {

		
		return getProxy().findByDate(date);
	}



}
