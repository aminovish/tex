package client.delegate;

import java.util.List;

import client.locator.ServiceLocator;
import edu.esprit.tex.domain.Picture;
import edu.esprit.tex.services.administrator.picture.PictureUserRemote;

public class PictureUserServiceDelegate {
	private static final String jndiName = "ejb:tex/tex-ejb/PictureUser!edu.esprit.tex.services.administrator.picture.PictureUserRemote";

	public static PictureUserRemote getProxy() {
		return (PictureUserRemote) ServiceLocator.getInstance().getProxy(
				jndiName);

	}
	public static void createPictureUser(Picture picture) {
		getProxy().createPictureUser(picture);

	}

	
	public static void updatePictureUser(Picture picture) {
		getProxy().updatePictureUser(picture);

	}

	
	public static void deletePictureUser(Picture picture) {
		getProxy().deletePictureUser(picture);

	}

	
	public static Picture getPictureUserById(int id) {
		return getProxy().getPictureUserById(id);
	}

	
	public static List<Picture> getAllPictureUser() {

		return getProxy().getAllPictureUser();
	}

}
