package client.delegate;

import client.locator.ServiceLocator;
import edu.esprit.tex.domain.Parameter;
import edu.esprit.tex.services.adminstration.administrator.parameter.AdminstratorParameterRemote;




public class AdministratorParameterDelegate {

	private static final String jndiName ="ejb:tex/tex-ejb/AdminstratorParameter!edu.esprit.tex.services.adminstration.administrator.parameter.AdminstratorParameterRemote";
	
	public static AdminstratorParameterRemote getProxy (){
		return  (AdminstratorParameterRemote) ServiceLocator.getInstance().getProxy(jndiName);
		
	}

	public static void CreatePrameter(Parameter parameter) {
		getProxy().CreatePrameter(parameter);
		
	}

	
	public static void UpdatePrameter(Parameter parameter) {

		getProxy().UpdatePrameter(parameter);
		
	}


	public static Parameter FindParameter() {
		return getProxy().FindParameter();
			
		
	}


}
