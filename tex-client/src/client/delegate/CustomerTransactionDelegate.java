package client.delegate;

import java.util.Date;
import java.util.List;
import client.locator.ServiceLocator;
import edu.esprit.tex.domain.CustomerTransaction;
import edu.esprit.tex.services.customer.transaction.CustomerTransactionsRemote;

public class CustomerTransactionDelegate {
	private static final String jndiName = "ejb:tex/tex-ejb/CustomerTransactions!edu.esprit.tex.services.customer.transaction.CustomerTransactionsRemote";
	public static CustomerTransactionsRemote getProxy() {
		return (CustomerTransactionsRemote) ServiceLocator.getInstance().getProxy(
				jndiName);

	}
	
	public static void CreateTransaction(CustomerTransaction ct1) {

		getProxy().CreateTransaction(ct1);
	}

	public static List<CustomerTransaction> FindAllTransactions() {

		
		return getProxy().FindAllTransactions();
	}

	public static List<CustomerTransaction> FindByDate(Date date) {

		
		return (List<CustomerTransaction>) getProxy().FindByDate(date);
	}



}
