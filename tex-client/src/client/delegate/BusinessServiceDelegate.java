package client.delegate;

import java.util.List;

import client.locator.ServiceLocator;
import edu.esprit.tex.domain.Business;
import edu.esprit.tex.domain.Service;
import edu.esprit.tex.services.business.services.BusinessServicesRemote;

public class BusinessServiceDelegate {
	private static final String jndiName = "ejb:tex/tex-ejb/BusinessServices!edu.esprit.tex.services.business.services.BusinessServicesRemote";

	public static BusinessServicesRemote getProxy() {
		return (BusinessServicesRemote) ServiceLocator.getInstance().getProxy(
				jndiName);

	}

	public static void createService(Service service) {

		getProxy().createService(service);

	}

	public static void updateService(Service service) {

		getProxy().updateService(service);
	}

	public static void deleteService(Service service) {
		getProxy().deleteService(service);
	}

	public static List<Service> findServicesByBusiness(Business business) {

		return getProxy().findServicesByBusiness(business);

	}

	public static List<Service> findByValueAndName(int value, String name) {

		return getProxy().findByValueAndName(value, name);
	}

	public static Service findById(int id) {

		return getProxy().findById(id);
	}

}
