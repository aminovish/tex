package client.delegate;

import java.util.List;

import client.locator.ServiceLocator;
import edu.esprit.tex.services.adminstration.administrator.article.picture.ArticlePictureRemote;

public class AdminArticlePictureDelegate {
	private static final String jndiName = "ejb:tex/tex-ejb/ArticlePicture!edu.esprit.tex.services.adminstration.administrator.article.picture.ArticlePictureRemote";
	public static ArticlePictureRemote getProxy(){
		return (ArticlePictureRemote) ServiceLocator.getInstance()
				.getProxy(jndiName);		
	}
	public static void createArticlePicture(
			edu.esprit.tex.domain.ArticlePicture articlePicture) {
		getProxy().createArticlePicture(articlePicture);

	}
	public static void updateArticlePicture(
			edu.esprit.tex.domain.ArticlePicture articlePicture) {
		getProxy().updateArticlePicture(articlePicture);

	}
	public static void deleteArticlePicture(
			edu.esprit.tex.domain.ArticlePicture articlePicture) {
		getProxy().deleteArticlePicture(articlePicture);

	}
	public static edu.esprit.tex.domain.ArticlePicture getArticlePictureById(int id) {
		// TODO Auto-generated method stub
		return getProxy().getArticlePictureById(id);
	}
	public static List<edu.esprit.tex.domain.ArticlePicture> getAllPictureArticle() {
		// TODO Auto-generated method stub
		return getProxy().getAllPictureArticle();
		
	}

}
